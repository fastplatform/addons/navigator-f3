# `addons/navigator-f3`: Navigator F3 fertilization algorithm from DG AGRI

This service exposes the [Navigator F3](http://navigator-dev.teledeteccionysig.es/nutrients/l3.html) fertilization algorithm.

As an additional module of the FaST Platform, it integrates the main service(s) necessary for its operation as well as any other ancillary systems such as databases or cache systems. This repo includes both the [source code](services) and [a descriptive orchestration configuration](manifests) of the entire package.

Services:
* [addons/navigator-f3/fertilization](services/fertilization/navigator_f3)
