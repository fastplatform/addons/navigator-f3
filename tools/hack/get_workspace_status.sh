#!/bin/bash

git_rev=$(git rev-parse HEAD 2>/dev/null)
echo "BUILD_SCM_REVISION ${git_rev}"

git diff-index --quiet HEAD -- 2>/dev/null
if [[ $? == 0 ]];
then
    tree_status="Clean"
else
    tree_status="Modified"
fi
echo "BUILD_SCM_STATUS ${tree_status}"

GITSHA=$(git describe --always 2>/dev/null)

NAVIGATOR_F3_CI_VERSION=${NAVIGATOR_F3_CI_VERSION:=$(grep 'NAVIGATOR_F3_CI_VERSION\s*=' VERSION | awk '{print $3}')}

if [[ -z "${VERSION}" ]]; then
  if [ "$(git describe --tags --exact-match ${git_rev} >/dev/null 2>&1; echo $?)" -eq 0 ]; then
    VERSION=$(git describe --tags)
  else
    VERSION="${NAVIGATOR_F3_CI_VERSION}+${GITSHA}"
  fi
fi

echo "STABLE_NAVIGATOR_F3_VERSION ${VERSION}"

NAVIGATOR_F3_TAG=${VERSION/+/-}
if [ "$(git describe --tags --exact-match ${git_rev} >/dev/null 2>&1; echo $?)" -eq 0 ]; then
  NAVIGATOR_F3_TAG=${NAVIGATOR_F3_TAG}-release-${GITSHA}
  NAVIGATOR_F3_TAG=${NAVIGATOR_F3_TAG/+/-}
fi
echo "STABLE_NAVIGATOR_F3_TAG ${NAVIGATOR_F3_TAG}"
echo "STABLE_NAVIGATOR_F3_TAG_PREFIXED_WITH_COLON :${NAVIGATOR_F3_TAG}"

if [[ -z "${DOCKER_REGISTRY}" ]]; then
  DOCKER_REGISTRY="registry.gitlab.com"
fi
if [[ -z "${DOCKER_IMAGE_PREFIX}" ]]; then
  DOCKER_IMAGE_PREFIX=fastplatform/addons/navigator-f3/
fi
echo "STABLE_DOCKER_REGISTRY ${DOCKER_REGISTRY}"
echo "STABLE_DOCKER_IMAGE_PREFIX ${DOCKER_IMAGE_PREFIX}"
