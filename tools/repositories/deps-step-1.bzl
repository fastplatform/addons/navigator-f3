load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")
load("@io_bazel_rules_docker//container:container.bzl", "container_pull")
load("@io_bazel_rules_docker//python3:image.bzl", _py_image_repos = "repositories")
load("@io_bazel_rules_docker//repositories:deps.bzl", container_deps = "deps")
load("@rules_python_external//:defs.bzl", "pip_install")

def transitive_deps():
    _py_image_repos()

def docker_deps():
    container_deps()
    maybe(
        container_pull,
        name = "py3.7_image_base",
        registry = "gcr.io",
        repository = "distroless/python3-debian10",
        digest = "sha256:025b77e95e701917434c478e7fd267f3d894db5ca74e5b2362fe37ebe63bbeb0",
    )
    maybe(
        container_pull,
        name = "py3.7_debug_image_base",
        registry = "gcr.io",
        repository = "distroless/python3-debian10",
        digest = "sha256:ebd73d8f4da293c9826e8646137a05260ecd1b7ee103cb1f62ebf010fda7c7f9",
    )
    

def pip_deps():
    maybe(
        pip_install,
        name = "fertilization_navigator_f3_pip",
        requirements = "//services/fertilization/navigator_f3:requirements.txt",
    )
    maybe(
        pip_install,
        name = "fertilization_navigator_f3_pip_dev",
        requirements = "//services/fertilization/navigator_f3:requirements-dev.txt",
    )

def deps_step_1():
    transitive_deps()
    docker_deps()
    pip_deps()
