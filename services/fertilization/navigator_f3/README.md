# `addons/navigator-f3/fertilization`: Navigator F3 fertilization advice add-on

This add-on is based on the _Navigator_ study from the European Commission (DG AGRI) and more specifically on the F3 algorithm (for nutrients). A development interface of the original algorithm can be found [here](http://navigator-dev.teledeteccionysig.es/nutrients/l3.html) for reference.

<div align="center">
    <img src="docs/img/navigator-tool-screenshot.png" width="420">
    <div>The original <i>Navigator Tool</i> web interface</div>
    <br>
</div>

Note that the FaST implementation of F3 is a complete rewrite of the original algorithm (it does **not** use the original algorithm under the hood) to make it suitable for the FaST environment and prerequisites. In addition to the original algorithm, the FaST port provides:
- a [GraphQL API](#graphql-server) to query the base data and to run nutrient computations
- a [web administration interface](#administration-interface) to configure all the parameters of the algorithm, in real-time
- a [full test suite](#running-the-tests) to validate the FaST implementation against the original implementation

## Architecture

The service is entirely built in [Python](https://www.python.org/) and comprises the following components:
- a web server based on the [Django](https://www.djangoproject.com/) framework that provides the GraphQL API (using [Graphene](https://graphene-python.org/)) and the web administration interface (using Django Admin)
- a [Postgres](https://www.postgresql.org/) database that stores the base data and configuration

```plantuml
actor "Farmer" as farmer
actor "Admin" as admin

agent "Farmer app" as farmer_app
agent "Administration portal" as admin_portal

component "FaST API Gateway" as api #line.dashed
component "Web server" as web
database "Database" as db

farmer --> farmer_app
farmer_app --> api: /graphql
api --> web: /graphql

admin --> admin_portal
admin_portal --> web: /admin
web --> db
```

Note: the web server is not designed to run without the database.

## Algorithm

This add-on is a Python port of the original [Node.js](https://nodejs.org/) implementation of the [Navigator F3](http://navigator-dev.teledeteccionysig.es/nutrients/l3.html) algorithm. The original [Node.js](https://nodejs.org/) source code can be found in the [tests directory](./navigator_f3/tests/base/).

The calculation for the Navigator F3 recommendation, at a high level, is as follows:
1. Compute the `inputs` to the crop
    - Add nitrogen recovered by the plant from soil nitrogen mineralization
    - Add atmospheric nitrogen fixed by the crop
    - Add nitrogen applied to the crop through irrigation
    - Add nitrogen present in the soil at the start of the season
    - Add nutrients (nitrogen, phosphorus, potassium) applied to the crop through fertilization
2. Compute the `outputs` from the crop
    - Add nitrogen lost through leaching
    - Add nitrogen taken by the crop and harvested
    - Add nitrogen that should be present in the soil at the end of the season
    - Add phosphorus taken by the crop and harvested
    - Add potassium taken by the crop and harvested
    - Add nitrogen lost through denitrification
    - Add nitrogen lost through volatilization
3. Compute the `balance` as `balance = outputs - inputs`
4. If some fertilization quantities where unknown at step 1, infer those quantities from the balance (to cover for the missing fertilizers and get a zero balance)
5. If some fertilization quantities have been inferred at step 4, recompute steps 1, 2 and 3 (which depend on the fertilization quantities).
6. Return the balance, as well as the updated fertilization quantities.

The details of the FaST implementation can be found in the [navigator_f3/lib/algorithm.py](./navigator_f3/lib/algorithm.py) file. If necessary, agronomic information and justification of the algorithm should be requested from the authors of the original algorithm (DG AGRI).

## Data model

The algorithm uses a database to store the crop parameters, fertilizer parameters, meta-parameters, etc. Part of this database is accessible in a readonly mode through the [GraphQL API](#graphql-server) (e.g. the values that need to be displayed to the user in the app), and all the values are accessible in write mode through the web [administration interface](#administration-interface).

The data model is structured as follows:

```plantuml
rectangle "Crop" as crop {
    entity CropProfileGroup {
        <i>A type / group of crops,
        <i>as defined in the Navigator F3
        <i>nomenclature
        --
        * id
        --
        * name
    }

    enum CropProfileCycle {
        <i>The cycle of the crop
        --
        annual
        pluriannual
    }

    entity CropProfile {
        <i>A crop as defined in the Navigator F3
        <i>nomenclature, including its nutrient
        <i>content
        --
        * id
        --
        * name
        * latin_name
        * group
        * is_active
        * is_legume
        * cycle
        * cv
        * harvest_product_name
        * harvest_index
        * harvest_dry_matter
        * harvest_nitrogen_content
        * harvest_phosphorus_content
        * harvest_potassium_content
        * residues_product_name
        * residues_left_in_field
        * residues_dry_matter
        * residues_nitrogen_content
        * residues_phosphorus_content
        * residues_potassium_content
    }

    entity CropNitrogenFixationProfile {
        <i>A nitrogen fixation profile for crops
        <i>depending on the soil organic matter
        --
        * id
        --
        * soil_organic_matter_content_from
        * soil_organic_matter_content_to
        * crop_profile_cycle
        * nitrogen_fixation
    }

    CropProfileCycle - CropProfile::cycle
    CropProfileCycle - CropNitrogenFixationProfile::crop_profile_cycle
    CropProfileGroup --o{ CropProfile
}
```

```plantuml
rectangle "Soil" as soil {
    enum SoilMineralizationTexture {
        <i>The texture of the soil
        <i>with regards to mineralization
        --
        sandy_texture
        loamy_texture
        clay_texture
    }

    entity Soil {
        <i>A soil type, as defined in the
        <i>Navigator F3 / USDA Soil Data
        <i>nomenclature
        --
        * id
        --
        * name
        * is_active
        * mineralization_texture
        * carbon_nitrogen_ratio
        * cec
        * phosphorus_content_threshold_min
        * phosphorus_content_threshold_max
        * potassium_content_threshold_min
        * potassium_content_threshold_max
        * fk
        * density
        * qfc_qwp
    }

    SoilMineralizationTexture - Soil::mineralization_texture

    entity SoilMineralizationProfile {
        <i>A soil nitrogen mineralization profile
        <i>depending on the soil organic matter
        --
        * id
        --
        * soil_mineralization_texture
        * soil_organic_matter_content_from
        * soil_organic_matter_content_to
        * nitrogen_mineralization
    }

    entity VolatilizationCoefficientForPH {
        <i>Nitrogen volatilization coefficient
        <i>depending on the soil pH
        --
        * id
        --
        * ph_from
        * ph_to
        * volatilization_coefficient
    }

    entity VolatilizationCoefficientForCEC {
        <i>Nitrogen volatilization coefficient
        <i>depending on the soil CEC
        --
        * id
        --
        * cec_from
        * cec_to
        * volatilization_coefficient
    }

    entity PhosphorusAnalysisMethod {
        <i>A type of soil phosphorus analysis
        --
        * id
        --
        * name
        * value
        * is_active
    }
}
```

```plantuml
rectangle "Fertilizer" as fertilizer {
    enum FertilizerIncorporationMethod {
        <i>A method used to incorporate
        <i>fertilizers into the soil
        --
        incorporated
        topdressing
    }

    entity Fertilizer {
        <i>A fertilizer, as defined in the
        <i>Navigator F3 nomenclature
        --
        * id
        --
        * name
        * is_active
        * is_organic
        * volatilization_coefficient
        * dry_matter
        * incorporation_method
        * nitrogen_total_content
        * nitrogen_no3_content
        * nitrogen_nh4_content
        * nitrogen_urea_content
        * nitrogen_cn2_content
        * nitrogen_organic_dry_matter_content
        * phosphorus_total_content
        * phosphorus_p2o5_content
        * potassium_total_content
        * potassium_k2o_content
        * sodium_total_content
        * calcium_total_content
        * calcium_cao_content
        * magnesium_total_content
        * magnesium_mgo_content
        * sulphur_total_content
        * sulphur_so3_content
        * sulphur_so4_content
        * chlorine_total_content
    }

    FertilizerIncorporationMethod - Fertilizer::incorporation_method

    enum FertilizerApplicationFrequency {
        <i>A frequency of fertilizer application
        --
        annual
        biennial
    }
}
```

```plantuml
rectangle "Agricultural practice" as agricultural_practice {
    enum PhosphorusPotassiumStrategy {
        <i>A strategy for optimizing
        <i>phosphorus and potassium
        <i>inputs
        --
        sufficiency
        maintenance
        maximum_yield
        minimum_fertilizer
    }

    enum WaterSupplyMethod {
        <i>A method for supplying
        <i>water to the crop
        --
        rainfed
        irrigated
    }

    enum IrrigationMethod {
        <i>A method of irrigation
        --
        trickle
        sprinkler
        surface
    }
}
```

```plantuml
rectangle "Young trees (Greece only)" as young_trees {
    entity YoungTree {
        <i>A species of young tree
        <i>in the Navigator F3
        <i>nomenclature
        --
        * id
        --
        * crop_profile
        * ph_ideal_min
        * ph_ideal
        * ph_ideal_max
        * trees_per_ha
    }

    entity CropProfile {
        <i>A crop as defined in the Navigator F3
        <i>nomenclature, including its nutrient
        <i>content (see above)
        --
        * id
        --
        * name
        * ...
    }

    entity YoungTreeFertilizationYear {
        <i>The fertilization requirements
        <i>for a given growing year of a
        <i>young tree species
        --
        * id
        --
        * young_tree
        * sort_order
        * label
        * nitrogen_per_ha_min
        * nitrogen_per_ha
        * nitrogen_per_ha_max
        * phosphorus_per_ha_min
        * phosphorus_per_ha
        * phosphorus_per_ha_max
        * potassium_per_ha_min
        * potassium_per_ha
        * potassium_per_ha_max
        * nitrogen_per_tree_min
        * nitrogen_per_tree
        * nitrogen_per_tree_max
        * phosphorus_per_tree_min
        * phosphorus_per_tree
        * phosphorus_per_tree_max
        * potassium_per_tree_min
        * potassium_per_tree
        * potassium_per_tree_max
    }

    CropProfile - YoungTree::crop_profile
    YoungTree -o{ YoungTreeFertilizationYear::young_tree
}
```

```plantuml
rectangle "Other" as other {
    entity ClimateZone {
        <i>A climate zone as defined 
        <i>in the Navigator F3
        <i>nomenclature
        --
        * id
        --
        * name
        * is_active
        * humidity_factor
        * rainfall_annual
        * rainfall_autumn_winter
    }

    entity Configuration {
        <i>Generic configuration/metadata for the algorithm
        --
        * nitrogen_ratio_roots_vs_shoots
        * fraction_residues_left_in_field_after_harvest_with_roots
        * nitrogen_fixation_non_legume
        * irrigation_factor_trickle
        * irrigation_factor_sprinkler
        * irrigation_factor_surface
        * crop_phosphorus_extraction_max
        * crop_potassium_extraction_max
        * fertilization_dry_matter_amendment
        * fertilization_nitrogen_content_dry_matter_amendment
        * display_surface_unit_name
        * display_surface_unit_short_name
        * display_surface_unit_ratio_to_hectare
        * default_phosphorus_potassium_strategy
        * default_yield
        * default_soil_id
        * default_soil_ph
        * default_soil_depth
        * default_soil_nitrogen_content_initial
        * default_soil_nitrogen_content_final
        * default_phosphorus_analysis_method_id
        * default_soil_phosphorus
        * default_soil_potassium
        * default_climate_zone_id
        * default_water_supply_method
        * default_irrigation_method
        * default_irrigation_dose
        * default_nitrogen_no3_content_in_water
        * maximum_computed_quantity_for_organic_fertilizers
        * maximum_computed_quantity_for_mineral_fertilizers
        * maximum_computed_quantity_total_nitrogen
        * maximum_computed_quantity_total_phosphorus
        * maximum_computed_quantity_total_potassium
    }
}
```

#### Initial metadata for the algorithm

The initial metadata for the algorithm (to fill in the tables listed [above](#data-model)) can be found in the [init](../../../init/) folder. These are the initial values that were provided by the algorithm authors. The initialization files are provided in [English](../../../init/), [Bulgarian](../../../init/bg/) and [Greek](../../../init/gr/).

## GraphQL server

The service exposes a GraphQL server on the `/graphql` endpoint. The following (high-level) GraphQL schema is exposed:
```graphql
query {
    navigator_f3__config
    navigator_f3__climate_zone
    navigator_f3__crop_profile_group
    navigator_f3__crop_profile
    navigator_f3__phosphorus_analysis_method
    navigator_f3__soil_mineralization_profile
    navigator_f3__soil
    navigator_f3__fertilizer
    navigator_f3__fertilization_application_frequency
    navigator_f3__fertilization_incorporation_method
    navigator_f3__water_supply_method
    navigator_f3__irrigation_method
    navigator_f3__phosphorus_potassium_strategy
    navigator_f3__compute_balance

    # if the young trees module is activated:
    young_trees__young_trees
    young_trees__get_fertilization
}

# The service does not expose any mutation or subscription.
```

The schema is self documenting and we encourage the reader to introspect it to learn more about each node and its associated fields and relations. This can done using the [GraphiQL](https://github.com/graphql/graphiql) or [Altair](https://altair.sirmuel.design/) tools, for example:

<div align="center">
    <img src="docs/img/schema-docs.png" width="340">
</div>

Several example queries are provided for each node, as part of the test suite:
- for the Navigator F3 algorithm: [navigator_f3/tests/graphql/]()
- for the young trees module: [young_trees/tests/graphql/]()

## Administration interface

The service exposes an administration interface on the `/admin` endpoint, which allows the administrator to manage the base data tables of the algorithm:
- Climate zones
- Crop profiles and groups
- Soil textures
- Fertilizers
- Algorithm meta-parameters
- ...

All the underlying algorithm tables and meta-parameters are editable. Objects can be added, edited and removed. Changes in the administration interface are immediately reflected in the GraphQL endpoint (and therefore in the FaST mobile app).

<div align="center">
    <img src="docs/img/admin-interface.png" width="420">
</div>

## Localization

The administration interface is localized in the following languages:
- Bulgarian / Български
- Greek / Ελληνική
- English
- Spanish / Castellano
- Estonian / Eesti
- French / Français
- Italian / Italiano
- Romanian / Română
- Slovak / Slovenčina

Note that the content of the configuration tables (e.g. the crop profiles) is not localized (it is in the language it was loaded in).

## Environment variables

Below are the available environment variables for the web server:
- `DJANGO_SECRET_KEY`: See [SECRET_KEY](https://docs.djangoproject.com/en/3.2/ref/settings/#std:setting-SECRET_KEY). No default value.
- `DEBUG`: See [DEBUG](https://docs.djangoproject.com/en/3.2/ref/settings/#debug). Defaults to `True`.
- `DOMAIN_NAME`: The domain on which the service is running. Ignored if `DEBUG` is `True`. No default value.
- `DJANGO_ALLOWED_HOSTS`: See [ALLOWED_HOSTS](https://docs.djangoproject.com/en/3.2/ref/settings/#std:setting-ALLOWED_HOSTS)
- `DJANGO_SECURE_HSTS_SECONDS`: See [SECURE_HSTS_SECONDS](https://docs.djangoproject.com/en/3.2/ref/settings/#std:setting-SECURE_HSTS_SECONDS)
- `DJANGO_CSRF_COOKIE_SECURE`: See [CSRF_COOKIE_SECURE](https://docs.djangoproject.com/en/3.2/ref/settings/#std:setting-CSRF_COOKIE_SECURE)

- `POSTGRES_DATABASE`: See [NAME](https://docs.djangoproject.com/en/3.2/ref/settings/#name). No default value.
- `POSTGRES_USER`: See [USER](https://docs.djangoproject.com/en/3.2/ref/settings/#user). No default value.
- `POSTGRES_PASSWORD`: See [PASSWORD](https://docs.djangoproject.com/en/3.2/ref/settings/#password). No default value.
- `POSTGRES_HOST`: See [HOST](https://docs.djangoproject.com/en/3.2/ref/settings/#host). No default value.
- `POSTGRES_PORT`: See [PORT](https://docs.djangoproject.com/en/3.2/ref/settings/#port). No default value.
- `POSTGRES_CONN_MAX_AGE`: See [CONN_MAX_AGE](https://docs.djangoproject.com/en/3.2/ref/settings/#conn-max-age). Defaults to `0`.
- `DISABLE_SERVER_SIDE_CURSORS`: See [DISABLE_SERVER_SIDE_CURSORS](https://docs.djangoproject.com/en/4.0/ref/settings/#disable-server-side-cursors). Defaults to `True`.

- `LANGUAGE_CODE`: See [LANGUAGE_CODE](https://docs.djangoproject.com/en/3.2/ref/settings/#std:setting-LANGUAGE_CODE). Defaults to `en`.
- `TIME_ZONE`: See [TIME_ZONE](https://docs.djangoproject.com/en/3.2/ref/settings/#std:setting-TIME_ZONE). Defaults to `UTC`.

- `ENABLE_YOUNG_TREES`: Whether to enable the young trees feature (for Greece). Default to `False`.

## Development 

To run the service on your local machine, ensure you have the following dependencies installed:
- [Docker](https://www.docker.com/)
- [Python 3.7+](https://www.python.org/) with the `virtualenv` package installed
- The `make` utility

Create a virtual environment and activate it:
```bash
virtualenv .venv
source .venv/bin/activate
```

Install the Python dependencies:
```bash
pip install -r requirements.txt
pip install -r requirements-dev.txt  # optional
```

Start the Postgres server:
```bash
make start-postgres
# make stop-postgres (to stop the server)
```

Fill in the database with the base Navigator F3 data:
```bash
make init-data
```

Start the development server with auto-reload enabled:
```bash
make start
```

The GraphQL endpoint is now available at `http://localhost:8000/graphql` and the administration interface is available at `http://localhost:8000/admin`.

### Management commands

The following management commands are available from the `Makefile`.

- Django targets:
    - `make start`: Start the development server with auto-reload enabled.
    - `make starts`: Start the development server `https` on with auto-reload enabled (assumes that you have the relevant certificates in the `/certificates` directory).
    - `make migrations`: Generate Django migrations
    - `make migrate`: Apply Django migrations
    - `make collectstatic`: Collect static files in the `/static` directory

- Postgres targets:
    - `make start-postgres`: Start the local Postgres server
    - `make stop-postgres`: Stop the local Postgres server
    - `make reset-postgres`: Delete the data of the local Postgres server

- Internationalisation targets:
    - `make messages`: Generate localized files (`.po`) for defined locales and extract untranslated strings
    - `make messages-csv`: Generate localized files (`.csv`) for defined locales (you need to have the `po2csv` utility installed, from the [Translate Toolkit](http://docs.translatehouse.org/projects/translate-toolkit/))
    - `make csv-messages`: Generate `.po` files from `.csv` for defined locales
    (you need to have the `csv2po` utility installed, from the [Translate Toolkit](http://docs.translatehouse.org/projects/translate-toolkit/))
    - `make compile-messages`: Compile `django.po` into `django.mo`

- Data loading targets:
    - `make create-super-user`: Create a super user with the given username "admin" and password "azerty".
    - `make init-data`:  Load seed data in the database
    - `make clear-data`: Clear data from the database

- Testing targets:
    - `make test`: Run the test suite
	
Some additional commands are available in the `Makefile`, we encourage you to read it (or to type `make help` to get the full list of commands).

## Running the tests

In addition to the dependencies listed above, the service also requires the following dependencies:
- [node.js](https://nodejs.org/en/)

Install the node.js packages for the base implementation:
```bash
cd navigator_f3/tests/base && npm install
```

Start the Postgres server (if it is not already running):
```bash
make start-postgres
```

With the virtual environment activated, run the tests:
```bash
make test
```

The test suite contains 2 types of tests:
- [tests of the algorithm itself](navigator_f3/tests/test_lib.py): this is done by running random inputs through the original algorithm (in node.js) and through the FaST implementation, and ensuring that results are identical. The number of iterations can be adjusted (set to 100 by default); the tests have previously been run on approximately 1,000,000 random inputs.
- tests of the GraphQL API: this is done by running predefined queries against the GraphQL endpoint and ensuring that the results are the expected ones. The queries are listed [here](navigator_f3/tests/graphql/) and [here](young_trees/tests/graphql/).
