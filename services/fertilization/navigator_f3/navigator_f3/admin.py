from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from import_export.admin import ImportExportMixin

from navigator_f3.models import (
    CropProfileGroup,
    CropProfile,
    CropNitrogenFixationProfile,
    ClimateZone,
    Soil,
    SoilMineralizationProfile,
    Fertilizer,
    PhosphorusAnalysisMethod,
    VolatilizationCoefficientForPH,
    VolatilizationCoefficientForCEC,
)

from navigator_f3.resources import (
    CropProfileGroupResource,
    CropProfileResource,
    CropNitrogenFixationProfileResource,
    ClimateZoneResource,
    SoilResource,
    SoilMineralizationProfileResource,
    FertilizerResource,
    PhosphorusAnalysisMethodResource,
    VolatilizationCoefficientForPHResource,
    VolatilizationCoefficientForCECResource,
)


@admin.register(CropProfileGroup)
class CropProfileGroupAdmin(ImportExportMixin, admin.ModelAdmin):
    resource_class = CropProfileGroupResource
    list_display = ["name"]
    list_display_links = ["name"]


@admin.register(CropProfile)
class CropProfileAdmin(ImportExportMixin, admin.ModelAdmin):
    resource_class = CropProfileResource
    list_display = [
        "name",
        "is_active",
        "group",
        "is_legume",
        "cycle",
        "cv",
        "harvest_index",
        "harvest_dry_matter",
    ]
    list_display_links = ["name"]

    fieldsets = (
        (
            None,
            {
                "fields": (
                    "id",
                    ("name", "latin_name"),
                    "is_active",
                    "group",
                    ("is_legume", "cycle", "cv"),
                )
            },
        ),
        (
            _("Harvest"),
            {
                "fields": (
                    "harvest_product_name",
                    ("harvest_index", "harvest_dry_matter"),
                    (
                        "harvest_nitrogen_content",
                        "harvest_phosphorus_content",
                        "harvest_potassium_content",
                    ),
                )
            },
        ),
        (
            _("Residues"),
            {
                "fields": (
                    "residues_product_name",
                    ("residues_left_in_field", "residues_dry_matter"),
                    (
                        "residues_nitrogen_content",
                        "residues_phosphorus_content",
                        "residues_potassium_content",
                    ),
                )
            },
        ),
    )


@admin.register(CropNitrogenFixationProfile)
class CropNitrogenFixationProfileAdmin(ImportExportMixin, admin.ModelAdmin):
    resource_class = CropNitrogenFixationProfileResource
    list_display = [
        "soil_organic_matter_content_from",
        "soil_organic_matter_content_to",
        "crop_profile_cycle",
        "nitrogen_fixation",
    ]
    list_display_links = [
        "soil_organic_matter_content_from",
        "soil_organic_matter_content_to",
    ]


@admin.register(ClimateZone)
class ClimateZoneAdmin(ImportExportMixin, admin.ModelAdmin):
    resource_class = ClimateZoneResource
    list_display = [
        "name",
        "is_active",
        "humidity_factor",
        "rainfall_annual",
        "rainfall_autumn_winter",
    ]
    list_display_links = ["name"]


@admin.register(Soil)
class SoilAdmin(ImportExportMixin, admin.ModelAdmin):
    resource_class = SoilResource
    list_display = [
        "name",
        "is_active",
        "mineralization_texture",
        "carbon_nitrogen_ratio",
        "cec",
        "fk",
        "density",
        "qfc_qwp",
    ]
    list_display_links = ["name"]


@admin.register(SoilMineralizationProfile)
class SoilMineralizationProfileAdmin(ImportExportMixin, admin.ModelAdmin):
    resource_class = SoilMineralizationProfileResource
    list_display = [
        "soil_mineralization_texture",
        "soil_organic_matter_content_from",
        "soil_organic_matter_content_to",
        "nitrogen_mineralization",
    ]
    list_display_links = [
        "soil_organic_matter_content_from",
        "soil_organic_matter_content_to",
    ]


@admin.register(Fertilizer)
class FertilizerAdmin(ImportExportMixin, admin.ModelAdmin):
    resource_class = FertilizerResource
    list_display = [
        "name",
        "is_active",
        "is_organic",
        "nitrogen_total_content",
        "phosphorus_total_content",
        "potassium_total_content",
    ]
    list_display_links = ["name"]

    fieldsets = (
        (
            None,
            {
                "fields": (
                    "id",
                    "name",
                    "is_active",
                    "is_organic",
                )
            },
        ),
        (
            _("For organic fertilizers only"),
            {
                "fields": (
                    "dry_matter",
                    "incorporation_method",
                    "volatilization_coefficient",
                )
            },
        ),
        (
            _("Nitrogen"),
            {
                "fields": (
                    "nitrogen_total_content",
                    "nitrogen_no3_content",
                    "nitrogen_nh4_content",
                    "nitrogen_urea_content",
                    "nitrogen_cn2_content",
                    "nitrogen_organic_dry_matter_content",
                )
            },
        ),
        (
            _("Phosphorus"),
            {
                "fields": (
                    "phosphorus_total_content",
                    "phosphorus_p2o5_content",
                )
            },
        ),
        (
            _("Potassium"),
            {
                "fields": (
                    "potassium_total_content",
                    "potassium_k2o_content",
                )
            },
        ),
        (
            _("Sodium"),
            {"fields": ("sodium_total_content",)},
        ),
        (
            _("Calcium"),
            {"fields": ("calcium_total_content", "calcium_cao_content")},
        ),
        (
            _("Magnesium"),
            {
                "fields": (
                    "magnesium_total_content",
                    "magnesium_mgo_content",
                )
            },
        ),
        (
            _("Sulfur"),
            {
                "fields": (
                    "sulphur_total_content",
                    "sulphur_so3_content",
                    "sulphur_so4_content",
                )
            },
        ),
        (
            _("Chlorine"),
            {"fields": ("chlorine_total_content",)},
        ),
    )


@admin.register(PhosphorusAnalysisMethod)
class PhosphorusAnalysisMethodAdmin(ImportExportMixin, admin.ModelAdmin):
    resource_class = PhosphorusAnalysisMethodResource
    list_display = ["name", "is_active", "value"]
    list_display_links = ["name"]


@admin.register(VolatilizationCoefficientForPH)
class VolatilizationCoefficientForPHAdmin(ImportExportMixin, admin.ModelAdmin):
    resource_class = VolatilizationCoefficientForPHResource
    list_display = ["ph_from", "ph_to", "volatilization_coefficient"]
    list_display_links = ["ph_from", "ph_to"]


@admin.register(VolatilizationCoefficientForCEC)
class VolatilizationCoefficientForCECAdmin(ImportExportMixin, admin.ModelAdmin):
    resource_class = VolatilizationCoefficientForCECResource
    list_display = ["cec_from", "cec_to", "volatilization_coefficient"]
    list_display_links = ["cec_from", "cec_to"]
