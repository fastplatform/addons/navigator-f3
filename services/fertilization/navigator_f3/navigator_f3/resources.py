from utils.resources import ModelResourceWithColumnIds
from navigator_f3.models import (
    CropProfileGroup,
    CropProfile,
    CropNitrogenFixationProfile,
    ClimateZone,
    Soil,
    SoilMineralizationProfile,
    Fertilizer,
    PhosphorusAnalysisMethod,
    VolatilizationCoefficientForPH,
    VolatilizationCoefficientForCEC,
)


class CropProfileGroupResource(ModelResourceWithColumnIds):
    class Meta:
        model = CropProfileGroup


class CropProfileResource(ModelResourceWithColumnIds):
    class Meta:
        model = CropProfile


class CropNitrogenFixationProfileResource(ModelResourceWithColumnIds):
    class Meta:
        model = CropNitrogenFixationProfile


class ClimateZoneResource(ModelResourceWithColumnIds):
    class Meta:
        model = ClimateZone


class SoilResource(ModelResourceWithColumnIds):
    class Meta:
        model = Soil


class SoilMineralizationProfileResource(ModelResourceWithColumnIds):
    class Meta:
        model = SoilMineralizationProfile


class FertilizerResource(ModelResourceWithColumnIds):
    class Meta:
        model = Fertilizer


class PhosphorusAnalysisMethodResource(ModelResourceWithColumnIds):
    class Meta:
        model = PhosphorusAnalysisMethod


class VolatilizationCoefficientForPHResource(ModelResourceWithColumnIds):
    class Meta:
        model = VolatilizationCoefficientForPH


class VolatilizationCoefficientForCECResource(ModelResourceWithColumnIds):
    class Meta:
        model = VolatilizationCoefficientForCEC
