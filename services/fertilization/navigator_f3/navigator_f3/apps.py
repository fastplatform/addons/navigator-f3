from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class NavigatorF3Config(AppConfig):
    name = "navigator_f3"
    verbose_name = _("Algorithm data")
