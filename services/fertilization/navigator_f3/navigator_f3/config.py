from typing import OrderedDict
from django.utils.translation import ugettext_lazy as _


def get_phosphorus_potassium_strategy_choices():
    from navigator_f3.models import PhosphorusPotassiumStrategy

    return PhosphorusPotassiumStrategy.choices


def get_soil_id_choices():
    from navigator_f3.models import Soil

    return Soil.objects.all().values_list("id", "name")


def get_phosphorus_analysis_method_id_choices():
    from navigator_f3.models import PhosphorusAnalysisMethod

    return PhosphorusAnalysisMethod.objects.all().values_list("id", "name")


def get_climate_zone_id_choices():
    from navigator_f3.models import ClimateZone

    return ClimateZone.objects.all().values_list("id", "name")


def get_water_supply_method_choices():
    from navigator_f3.models import WaterSupplyMethod

    return WaterSupplyMethod.choices


def get_irrigation_method_choices():
    from navigator_f3.models import IrrigationMethod

    return IrrigationMethod.choices


NAVIGATOR_F3_ADDITIONAL_FIELDS = {
    "phosphorus_potassium_strategy_select": [
        "django.forms.fields.ChoiceField",
        {
            "widget": "django.forms.Select",
            "choices": get_phosphorus_potassium_strategy_choices,
        },
    ],
    "soil_id_select": [
        "django.forms.fields.ChoiceField",
        {"widget": "django.forms.Select", "choices": get_soil_id_choices},
    ],
    "phosphorus_analysis_method_id_select": [
        "django.forms.fields.ChoiceField",
        {
            "widget": "django.forms.Select",
            "choices": get_phosphorus_analysis_method_id_choices,
        },
    ],
    "climate_zone_id_select": [
        "django.forms.fields.ChoiceField",
        {"widget": "django.forms.Select", "choices": get_climate_zone_id_choices},
    ],
    "water_supply_method_select": [
        "django.forms.fields.ChoiceField",
        {"widget": "django.forms.Select", "choices": get_water_supply_method_choices},
    ],
    "irrigation_method_select": [
        "django.forms.fields.ChoiceField",
        {"widget": "django.forms.Select", "choices": get_irrigation_method_choices},
    ],
}

NAVIGATOR_F3_CONFIG = OrderedDict(
    [
        # Algorithm constants
        # -------------------
        (
            "NITROGEN_RATIO_ROOTS_VS_SHOOTS",
            (0.1, _("Ratio of nitrogen in roots versus nitrogen in shoots"), float),
        ),
        (
            "FRACTION_RESIDUES_LEFT_IN_FIELD_AFTER_HARVEST_WITH_ROOTS",
            (
                0.15,
                "Fraction of residues left in the field after harvest with roots",
                float,
            ),
        ),
        (
            "NITROGEN_FIXATION_NON_LEGUME",
            (10.0, _("Nitrogen fixation for non-legume crops"), float),
        ),
        (
            "IRRIGATION_FACTOR_TRICKLE",
            (0.9, _("Irrigation factor for trickle irrigation"), float),
        ),
        (
            "IRRIGATION_FACTOR_SPRINKLER",
            (0.85, _("Irrigation factor for sprinkler irrigation"), float),
        ),
        (
            "IRRIGATION_FACTOR_SURFACE",
            (0.7, _("Irrigation factor for surface irrigation"), float),
        ),
        (
            "CROP_PHOSPHORUS_EXTRACTION_MAX",
            (100.0, _("Crop phosphorus extraction max"), float),
        ),
        (
            "CROP_POTASSIUM_EXTRACTION_MAX",
            (275.0, _("Crop potassium extraction max"), float),
        ),
        (
            "FERTILIZATION_DRY_MATTER_AMENDMENT",
            (20.0, _("Fertilization dry matter amendment"), float),
        ),
        (
            "FERTILIZATION_NITROGEN_CONTENT_DRY_MATTER_AMENDMENT",
            (0.99, _("Fertilization nitrogen content dry matter amendment"), float),
        ),
        #
        # Display surface unit
        # --------------------
        (
            "DISPLAY_SURFACE_UNIT_NAME",
            ("hectare", _("Display surface unit name in the app"), str),
        ),
        (
            "DISPLAY_SURFACE_UNIT_SHORT_NAME",
            ("ha", _("Display surface unit short name in the app"), str),
        ),
        (
            "DISPLAY_SURFACE_UNIT_RATIO_TO_HECTARE",
            (1.0, _("Ratio between one display surface unit and one hectare"), float),
        ),
        #
        # Default interface values for crop
        # ---------------------------------
        (
            "DEFAULT_PHOSPHORUS_POTASSIUM_STRATEGY",
            (
                "maximum_yield",
                _("Default phosphorus-potassium strategy"),
                "phosphorus_potassium_strategy_select",
            ),
        ),
        ("DEFAULT_YIELD", (2000.0, _("Default yield (kg/ha)"), float)),
        #
        # Default interface values for soil
        # ---------------------------------
        (
            "DEFAULT_SOIL_ID",
            ("sand", _("Default soil"), "soil_id_select"),
        ),
        ("DEFAULT_SOIL_PH", (7.0, _("Default soil pH"), float)),
        (
            "DEFAULT_SOIL_ORGANIC_MATTER",
            (1.5, _("Default soil organic matter (%)"), float),
        ),
        ("DEFAULT_SOIL_DEPTH", (0.3, _("Default soil depth (m)"), float)),
        (
            "DEFAULT_SOIL_NITROGEN_CONTENT_INITIAL",
            (30.0, _("Default initial soil nitrogen content"), float),
        ),
        (
            "DEFAULT_SOIL_NITROGEN_CONTENT_FINAL",
            (20.0, _("Default final soil nitrogen content"), float),
        ),
        (
            "DEFAULT_PHOSPHORUS_ANALYSIS_METHOD_ID",
            (
                "olsen",
                _("Default phosphorus analysis method"),
                "phosphorus_analysis_method_id_select",
            ),
        ),
        ("DEFAULT_SOIL_PHOSPHORUS", (0.0, _("Default soil phosphorus"), float)),
        ("DEFAULT_SOIL_POTASSIUM", (0.0, _("Default soil potassium"), float)),
        #
        # Default interface values for plot
        # ---------------------------------
        (
            "DEFAULT_CLIMATE_ZONE_ID",
            ("continental", _("Default climate zone"), "climate_zone_id_select"),
        ),
        (
            "DEFAULT_WATER_SUPPLY_METHOD",
            ("rainfed", _("Default water supply method"), "water_supply_method_select"),
        ),
        (
            "DEFAULT_IRRIGATION_METHOD",
            ("sprinkler", _("Default irrigation method"), "irrigation_method_select"),
        ),
        (
            "DEFAULT_IRRIGATION_DOSE",
            (3000.0, _("Default irrigation dose (m3/ha)"), float),
        ),
        (
            "DEFAULT_NITROGEN_NO3_CONTENT_IN_WATER",
            (24.0, _("Nitrogen (NO3) content in water (ppm)"), float),
        ),
        # Algorithm optimizer parameters
        (
            "MAXIMUM_COMPUTED_QUANTITY_FOR_ORGANIC_FERTILIZERS",
            (100000.0, _("Maximum computed quantity (in kg/ha) for organic fertilizers"), float),
        ),
        (
            "MAXIMUM_COMPUTED_QUANTITY_FOR_MINERAL_FERTILIZERS",
            (2000.0, _("Maximum computed quantity (in kg/ha) for mineral fertilizers"), float),
        ),
        (
            "MAXIMUM_COMPUTED_QUANTITY_TOTAL_NITROGEN",
            (170.0, _("Maximum computed nitrogen (in kg/ha) as a result of computed quantities"), float),
        ),
        (
            "MAXIMUM_COMPUTED_QUANTITY_TOTAL_PHOSPHORUS",
            (170.0, _("Maximum computed phosphorus (in kg/ha) as a result of computed quantities"), float),
        ),
        (
            "MAXIMUM_COMPUTED_QUANTITY_TOTAL_POTASSIUM",
            (10000.0, _("Maximum computed potassium (in kg/ha) as a result of computed quantities"), float),
        ),
    ]
)

NAVIGATOR_F3_CONFIG_FIELDSETS = OrderedDict(
    [
        (
            lambda: _("Algorithm constants"),
            (
                "NITROGEN_RATIO_ROOTS_VS_SHOOTS",
                "FRACTION_RESIDUES_LEFT_IN_FIELD_AFTER_HARVEST_WITH_ROOTS",
                "NITROGEN_FIXATION_NON_LEGUME",
                "IRRIGATION_FACTOR_TRICKLE",
                "IRRIGATION_FACTOR_SPRINKLER",
                "IRRIGATION_FACTOR_SURFACE",
                "CROP_PHOSPHORUS_EXTRACTION_MAX",
                "CROP_POTASSIUM_EXTRACTION_MAX",
                "FERTILIZATION_DRY_MATTER_AMENDMENT",
                "FERTILIZATION_NITROGEN_CONTENT_DRY_MATTER_AMENDMENT",
            ),
        ),
        (
            lambda: _("Algorithm optimizer constants"),
            (
                "MAXIMUM_COMPUTED_QUANTITY_FOR_ORGANIC_FERTILIZERS",
                "MAXIMUM_COMPUTED_QUANTITY_FOR_MINERAL_FERTILIZERS",
                "MAXIMUM_COMPUTED_QUANTITY_TOTAL_NITROGEN",
                "MAXIMUM_COMPUTED_QUANTITY_TOTAL_PHOSPHORUS",
                "MAXIMUM_COMPUTED_QUANTITY_TOTAL_POTASSIUM",
            ),
        ),
        (
            lambda: _("Display surface unit"),
            (
                "DISPLAY_SURFACE_UNIT_NAME",
                "DISPLAY_SURFACE_UNIT_SHORT_NAME",
                "DISPLAY_SURFACE_UNIT_RATIO_TO_HECTARE",
            ),
        ),
        (
            lambda: _("Default interface values for crop"),
            ("DEFAULT_PHOSPHORUS_POTASSIUM_STRATEGY", "DEFAULT_YIELD"),
        ),
        (
            lambda: _("Default interface values for soil"),
            (
                "DEFAULT_SOIL_ID",
                "DEFAULT_SOIL_PH",
                "DEFAULT_SOIL_ORGANIC_MATTER",
                "DEFAULT_SOIL_DEPTH",
                "DEFAULT_SOIL_NITROGEN_CONTENT_INITIAL",
                "DEFAULT_SOIL_NITROGEN_CONTENT_FINAL",
                "DEFAULT_PHOSPHORUS_ANALYSIS_METHOD_ID",
                "DEFAULT_SOIL_PHOSPHORUS",
                "DEFAULT_SOIL_POTASSIUM",
            ),
        ),
        (
            lambda: _("Default interface values for plot"),
            (
                "DEFAULT_CLIMATE_ZONE_ID",
                "DEFAULT_WATER_SUPPLY_METHOD",
                "DEFAULT_IRRIGATION_METHOD",
                "DEFAULT_IRRIGATION_DOSE",
                "DEFAULT_NITROGEN_NO3_CONTENT_IN_WATER",
            ),
        ),
    ]
)
