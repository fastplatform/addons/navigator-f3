from django.db import models
from django.utils.translation import gettext_lazy as _
from django.core.validators import MinValueValidator, MaxValueValidator

from navigator_f3.constants import (
    CA_TO_CAO,
    MG_TO_MGO,
    P_TO_P2O5,
    K_TO_K2O,
    S_TO_SO3,
    S_TO_SO4,
)


class CropProfileGroup(models.Model):
    id = models.CharField(
        max_length=100, primary_key=True, verbose_name=_("identifier")
    )
    name = models.CharField(
        max_length=100, null=False, blank=False, verbose_name=_("name")
    )

    def __str__(self):
        return self.name

    class Meta:
        db_table = "crop_profile_group"
        verbose_name = _("crop profile group")
        verbose_name_plural = _("crop profile groups")
        ordering = ["name"]


class CropProfileCycle(models.TextChoices):
    annual = "annual", _("Annual")
    pluriannual = "pluriannual", _("Pluriannual")


class CropProfile(models.Model):
    id = models.CharField(
        max_length=512,
        primary_key=True,
        null=False,
        blank=False,
        verbose_name=_("identifier"),
    )
    name = models.CharField(
        max_length=100, null=False, blank=False, verbose_name=_("name")
    )
    latin_name = models.CharField(
        max_length=100, null=False, blank=False, verbose_name=_("latin name")
    )
    group = models.ForeignKey(
        to="navigator_f3.CropProfileGroup",
        related_name="crop_profiles",
        on_delete=models.PROTECT,
        null=False,
        blank=False,
        verbose_name=_("group"),
    )
    is_active = models.BooleanField(
        null=False,
        blank=False,
        default=True,
        verbose_name=_("active?"),
        help_text=_("Uncheck to disable"),
    )

    is_legume = models.BooleanField(null=False, blank=False, verbose_name=_("legume?"))
    cycle = models.CharField(
        max_length=50,
        choices=CropProfileCycle.choices,
        null=True,
        blank=True,
        verbose_name=_("crop cycle"),
    )
    cv = models.FloatField(null=False, blank=False, verbose_name=_("CV"))

    harvest_product_name = models.CharField(
        max_length=50,
        null=False,
        blank=False,
        verbose_name=_("name of harvested product"),
    )
    harvest_index = models.FloatField(
        null=False,
        blank=False,
        validators=[MinValueValidator(10), MaxValueValidator(99)],
        verbose_name=_("harvest index (HI)"),
        help_text=_("Ratio (10 to 99)"),
    )
    harvest_dry_matter = models.FloatField(
        null=False,
        blank=False,
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        verbose_name=_("harvest dry matter"),
        help_text=_("Percentage (0 to 100)"),
    )
    harvest_nitrogen_content = models.FloatField(
        null=False,
        blank=False,
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        verbose_name=_("harvest nitrogen (N) content"),
        help_text=_("Percentage of dry matter (0 to 100)"),
    )
    harvest_phosphorus_content = models.FloatField(
        null=False,
        blank=False,
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        verbose_name=_("harvest phosphorus (P) content"),
        help_text=_("Percentage of dry matter (0 to 100)"),
    )
    harvest_potassium_content = models.FloatField(
        null=False,
        blank=False,
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        verbose_name=_("harvest potassium (K) content"),
        help_text=_("Percentage of dry matter (0 to 100)"),
    )

    residues_product_name = models.CharField(
        max_length=50,
        null=True,
        blank=True,
        verbose_name=_("name of residues product"),
    )
    residues_left_in_field = models.FloatField(
        null=False,
        blank=False,
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        verbose_name=_("part of residues left in the field"),
        help_text=_("Percentage (0 to 100)"),
    )
    residues_dry_matter = models.FloatField(
        null=True,
        blank=True,
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        verbose_name=_("residues dry matter"),
        help_text=_("Percentage (0 to 100)"),
    )
    residues_nitrogen_content = models.FloatField(
        null=True,
        blank=True,
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        verbose_name=_("residues average nitrogen (N) content"),
        help_text=_("Percentage (0 to 100)"),
    )
    residues_phosphorus_content = models.FloatField(
        null=True,
        blank=True,
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        verbose_name=_("residues phosphorus (P) content"),
        help_text=_("Percentage (0 to 100)"),
    )
    residues_potassium_content = models.FloatField(
        null=True,
        blank=True,
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        verbose_name=_("residues potassium (K) content"),
        help_text=_("Percentage (0 to 100)"),
    )

    def __str__(self):
        return self.name

    class Meta:
        db_table = "crop_profile"
        verbose_name = _("crop profile")
        verbose_name_plural = _("crop profiles")
        ordering = ["name"]


class CropNitrogenFixationProfile(models.Model):
    id = models.AutoField(primary_key=True)
    soil_organic_matter_content_from = models.FloatField(
        null=True,
        blank=True,
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        verbose_name=_("soil organic matter (from)"),
        help_text=_("Parts-per-million (ppm)"),
    )
    soil_organic_matter_content_to = models.FloatField(
        null=True,
        blank=True,
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        verbose_name=_("soil organic matter (to)"),
        help_text=_("Parts-per-million (ppm)"),
    )
    crop_profile_cycle = models.CharField(
        max_length=50,
        choices=CropProfileCycle.choices,
        null=True,
        blank=True,
        verbose_name=_("crop cycle"),
    )
    nitrogen_fixation = models.FloatField(
        null=True,
        blank=True,
        validators=[MinValueValidator(0)],
        verbose_name=_("nitrogen fixation factor"),
        help_text=_("0 to 1"),
    )

    def __str__(self):
        return f"{self.crop_profile_cycle} / {self.soil_organic_matter_content_from} to {self.soil_organic_matter_content_to}: {self.nitrogen_fixation}"

    class Meta:
        db_table = "crop_nitrogen_fixation_profile"
        verbose_name = _("crop nitrogen fixation profile")
        verbose_name_plural = _("crop nitrogen fixation profiles")
        ordering = [
            models.F("soil_organic_matter_content_from").asc(nulls_last=False),
            models.F("soil_organic_matter_content_to").asc(nulls_last=True),
        ]


class ClimateZone(models.Model):
    id = models.CharField(
        max_length=512, primary_key=True, verbose_name=_("identifier")
    )
    name = models.CharField(
        max_length=100, unique=True, null=False, blank=False, verbose_name=_("name")
    )
    is_active = models.BooleanField(
        null=False,
        blank=False,
        default=True,
        verbose_name=_("active?"),
        help_text=_("Uncheck to disable"),
    )
    humidity_factor = models.FloatField(
        null=False,
        blank=False,
        verbose_name=_("humidity factor"),
        validators=[MinValueValidator(0), MaxValueValidator(1)],
        help_text=_("0 to 1"),
    )
    rainfall_annual = models.FloatField(
        null=False,
        blank=False,
        validators=[MinValueValidator(1), MaxValueValidator(2000)],
        verbose_name=_("annual rainfall"),
        help_text=_("Millimeters (mm)"),
    )
    rainfall_autumn_winter = models.FloatField(
        null=False,
        blank=False,
        validators=[MinValueValidator(0), MaxValueValidator(2000)],
        verbose_name=_("autumn/winter rainfall"),
        help_text=_("Millimeters (mm)"),
    )

    def __str__(self):
        return self.name

    class Meta:
        db_table = "climate_zone"
        verbose_name = _("climate zone")
        verbose_name_plural = _("climate zones")
        ordering = ["name"]


class SoilMineralizationTexture(models.TextChoices):
    sandy_texture = "sandy_texture", _("sandy texture")
    loamy_texture = "loamy_texture", _("loamy texture")
    clay_texture = "clay_texture", _("clay texture")


class Soil(models.Model):
    id = models.CharField(
        max_length=512, primary_key=True, verbose_name=_("identifier")
    )
    name = models.CharField(
        max_length=100, unique=True, null=False, blank=False, verbose_name=_("name")
    )
    is_active = models.BooleanField(
        null=False,
        blank=False,
        default=True,
        verbose_name=_("active?"),
        help_text=_("Uncheck to disable"),
    )
    mineralization_texture = models.CharField(
        max_length=20,
        null=False,
        blank=False,
        choices=SoilMineralizationTexture.choices,
        verbose_name=_("mineralization texture"),
    )

    carbon_nitrogen_ratio = models.FloatField(
        null=False,
        blank=False,
        verbose_name=_("carbon/nitrogen ratio"),
        validators=[MinValueValidator(1), MaxValueValidator(50)],
        help_text=_("Ratio (1 to 50)"),
    )
    cec = models.FloatField(
        null=False,
        blank=False,
        verbose_name=_("CEC"),
        help_text=_("Milliequivalent per kilogram (meq/kg)"),
        validators=[MinValueValidator(0), MaxValueValidator(1000)],
    )

    phosphorus_content_threshold_min = models.FloatField(
        null=False,
        blank=False,
        verbose_name=_("min threshold for phosphorus content"),
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        help_text=_("Percentage (0 to 100)"),
    )
    phosphorus_content_threshold_max = models.FloatField(
        null=False,
        blank=False,
        verbose_name=_("max threshold for phosphorus content"),
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        help_text=_("Percentage (0 to 100)"),
    )

    potassium_content_threshold_min = models.FloatField(
        null=False,
        blank=False,
        verbose_name=_("min threshold for potassium content"),
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        help_text=_("Percentage (0 to 100)"),
    )
    potassium_content_threshold_max = models.FloatField(
        null=False,
        blank=False,
        verbose_name=_("max threshold for potassium content"),
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        help_text=_("Percentage (0 to 100)"),
    )

    fk = models.FloatField(
        null=False,
        blank=False,
        verbose_name=_("fK density"),
        validators=[MinValueValidator(0)],
    )
    density = models.FloatField(
        null=False,
        blank=False,
        verbose_name=_("density"),
        validators=[MinValueValidator(0.1), MaxValueValidator(2.0)],
    )
    qfc_qwp = models.FloatField(
        null=False,
        blank=False,
        verbose_name=_("Average qFC-qWP"),
        validators=[MinValueValidator(0.1), MaxValueValidator(0.20)],
    )

    def __str__(self):
        return self.name

    class Meta:
        db_table = "soil"
        verbose_name = _("soil")
        verbose_name_plural = _("soil")
        ordering = ["name"]


class SoilMineralizationProfile(models.Model):
    id = models.AutoField(primary_key=True, verbose_name=_("identifier"))
    soil_mineralization_texture = models.CharField(
        max_length=50,
        null=False,
        blank=False,
        choices=SoilMineralizationTexture.choices,
        verbose_name=_("soil mineralization texture"),
    )
    soil_organic_matter_content_from = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("from soil organic matter content"),
        help_text=_("Percentage (0 to 100)"),
        validators=[MinValueValidator(0), MaxValueValidator(10)],
    )
    soil_organic_matter_content_to = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("to soil organic matter content"),
        help_text=_("Percentage (0 to 100)"),
        validators=[MinValueValidator(0), MaxValueValidator(10)],
    )
    nitrogen_mineralization = models.FloatField(
        null=False,
        blank=False,
        verbose_name=_("nitrogen mineralization"),
        validators=[MinValueValidator(0), MaxValueValidator(100)],
    )

    class Meta:
        db_table = "soil_mineralization_profile"
        verbose_name = _("soil mineralization profile")
        verbose_name_plural = _("soil mineralization profiles")
        ordering = [
            "soil_mineralization_texture",
            "soil_organic_matter_content_from",
            "soil_organic_matter_content_to",
        ]


class FertilizerIncorporationMethod(models.TextChoices):
    incorporated = "incorporated", _("incorporated")
    topdressing = "topdressing", _("topdressing")


class Fertilizer(models.Model):
    id = models.CharField(
        max_length=512, primary_key=True, verbose_name=_("identifier")
    )
    name = models.CharField(
        max_length=100, null=False, blank=False, verbose_name=_("name")
    )
    is_active = models.BooleanField(
        null=False,
        blank=False,
        default=True,
        verbose_name=_("active?"),
        help_text=_("Uncheck to disable"),
    )
    is_organic = models.BooleanField(
        null=False, blank=False, verbose_name=_("organic fertilizer?")
    )
    volatilization_coefficient = models.FloatField(
        null=False,
        blank=False,
        verbose_name=_("volatilization coefficient"),
    )
    # For organic fertilizers
    dry_matter = models.FloatField(
        null=True,
        blank=True,
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        verbose_name=_("dry matter"),
        help_text=_("Percentage (0 to 100)"),
    )
    incorporation_method = models.CharField(
        max_length=50,
        choices=FertilizerIncorporationMethod.choices,
        null=False,
        blank=False,
        verbose_name=_("incorporation method"),
    )

    # Nitrogen
    # --------
    # For inorganic fertilizers
    nitrogen_total_content = models.FloatField(
        null=False,
        blank=True,
        verbose_name=_("total nitrogen (N) content"),
        help_text=_(
            "Percentage (0 to 100), if left empty, will be recomputed automatically upon save"
        ),
        validators=[MinValueValidator(0), MaxValueValidator(100)],
    )
    nitrogen_no3_content = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("nitrogen (N) content as NO3"),
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        help_text=_("Percentage (0 to 100)"),
    )
    nitrogen_nh4_content = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("nitrogen (N) content as NH4"),
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        help_text=_("Percentage (0 to 100)"),
    )
    nitrogen_urea_content = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("nitrogen (N) content as urea"),
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        help_text=_("Percentage (0 to 100)"),
    )
    nitrogen_cn2_content = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("nitrogen (N) content as CN2"),
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        help_text=_("Percentage (0 to 100)"),
    )
    # For organic fertilizers
    nitrogen_organic_dry_matter_content = models.FloatField(
        null=True,
        blank=True,
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        verbose_name=_("nitrogen (N) content in organic dry matter"),
        help_text=_("Percentage (0 to 100)"),
    )

    # Phosphorus
    # ----------
    phosphorus_total_content = models.FloatField(
        null=False,
        blank=True,
        verbose_name=_("total phosphorus (P) content"),
        help_text=_(
            "Percentage (0 to 100), if left empty, will be recomputed automatically upon save"
        ),
        validators=[MinValueValidator(0), MaxValueValidator(100)],
    )
    phosphorus_p2o5_content = models.FloatField(
        null=True,
        blank=True,
        default=0,
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        verbose_name=_("phosphorus (P) content as P2O5"),
        help_text=_("Percentage (0 to 100)"),
    )

    # Potassium
    # ---------
    potassium_total_content = models.FloatField(
        null=False,
        blank=True,
        verbose_name=_("total potassium (K) content"),
        help_text=_(
            "Percentage (0 to 100), if left empty, will be recomputed automatically upon save"
        ),
        validators=[MinValueValidator(0), MaxValueValidator(100)],
    )
    potassium_k2o_content = models.FloatField(
        null=True,
        blank=True,
        default=0,
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        verbose_name=_("potassium (K) content as K2O"),
        help_text=_("Percentage (0 to 100)"),
    )

    # Sodium
    # ------
    sodium_total_content = models.FloatField(
        null=False,
        blank=True,
        verbose_name=_("total sodium (Na) content"),
        help_text=_("Percentage (0 to 100)"),
        validators=[MinValueValidator(0), MaxValueValidator(100)],
    )

    # Calcium
    # -------
    calcium_total_content = models.FloatField(
        null=False,
        blank=True,
        verbose_name=_("total calcium (Ca) content"),
        help_text=_(
            "Percentage (0 to 100), if left empty, will be recomputed automatically upon save"
        ),
        validators=[MinValueValidator(0), MaxValueValidator(100)],
    )
    calcium_cao_content = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("calcium (Ca) content as CaO"),
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        help_text=_("Percentage (0 to 100)"),
    )

    # Magnesium
    # ---------
    magnesium_total_content = models.FloatField(
        null=False,
        blank=True,
        verbose_name=_("total magnesium (Mg) content"),
        help_text=_(
            "Percentage (0 to 100), if left empty, will be recomputed automatically upon save"
        ),
        validators=[MinValueValidator(0), MaxValueValidator(100)],
    )
    magnesium_mgo_content = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("magnesium (Mg) content as MgO"),
        help_text=_("Percentage (0 to 100)"),
        validators=[MinValueValidator(0), MaxValueValidator(100)],
    )

    # Sulfur
    # ------
    sulphur_total_content = models.FloatField(
        null=False,
        blank=True,
        verbose_name=_("total sulfur (S) content"),
        help_text=_(
            "Percentage (0 to 100), if left empty, will be recomputed automatically upon save"
        ),
        validators=[MinValueValidator(0), MaxValueValidator(100)],
    )
    sulphur_so3_content = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("sulfur (S) content as SO3"),
        help_text=_("Percentage (0 to 100)"),
        validators=[MinValueValidator(0), MaxValueValidator(100)],
    )
    sulphur_so4_content = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("sulfur (S) content as SO4"),
        help_text=_("Percentage (0 to 100)"),
        validators=[MinValueValidator(0), MaxValueValidator(100)],
    )

    # Chlorine
    # --------
    chlorine_total_content = models.FloatField(
        null=False,
        blank=True,
        verbose_name=_("total chlorine (Cl) content"),
        help_text=_("Percentage (0 to 100)"),
        validators=[MinValueValidator(0), MaxValueValidator(100)],
    )

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.nitrogen_total_content = (
            (self.nitrogen_no3_content or 0)
            + (self.nitrogen_nh4_content or 0)
            + (self.nitrogen_urea_content or 0)
            + (self.nitrogen_cn2_content or 0)
        )
        self.phosphorus_content = (self.phosphorus_p2o5_content or 0) / P_TO_P2O5
        self.potassium_content = (self.potassium_k2o_content or 0) / K_TO_K2O
        self.sodium_total_content = self.sodium_total_content or 0
        self.calcium_total_content = (self.calcium_cao_content or 0) / CA_TO_CAO
        self.magnesium_total_content = (self.magnesium_mgo_content or 0) / MG_TO_MGO
        self.sulphur_total_content = (self.sulphur_so3_content or 0) / S_TO_SO3 + (
            self.sulphur_so4_content or 0
        ) / S_TO_SO4
        self.chlorine_total_content = self.chlorine_total_content or 0
        super().save(*args, **kwargs)

    class Meta:
        db_table = "fertilizer"
        verbose_name = _("fertilizer")
        verbose_name_plural = _("fertilizers")
        ordering = ["name"]


class FertilizerApplicationFrequency(models.TextChoices):
    annual = "annual", _("annual")
    biennial = "biennial", _("biennial")


class PhosphorusPotassiumStrategy(models.TextChoices):
    sufficiency = "sufficiency", _("sufficiency")
    maintenance = "maintenance", _("maintenance")
    maximum_yield = "maximum_yield", _("maximum yield")
    minimum_fertilizer = "minimum_fertilizer", _("minimum fertilizer")


class PhosphorusAnalysisMethod(models.Model):
    id = models.CharField(
        max_length=100, primary_key=True, verbose_name=_("identifier")
    )
    name = models.CharField(
        max_length=100, null=False, blank=False, verbose_name=_("name")
    )
    value = models.FloatField(null=False, blank=False, verbose_name=_("value"))
    is_active = models.BooleanField(
        null=False,
        blank=False,
        default=True,
        verbose_name=_("active?"),
        help_text=_("Uncheck to disable"),
    )

    def __str__(self):
        return self.name

    class Meta:
        db_table = "phosphorus_analysis_method"
        verbose_name = _("phosphorus analysis method")
        verbose_name_plural = _("phosphorus analysis methods")
        ordering = ["name"]


class WaterSupplyMethod(models.TextChoices):
    rainfed = "rainfed", _("rainfed")
    irrigated = "irrigated", _("irrigated")


class IrrigationMethod(models.TextChoices):
    trickle = "trickle", _("trickle")
    sprinkler = "sprinkler", _("sprinkler")
    surface = "surface", _("surface")


class VolatilizationCoefficientForPH(models.Model):
    id = models.AutoField(primary_key=True, verbose_name=_("identifier"))
    ph_from = models.FloatField(null=True, blank=True, verbose_name=_("from pH"))
    ph_to = models.FloatField(null=True, blank=True, verbose_name=_("to pH"))
    volatilization_coefficient = models.FloatField(
        null=False, blank=False, verbose_name=_("volatilization coefficient")
    )

    class Meta:
        db_table = "volatilization_coefficient_for_ph"
        verbose_name = _("volatilization coefficient for pH")
        verbose_name_plural = _("volatilization coefficients for pH")
        ordering = [
            models.F("ph_from").asc(nulls_last=False),
            models.F("ph_to").asc(nulls_last=True),
        ]


class VolatilizationCoefficientForCEC(models.Model):
    id = models.AutoField(primary_key=True, verbose_name=_("identifier"))
    cec_from = models.FloatField(null=True, blank=True, verbose_name=_("from CEC"))
    cec_to = models.FloatField(null=True, blank=True, verbose_name=_("to CEC"))
    volatilization_coefficient = models.FloatField(
        null=False, blank=False, verbose_name=_("volatilization coefficient")
    )

    class Meta:
        db_table = "volatilization_coefficient_for_cec"
        verbose_name = _("volatilization coefficient for CEC")
        verbose_name_plural = _("volatilization coefficients for CEC")
        ordering = [
            models.F("cec_from").asc(nulls_last=False),
            models.F("cec_to").asc(nulls_last=True),
        ]
