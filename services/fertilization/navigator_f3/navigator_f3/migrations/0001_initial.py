# Generated by Django 3.2.7 on 2021-10-19 07:20

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import django.db.models.expressions


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ClimateZone',
            fields=[
                ('id', models.CharField(max_length=512, primary_key=True, serialize=False, verbose_name='identifier')),
                ('name', models.CharField(max_length=100, unique=True, verbose_name='name')),
                ('is_active', models.BooleanField(default=True, help_text='Uncheck to disable', verbose_name='active?')),
                ('humidity_factor', models.FloatField(help_text='0 to 1', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(1)], verbose_name='humidity factor')),
                ('rainfall_annual', models.FloatField(help_text='Millimeters (mm)', validators=[django.core.validators.MinValueValidator(1), django.core.validators.MaxValueValidator(2000)], verbose_name='annual rainfall')),
                ('rainfall_autumn_winter', models.FloatField(help_text='Millimeters (mm)', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(2000)], verbose_name='autumn/winter rainfall')),
            ],
            options={
                'verbose_name': 'climate zone',
                'verbose_name_plural': 'climate zones',
                'db_table': 'climate_zone',
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='CropNitrogenFixationProfile',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('soil_organic_matter_content_from', models.FloatField(blank=True, help_text='Parts-per-million (ppm)', null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='soil organic matter (from)')),
                ('soil_organic_matter_content_to', models.FloatField(blank=True, help_text='Parts-per-million (ppm)', null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='soil organic matter (to)')),
                ('crop_profile_cycle', models.CharField(blank=True, choices=[('annual', 'Annual'), ('pluriannual', 'Pluriannual')], max_length=50, null=True, verbose_name='crop cycle')),
                ('nitrogen_fixation', models.FloatField(blank=True, help_text='0 to 1', null=True, validators=[django.core.validators.MinValueValidator(0)], verbose_name='nitrogen fixation factor')),
            ],
            options={
                'verbose_name': 'crop nitrogen fixation profile',
                'verbose_name_plural': 'crop nitrogen fixation profiles',
                'db_table': 'crop_nitrogen_fixation_profile',
                'ordering': [django.db.models.expressions.OrderBy(django.db.models.expressions.F('soil_organic_matter_content_from'), nulls_last=False), django.db.models.expressions.OrderBy(django.db.models.expressions.F('soil_organic_matter_content_to'), nulls_last=True)],
            },
        ),
        migrations.CreateModel(
            name='CropProfileGroup',
            fields=[
                ('id', models.CharField(max_length=100, primary_key=True, serialize=False, verbose_name='identifier')),
                ('name', models.CharField(max_length=100, verbose_name='name')),
            ],
            options={
                'verbose_name': 'crop profile group',
                'verbose_name_plural': 'crop profile groups',
                'db_table': 'crop_profile_group',
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='Fertilizer',
            fields=[
                ('id', models.CharField(max_length=512, primary_key=True, serialize=False, verbose_name='identifier')),
                ('name', models.CharField(max_length=100, verbose_name='name')),
                ('is_active', models.BooleanField(default=True, help_text='Uncheck to disable', verbose_name='active?')),
                ('is_organic', models.BooleanField(verbose_name='organic fertilizer?')),
                ('volatilization_coefficient', models.FloatField(verbose_name='volatilization coefficient')),
                ('dry_matter', models.FloatField(blank=True, help_text='Percentage (0 to 100)', null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='dry matter')),
                ('incorporation_method', models.CharField(choices=[('incorporated', 'incorporated'), ('topdressing', 'topdressing')], max_length=50, verbose_name='incorporation method')),
                ('nitrogen_total_content', models.FloatField(blank=True, help_text='Percentage (0 to 100), if left empty, will be recomputed automatically upon save', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='total nitrogen (N) content')),
                ('nitrogen_no3_content', models.FloatField(blank=True, help_text='Percentage (0 to 100)', null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='nitrogen (N) content as NO3')),
                ('nitrogen_nh4_content', models.FloatField(blank=True, help_text='Percentage (0 to 100)', null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='nitrogen (N) content as NH4')),
                ('nitrogen_urea_content', models.FloatField(blank=True, help_text='Percentage (0 to 100)', null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='nitrogen (N) content as urea')),
                ('nitrogen_cn2_content', models.FloatField(blank=True, help_text='Percentage (0 to 100)', null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='nitrogen (N) content as CN2')),
                ('nitrogen_organic_dry_matter_content', models.FloatField(blank=True, help_text='Percentage (0 to 100)', null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='nitrogen (N) content in organic dry matter')),
                ('phosphorus_total_content', models.FloatField(blank=True, help_text='Percentage (0 to 100), if left empty, will be recomputed automatically upon save', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='total phosphorus (P) content')),
                ('phosphorus_p2o5_content', models.FloatField(blank=True, default=0, help_text='Percentage (0 to 100)', null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='phosphorus (P) content as P2O5')),
                ('potassium_total_content', models.FloatField(blank=True, help_text='Percentage (0 to 100), if left empty, will be recomputed automatically upon save', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='total potassium (K) content')),
                ('potassium_k2o_content', models.FloatField(blank=True, default=0, help_text='Percentage (0 to 100)', null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='potassium (K) content as K2O')),
                ('sodium_total_content', models.FloatField(blank=True, help_text='Percentage (0 to 100)', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='total sodium (Na) content')),
                ('calcium_total_content', models.FloatField(blank=True, help_text='Percentage (0 to 100), if left empty, will be recomputed automatically upon save', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='total calcium (Ca) content')),
                ('calcium_cao_content', models.FloatField(blank=True, help_text='Percentage (0 to 100)', null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='calcium (Ca) content as CaO')),
                ('magnesium_total_content', models.FloatField(blank=True, help_text='Percentage (0 to 100), if left empty, will be recomputed automatically upon save', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='total magnesium (Mg) content')),
                ('magnesium_mgo_content', models.FloatField(blank=True, help_text='Percentage (0 to 100)', null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='magnesium (Mg) content as MgO')),
                ('sulphur_total_content', models.FloatField(blank=True, help_text='Percentage (0 to 100), if left empty, will be recomputed automatically upon save', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='total sulfur (S) content')),
                ('sulphur_so3_content', models.FloatField(blank=True, help_text='Percentage (0 to 100)', null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='sulfur (S) content as SO3')),
                ('sulphur_so4_content', models.FloatField(blank=True, help_text='Percentage (0 to 100)', null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='sulfur (S) content as SO4')),
                ('chlorine_total_content', models.FloatField(blank=True, help_text='Percentage (0 to 100)', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='total chlorine (Cl) content')),
            ],
            options={
                'verbose_name': 'fertilizer',
                'verbose_name_plural': 'fertilizers',
                'db_table': 'fertilizer',
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='PhosphorusAnalysisMethod',
            fields=[
                ('id', models.CharField(max_length=100, primary_key=True, serialize=False, verbose_name='identifier')),
                ('name', models.CharField(max_length=100, verbose_name='name')),
                ('value', models.FloatField(verbose_name='value')),
                ('is_active', models.BooleanField(default=True, help_text='Uncheck to disable', verbose_name='active?')),
            ],
            options={
                'verbose_name': 'phosphorus analysis method',
                'verbose_name_plural': 'phosphorus analysis methods',
                'db_table': 'phosphorus_analysis_method',
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='Soil',
            fields=[
                ('id', models.CharField(max_length=512, primary_key=True, serialize=False, verbose_name='identifier')),
                ('name', models.CharField(max_length=100, unique=True, verbose_name='name')),
                ('is_active', models.BooleanField(default=True, help_text='Uncheck to disable', verbose_name='active?')),
                ('mineralization_texture', models.CharField(choices=[('sandy_texture', 'sandy texture'), ('loamy_texture', 'loamy texture'), ('clay_texture', 'clay texture')], max_length=20, verbose_name='mineralization texture')),
                ('carbon_nitrogen_ratio', models.FloatField(help_text='Ratio (1 to 50)', validators=[django.core.validators.MinValueValidator(1), django.core.validators.MaxValueValidator(50)], verbose_name='carbon/nitrogen ratio')),
                ('cec', models.FloatField(help_text='Milliequivalent per kilogram (meq/kg)', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(1000)], verbose_name='CEC')),
                ('phosphorus_content_threshold_min', models.FloatField(help_text='Percentage (0 to 100)', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='min threshold for phosphorus content')),
                ('phosphorus_content_threshold_max', models.FloatField(help_text='Percentage (0 to 100)', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='max threshold for phosphorus content')),
                ('potassium_content_threshold_min', models.FloatField(help_text='Percentage (0 to 100)', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='min threshold for potassium content')),
                ('potassium_content_threshold_max', models.FloatField(help_text='Percentage (0 to 100)', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='max threshold for potassium content')),
                ('fk', models.FloatField(validators=[django.core.validators.MinValueValidator(0)], verbose_name='fK density')),
                ('density', models.FloatField(validators=[django.core.validators.MinValueValidator(0.1), django.core.validators.MaxValueValidator(2.0)], verbose_name='density')),
                ('qfc_qwp', models.FloatField(validators=[django.core.validators.MinValueValidator(0.1), django.core.validators.MaxValueValidator(0.2)], verbose_name='Average qFC-qWP')),
            ],
            options={
                'verbose_name': 'soil',
                'verbose_name_plural': 'soil',
                'db_table': 'soil',
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='SoilMineralizationProfile',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='identifier')),
                ('soil_mineralization_texture', models.CharField(choices=[('sandy_texture', 'sandy texture'), ('loamy_texture', 'loamy texture'), ('clay_texture', 'clay texture')], max_length=50, verbose_name='soil mineralization texture')),
                ('soil_organic_matter_content_from', models.FloatField(blank=True, help_text='Percentage (0 to 100)', null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(10)], verbose_name='from soil organic matter content')),
                ('soil_organic_matter_content_to', models.FloatField(blank=True, help_text='Percentage (0 to 100)', null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(10)], verbose_name='to soil organic matter content')),
                ('nitrogen_mineralization', models.FloatField(validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='nitrogen mineralization')),
            ],
            options={
                'verbose_name': 'soil mineralization profile',
                'verbose_name_plural': 'soil mineralization profiles',
                'db_table': 'soil_mineralization_profile',
                'ordering': ['soil_mineralization_texture', 'soil_organic_matter_content_from', 'soil_organic_matter_content_to'],
            },
        ),
        migrations.CreateModel(
            name='VolatilizationCoefficientForCEC',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='identifier')),
                ('cec_from', models.FloatField(blank=True, null=True, verbose_name='from CEC')),
                ('cec_to', models.FloatField(blank=True, null=True, verbose_name='to CEC')),
                ('volatilization_coefficient', models.FloatField(verbose_name='volatilization coefficient')),
            ],
            options={
                'verbose_name': 'volatilization coefficient for CEC',
                'verbose_name_plural': 'volatilization coefficients for CEC',
                'db_table': 'volatilization_coefficient_for_cec',
                'ordering': [django.db.models.expressions.OrderBy(django.db.models.expressions.F('cec_from'), nulls_last=False), django.db.models.expressions.OrderBy(django.db.models.expressions.F('cec_to'), nulls_last=True)],
            },
        ),
        migrations.CreateModel(
            name='VolatilizationCoefficientForPH',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='identifier')),
                ('ph_from', models.FloatField(blank=True, null=True, verbose_name='from pH')),
                ('ph_to', models.FloatField(blank=True, null=True, verbose_name='to pH')),
                ('volatilization_coefficient', models.FloatField(verbose_name='volatilization coefficient')),
            ],
            options={
                'verbose_name': 'volatilization coefficient for pH',
                'verbose_name_plural': 'volatilization coefficients for pH',
                'db_table': 'volatilization_coefficient_for_ph',
                'ordering': [django.db.models.expressions.OrderBy(django.db.models.expressions.F('ph_from'), nulls_last=False), django.db.models.expressions.OrderBy(django.db.models.expressions.F('ph_to'), nulls_last=True)],
            },
        ),
        migrations.CreateModel(
            name='CropProfile',
            fields=[
                ('id', models.CharField(max_length=512, primary_key=True, serialize=False, verbose_name='identifier')),
                ('name', models.CharField(max_length=100, verbose_name='name')),
                ('latin_name', models.CharField(max_length=100, verbose_name='latin name')),
                ('is_active', models.BooleanField(default=True, help_text='Uncheck to disable', verbose_name='active?')),
                ('is_legume', models.BooleanField(verbose_name='legume?')),
                ('cycle', models.CharField(blank=True, choices=[('annual', 'Annual'), ('pluriannual', 'Pluriannual')], max_length=50, null=True, verbose_name='crop cycle')),
                ('cv', models.FloatField(verbose_name='CV')),
                ('harvest_product_name', models.CharField(max_length=50, verbose_name='name of harvested product')),
                ('harvest_index', models.FloatField(help_text='Ratio (10 to 99)', validators=[django.core.validators.MinValueValidator(10), django.core.validators.MaxValueValidator(99)], verbose_name='harvest index (HI)')),
                ('harvest_dry_matter', models.FloatField(help_text='Percentage (0 to 100)', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='harvest dry matter')),
                ('harvest_nitrogen_content', models.FloatField(help_text='Percentage of dry matter (0 to 100)', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='harvest nitrogen (N) content')),
                ('harvest_phosphorus_content', models.FloatField(help_text='Percentage of dry matter (0 to 100)', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='harvest phosphorus (P) content')),
                ('harvest_potassium_content', models.FloatField(help_text='Percentage of dry matter (0 to 100)', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='harvest potassium (K) content')),
                ('residues_product_name', models.CharField(blank=True, max_length=50, null=True, verbose_name='name of residues product')),
                ('residues_left_in_field', models.FloatField(help_text='Percentage (0 to 100)', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='part of residues left in the field')),
                ('residues_dry_matter', models.FloatField(blank=True, help_text='Percentage (0 to 100)', null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='residues dry matter')),
                ('residues_nitrogen_content', models.FloatField(blank=True, help_text='Percentage (0 to 100)', null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='residues average nitrogen (N) content')),
                ('residues_phosphorus_content', models.FloatField(blank=True, help_text='Percentage (0 to 100)', null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='residues phosphorus (P) content')),
                ('residues_potassium_content', models.FloatField(blank=True, help_text='Percentage (0 to 100)', null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='residues potassium (K) content')),
                ('group', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='crop_profiles', to='navigator_f3.cropprofilegroup', verbose_name='group')),
            ],
            options={
                'verbose_name': 'crop profile',
                'verbose_name_plural': 'crop profiles',
                'db_table': 'crop_profile',
                'ordering': ['name'],
            },
        ),
    ]
