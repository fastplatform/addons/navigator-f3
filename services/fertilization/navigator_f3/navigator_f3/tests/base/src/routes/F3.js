'use strict';

const router = require('express').Router();
const asyncHandler = require('express-async-handler');
const navBestFertiCtrl = require('../controllers/NavigatorBestFertilizerCtrl')();
const navF3Ctrl = require('../controllers/L3/NavigatorF3Ctrl')();
const navNutrient3Model =  new (require('../controllers/NavigatorModelCtrl'))();

const dispatcher = async input => {
	const liableFertilizers = navBestFertiCtrl.get(input.fertilizers);
	const applied = input.applied || [];
	input.prices &&
		liableFertilizers.forEach(fertilizer => {
			let row;
			(row = input.prices.find(row => row[0] == fertilizer.fertilizerID)) &&
				(fertilizer.price = row[2]/1000);
		});
	!input.applied &&
		(input.applied = []);
	input.fertilizers = [];
	let output = await navF3Ctrl.requeriments(input),
		N = Math.max(output.Ncrop_avg + output.Ndenitrification - output.Nc_mineralization_amendment, 0), 
		N_ur = Math.max(0.25*(output.Ncrop_avg + output.Ndenitrification) - applied.reduce((acc, fert) => acc + fert.amount*fert.N_ur, 0)/100, 0), 
		P = applied.reduce((acc, fert) => acc + fert.amount*fert.P, 0)/100,
		K = applied.reduce((acc, fert) => acc + fert.amount*fert.K, 0)/100;
		console.log(output);
	switch (input.PK_strategy) {
		case 'maximum-yield':
			P = Math.max(output.P_maxBM - P, 0);
			K = Math.max(output.K_maxBM - K, 0);
			break;
		case 'minimum-fertilizer':
			P = Math.max(output.P_minBM - P, 0);
			K = Math.max(output.K_minBM - K, 0);
			break;
		case 'sufficiency':
			P = Math.max(output.P_sufficiency - P, 0);
			K = Math.max(output.K_sufficiency - K, 0);
			break;
		case 'maintenance':
			P = Math.max(output.P_maintenance - P, 0);
			K = Math.max(output.K_maintenance - K, 0);
			break;
		default:
			break;
	}
	input.applied = applied;
	input.fertilizers = liableFertilizers;
	output = await navF3Ctrl.data4fertilizers(input);
	input.fertilizers = navBestFertiCtrl.bestCombination(output.updated_fertilizers, N, P, K, 0.0, N_ur);
	return await navF3Ctrl.requeriments(input);
};

module.exports.router = function () {

	router.get('/crops', asyncHandler(async (req, res) => {
		res.json({
			results: await navNutrient3Model.getCrops()
		});
	}));

	router.get('/crop/:cropID', asyncHandler(async (req, res) => {
		const cropID = req.body.cropID || req.params.cropID || req.query.cropID;
		res.json({
			results: await navNutrient3Model.crops.find(element => element.cropID === cropID)
		});
	}));

	router.get('/soil/types', asyncHandler(async (req, res) => {
		res.json({
			results: (await navNutrient3Model.getParmsTypeOfSoils()).map(element => {
				return {
					type: element.type,
					name: element.name,
				}
			}).filter(item => item.type !== 'none')
		});
	}));

	router.get('/pkstrategies', asyncHandler(async (req, res) => {
		res.json({
			results: await navNutrient3Model.getParmsStrategies()
		});
	}));

	router.get('/soil-textures', asyncHandler(async (req, res) => {
		res.json({
			results: await navNutrient3Model.getSoilTextures()
		});
	}));

	router.get('/soil-texture/:soilTextureID', asyncHandler(async (req, res) => {	
		const soil_texture = req.body.soilTextureID || req.params.soilTextureID || req.query.soilTextureID;
		res.json({
			results: await navNutrient3Model.getSoilTexture(soil_texture)
		});
	}));

	router.get('/climate-zones', asyncHandler(async (req, res) => {
		res.json({
			results: await navNutrient3Model.getClimaticZones()
		});
	}));

	router.get('/climate-zone/:climaticZoneID', asyncHandler(async (req, res) => {
		const climate_zone = req.body.climaticZoneID || req.params.climaticZoneID || req.query.climaticZoneID;
		res.json({
			results: await navNutrient3Model.getClimaticZone(climate_zone)
		});
	}));

	router.get('/fertilizers/all', asyncHandler(async (req, res) => {
		const names = typeof req.query.names === 'string' && req.query.names.split(',');
		res.json({
			results: navBestFertiCtrl.get(names, false)
		});
	}));

	router.get('/fertilizers/organics', asyncHandler(async (req, res) => {
		const names = typeof req.query.names === 'string' && req.query.names.split(',');
		res.json({
			results: navBestFertiCtrl.getOrganic(names, false)
		});
	}));

	router.get('/fertilizers/:fertilizerID', asyncHandler(async (req, res) => {
		const fertilizerID = req.params.fertilizerID || req.query.fertilizerID; 
		res.json({
			results: navBestFertiCtrl.getFertilizerID(fertilizerID)
		});
	}));

	router.get('/fertilizers/optimization', asyncHandler(async (req, res) => {
		const include = typeof req.query.include === 'string' && req.query.include.split(','),
			exclude = typeof req.query.exclude === 'string' && req.query.exclude.split(','),
			N = typeof req.query.N === 'string' && parseFloat(req.query.N) || 0.0,
			P = typeof req.query.P === 'string' && parseFloat(req.query.P) || 0.0,
			K = typeof req.query.K === 'string' && parseFloat(req.query.K) || 0.0,
			S = typeof req.query.S === 'string' && parseFloat(req.query.S) || 0.0,
			N_ur = typeof req.query.N_ur === 'string' && parseFloat(req.query.N_ur)*N || 0.0,
			fertilizers = navBestFertiCtrl.bestCombination(navBestFertiCtrl.get(include, exclude), N, P, K, S, N_ur);
		res.json({
			results: fertilizers,
			total: navBestFertiCtrl.aggregate(fertilizers)
		});
	}));

	router.post('/fertilizers/optimization', asyncHandler(async (req, res) => {
		const include = typeof req.body.include === 'object' && req.body.include.length !== undefined && req.body.include 
				|| typeof req.params.include === 'object' && req.params.include.length !== undefined && req.params.include,
			exclude = typeof req.body.exclude === 'object' && req.body.exclude.length !== undefined && req.body.exclude
				|| typeof req.params.exclude === 'object' && req.params.exclude.length !== undefined && req.params.exclude,
			N = req.body.N && parseFloat(req.body.N) || req.params.N && parseFloat(req.params.N) || 0.0,
			P = req.body.P && parseFloat(req.body.P) || req.params.P && parseFloat(req.params.P) || 0.0,
			K = req.body.K && parseFloat(req.body.K) || req.params.K && parseFloat(req.params.K) || 0.0,
			S = req.body.S && parseFloat(req.body.S) || req.params.S && parseFloat(req.params.S) || 0.0,
			N_ur = (req.body.N_ur && parseFloat(req.body.N_ur) || req.params.N_ur && parseFloat(req.params.N_ur))*N || 0.0,
			fertilizers = navBestFertiCtrl.bestCombination(navBestFertiCtrl.get(include, exclude), N, P, K, S, N_ur);
		console.log(navBestFertiCtrl.get(include, exclude))
		res.json({
			results: fertilizers,
			total: navBestFertiCtrl.aggregate(fertilizers)
		});
	}));

	router.post('/requirements', asyncHandler(async (req, res) => {
		const input = typeof req.body.input === 'object' && req.body.input || req.params.input && typeof req.params.input === 'object' || {};
		const output = await dispatcher(input);
		let N, P, K;
		N = output.Ncrop_avg;
		switch (input.PK_strategy) {
			case 'maximum-yield':
				P = output.P_maxBM;
				K = output.K_maxBM;
				break;
			case 'minimum-fertilizer':
				P = output.P_minBM;
				K = output.K_minBM;
				break;
			case 'sufficiency':
				P = output.P_sufficiency;
				K = output.K_sufficiency;
				break;
			case 'maintenance':
				P = output.P_maintenance;
				K = output.K_maintenance;
				break;
			default:
				break;
		}
		const results = [
			{
				drain_rate: output.drain_rate,
				fertilization: input.fertilizers, 
				...input,
				dose_irrigation: input.water_supply == '1' && input.dose_irrigation || 0,
				balance: {
					input: {
						Nmineralization: output.Nmineralization,
						Nfixation: output.Nfixation,
						Nwater: output.Nirrigation,
						NminInitial: output.Nc_s_initial,
						recommendedFertilizer: {
							N: input.fertilizers.reduce((acc, fert) => acc + fert.N, 0),
							P: input.fertilizers.reduce((acc, fert) => acc + fert.P, 0),
							K: input.fertilizers.reduce((acc, fert) => acc + fert.K, 0)
						},
						appliedFertilizer: {
							N: input.applied.reduce((acc, fert) => acc + fert.amount*(fert.N || 0)/100, 0),
							P: input.applied.reduce((acc, fert) => acc + fert.amount*(fert.P || 0)/100, 0),
							K: input.applied.reduce((acc, fert) => acc + fert.amount*(fert.K || 0)/100, 0)
						}
					},
					output: {
						Nleaching: output.Nleaching,
						Uptake : {
							N: output.Nuptake,
							P: P,
							K: K
						},
						Ndenitrification: output.Ndenitrification,
						NminPostharvest: output.Nc_s_end,
						Nvolatilization: output.Nvolatilization
					}
				}
			}
		];
		results[0].fertilizers = undefined;
		res.json({
			results: results
		});
	}));

	return router;
};

module.exports.dispatcher = dispatcher;