# Navigator Tool

The web-based Navigator Tool aims to perform modelling of nutrients recommendations, GHG emissions / removals and economic performance assessment (EPA), on a defined single crop and plot for a specified location, from a set of input data supplied by the user through an interface. This repository only covers nutrients part though.


- [Getting Started](#getting-started)
    - [Running Navigator Tool](#running-navigator-tool)
        - [Compatibility](#compatibility)
            - [Node.js](#nodejs)
        - [Locally](#locally)
        - [Connect an API](#connect-an-api)
- [Architecture](#architecture)
- [Configuration](#configuration)
- [Interface](#interface)
- [Modules](#modules)
   - [NPK](#npk)
   - [GHG](#ghg)
   - [EPA](#ghg)

# Getting Started

The fastest and easiest way to get started is to run Navigator Tool locally.

## Running Navigator Tool

Before you start make sure you have installed:

- [NodeJS](https://www.npmjs.com/) that includes `npm`

### Compatibility

#### Node.js
Navigator Tool has been tested with the most recent releases of Node.js to ensure compatibility. 

| Version    | Latest Version | End-of-Life Date | Compatibility      |
|------------|----------------|------------------|--------------------|
| Node.js 12 | 12.22.1        | April 2022       | ✅ Fully compatible |
| Node.js 14 | 14.16.1        | April 2023       | ✅ Fully compatible |
| Node.js 15 | 15.14.0        | June 2021        | ✅ Fully compatible |


### Locally

```bash
$ git clone https://github.com/tsig-idr/navigator-tool-F3
$ cd navigator-tool-F3
$ npm install
$ npm run dev
```

### Connect an API

The Navigator Tool services are available in REST API format that can be requested from a Web client, cli, mobile device, etc., following the HTTP Request Methods protocol: GET and POST.

Web Services available are:

#### NPK

| URL                   | Line | HTTP Verb | Funcionality |
|-----------------------|------|-----------|--------------|
| /F3/requirements      | 3    | POST      | Computes and returns NPK requirements as well as the best fertilization to meet those requirements |
| /F3/crops             | 3    | GET       | Returns data of all the crops available in the system |
| /F3/crop/:cropID      | 3    | GET       | Returns data of the crop identified by "cropID" |
| /F3/soil-textures     | 3    | GET       | Returns data of all the soil textures available in the system |
| /F3/fertilizers/all   | 3    | GET       | Returns data of all the fertilizers available in the system |
| /F3/climate-zones     | 3    | GET       | Returns data of all the climitic zones available in the system |

The input params which should be sent in the body of the POST request for that one service of "requirements" can be seen in this JSON:
```JSON
{
	"input": {
		"crop_type": "BARLEY_6_ROW",
		"soil_texture": "loam",
		"Pc_method": "olsen",
		"climatic_zone": "atlantic",
		"water_supply": "1",
		"type_irrigated": "sprinkler",
		"phosphorus_potassium_strategy": "maximum-yield",
		"tilled": "yes",
		"export_r": 100,
		"depth_s": 0.5,
		"HI_est": 40,
		"Nc_h": 2.3,
		"Pc_h": 0.36,
		"Kc_h": 0.49,
		"Pc_s": 10,
		"Kc_s": 10,
		"yield": 10000,
		"pH": 8,
		"CEC": 100,
		"CV": 20,
		"SOM": 1.8,
		"Nc_s_initial": 4,
		"Nc_end": 5,
		"dose_irrigation": 4000 ,
		"Nc_NO3_water": 25,
		"rain_a": 800,
		"rain_w": 480
	}
}
```

# Architecture:
Navigator Tool works with the Express web application framework that can run Node.js. The system architecture is based on client server through REST web services. Communication with the tool's computational core is based on text files and requests in JSON format. 

![Image of Architecture](https://github.com/tsig-idr/navigator-tool/blob/main/.github/architecture_navigator_tool.png)

# Configuration

Navigator Tool contains the boot configuration variables in the .env file. Edit them only if necessary.

```
PORT=1345
HOST=localhost
APP_ID=myAppIdNavigatorTool
```

# Interface

Warning: This is not the final user interface of the Navigator Tool

Currently the interface is a prototype of a web interface developed in "vanilla". It is a simple interface aimed at programmers and technicians to interact with the Services API. . The interface allows the NPK, GHG and EPA models to be tested independently. In the top menu you can navigate to the active modules.

Web live: http://navigator-dev.teledeteccionysig.es

# Modules

## NPK 

Nutrient requirements calculation module: Nitrogen, Phosphorus and potassium, based on 4 lines of complexity.

* F1 (not available): Daily nutrient balance. Spatial and temporal variability. FATIMA/AgriSAT Sensor monitoring.
Parcel and crop characteristics.

* F3: Empirical models. ITAP. Crop cycle nutrient balance. parcel and crop characteristics.

* F4 (not available): Same as F3 but with simplified user inputs.

## GHG (not available)

The greenhouse gas emissions (E) at farm level are calculated based on the following equation: E = AD·EF where AD is the activity data and EF the emission factor.

* G3: The farmer provides additional qualitative information which are coupled with respective default emission factors. In addition, country specific emission factors are used which are derived from the National Inventory Reports (NIR). Qualitative information can be the method of management (e.g. geographic stratifications, no till practice, type of manure management, organic / conventional farming, fertilizer type, animal characterization, housing facilities (e.g. freestall barns with solid floors, barns with slatted floors)

## EPA (not available)

Economic performance will consider the management of activities and take into account GHG emissions/removals, the use of fertilisers accounting, as well as all potential costs and revenues

* E3: Match with specific information (arising from on farm survey) about the nutrient, water requirements of crops and yield to assess a first proxy of efficiency (environmental and economic performance) of pilot farms. Data will be collected either using FADN (when available) or with a specific survey, which will include a visit of the farm, gathering of data with questionnaires, data analysis with dedicated tool, a report and recommendations followed by a business plan

