{
	"input": {
		"crop_type": "BARLEY_6_ROW",
		"yield": 10000,
		"soil_texture": "loam",
		"Pc_method": "olsen",
		"climatic_zone": "atlantic",
		"water_supply": "1",
		"type_irrigated": "sprinkler",
		"PK_strategy": "maximum-yield",
		"tilled": "yes",
		"residues_exported": 100,
		"soil_depth": 0.5,
		"harvest_index": 40,
		"harvest_nitrogen_content": 2.3,
		"harvest_phosphorus_content": 0.36,
		"harvest_potassium_content": 0.49,
		"soil_phosphorus_content": 10,  # Pc_s_0
		"soil_potassium_content": 10,  # Kc_s_0
		
		"pH": 8,
		"CEC": 100,
		"cv": 20,
		"soil_organic_matter_content": 1.8,
		"soil_nitrogen_content_initial": 4,  # Nc_s_0
		"soil_nitrogen_content_final": 5,  # Nc_s_n
		"irrigation_dose": 4000 ,
		"irrigation_nitrogen_no3_content": 25,
		"rainfall_annual": 800,
		"rainfall_autumn_winter": 480
	}
}

FNR = 0.1
FMC_R = 0.15
P_CROP_MAX = 100
K_CROP_MAX = 275

T_50 = 0
T_20 = -0.84
T_80 = 0.84

CropData = SP_CSV2ARRAY (CONCAT ('sheetscript/F3/', 'CropData.csv'))
SoilData = SP_CSV2ARRAY (CONCAT ('sheetscript/F3/', 'SoilData.csv'))
Nmineralization_SOM = SP_CSV2ARRAY (CONCAT ('sheetscript/F3/', 'Nmineralization_SOM.csv'))
Pc_method_table = SP_CSV2ARRAY (CONCAT ('sheetscript/F3/', 'Pc_method_table.csv'))
Clima = SP_CSV2ARRAY (CONCAT ('sheetscript/F3/', 'Clima.csv'))
n_fix_per = SP_CSV2ARRAY (CONCAT ('sheetscript/F3/', 'n_fix_per.csv'))
Drainage = SP_CSV2ARRAY (CONCAT ('sheetscript/F3/', 'Drainage.csv'))
Fertilizers = SP_CSV2ARRAY (CONCAT ('sheetscript/F3/', 'Fertilizers.csv'))
Fertilizers_aux = SP_CSV2ARRAY (CONCAT ('sheetscript/F3/', 'Fertilizers_aux.csv'))
pH4vol = SP_CSV2ARRAY (CONCAT ('sheetscript/F3/', 'pH4vol.csv'))
CEC4vol = SP_CSV2ARRAY (CONCAT ('sheetscript/F3/', 'CEC4vol.csv'))

----- FERTILIZERS -----

vol_c = EXP (IF (water_supply == '0'; 0-0.045; 0) + VLOOKUP (pH; pH4vol; 2; 1) + VLOOKUP (CEC/10; CEC4vol; 2; 1) + 0-0.402)

nitrogen_mineralization_from_fertilization = 0
nitrogen_total_losses_volatilization = nitrogen_total_losses_denitrification = 0
n = LEN (fertilizers)
i = 0
while i < n then begin '{'
	fertilizer = GET (fertilizers, i)
	fertilizer_id = GET (fertilizer, 'fertilizerID')
	fertilization.quantity = GET (fertilizer, 'fertilization.quantity')
	fertilizer.nitrogen_organic_dry_matter_content = IF_ERROR (VLOOKUP (fertilizer_id; Fertilizers; 14); 0)
	fertilizer.nitrogen_content = IF (fertilizer.is_organic == 'Inorganic'; fertilizer.nitrogen_total_content; fertilizer.nitrogen_organic_dry_matter_content)
	incorporation_method = VLOOKUP (fertilizer_id; Fertilizers_aux; 2)
	nitrogen_volatilization_losses = EXP (IF (incorporation_method == 'incorporated'; 0-1.895; IF (incorporation_method == 'topdressing'; 0-1.305; 0)) + fertilizer.volatilization_coefficient)*vol_c
	nitrogen_bf_volatilization = fertilizer.nitrogen_content*(1 - nitrogen_volatilization_losses)
	nitrogen_denitrification_losses = 0
	nitrogen_bf_denitrification = fertilizer.nitrogen_content*(1 - nitrogen_denitrification_losses)
	nitrogen_bf = IF_ERROR (nitrogen_bf_volatilization*nitrogen_bf_denitrification/fertilizer.nitrogen_content; 0)
	fertilizer_application_frequency = 1.0
	nitrogen_mineralization_from_fertilization = nitrogen_mineralization_from_fertilization + nitrogen_bf*fertilization.quantity*fertilizer_application_frequency
	nitrogen_raw = nitrogen_bf*fertilization.quantity/(1 - nitrogen_volatilization_losses - nitrogen_denitrification_losses)
	nitrogen_losses_volatilization = nitrogen_raw*nitrogen_volatilization_losses
	nitrogen_total_losses_volatilization = nitrogen_total_losses_volatilization + nitrogen_losses_volatilization
	nitrogen_total_losses_denitrification = nitrogen_total_losses_denitrification + (nitrogen_raw - nitrogen_losses_volatilization)*nitrogen_denitrification_losses
	i = i + 1
'}' end
m = LEN (applied)
j = 0
while j < m then begin '{'
	fertilizer = GET (applied, j)
	fertilizer_id = GET (fertilizer, 'fertilizerID')
	fertilization.quantity = GET (fertilizer, 'fertilization.quantity')
	fertilizer.is_organic = GET (fertilizer, 'type')
	method = GET (fertilizer, 'method')
	frequency = GET (fertilizer, 'frequency')
	Nc_i = GET (fertilizer, 'N')/100
	fertilizer.volatilization_coefficient = IF_ERROR (VLOOKUP (fertilizer_id; Fertilizers; 6); IF (fertilizer.is_organic == 'Organic'; 0.995; 0))
	nitrogen_volatilization_losses = EXP (IF (method == 'incorporated'; 0-1.895; IF (method == 'topdressing'; 0-1.305; 0)) + fertilizer.volatilization_coefficient)*vol_c
	nitrogen_bf_volatilization = Nc_i*(1 - nitrogen_volatilization_losses)
	nitrogen_denitrification_losses = 0
	nitrogen_bf_denitrification = Nc_i*(1 - nitrogen_denitrification_losses)
	nitrogen_bf = IF_ERROR (nitrogen_bf_volatilization*nitrogen_bf_denitrification/Nc_i; 0)
	fertilizer_application_frequency = IF (fertilizer.is_organic == 'Inorganic'; 1.0; IF (frequency == 'annual'; 1.0; 0.5))
	nitrogen_mineralization_from_fertilization = nitrogen_mineralization_from_fertilization + nitrogen_bf*fertilization.quantity*fertilizer_application_frequency
	nitrogen_raw = nitrogen_bf*fertilization.quantity/(1 - nitrogen_volatilization_losses - nitrogen_denitrification_losses)
	nitrogen_losses_volatilization = nitrogen_raw*nitrogen_volatilization_losses
	nitrogen_total_losses_volatilization = nitrogen_total_losses_volatilization + nitrogen_losses_volatilization
	nitrogen_total_losses_denitrification = nitrogen_total_losses_denitrification + (nitrogen_raw - nitrogen_losses_volatilization)*nitrogen_denitrification_losses
	j = j + 1
'}' end


harvest_dry_matter_med_20 = yield * harvest_dry_matter * (1 + cv * T_20)
harvest_dry_matter_med_50 = yield * harvest_dry_matter * (1 + cv * T_50)
harvest_dry_matter_med_80 = yield * harvest_dry_matter * (1 + cv * T_80)
residues_dry_matter_med_50 = (harvest_dry_matter_med_50 * (1 - harvest_index)/harvest_index)

----- PHOSPHORUS -----

phosphorus_exported = harvest_dry_matter_med_50 * harvest_phosphorus_content + residues_dry_matter_med_50 * (1 - FMC_R) * residues_phosphorus_content * residues_exported

soil_phosphorus_content_corrected = soil_phosphorus_content * soil_phosphorus_analysis_method.value
phosphorus_content_threshold_min = VLOOKUP (soil_texture; SoilData; 25)
soil_phosphorus_content_stl_tmin = IF (soil_phosphorus_content_corrected < phosphorus_content_threshold_min; 1; 0)
soil_phosphorus_content_2stl_tmin = IF (soil_phosphorus_content_corrected > 2*phosphorus_content_threshold_min; 0.5; 1)
crop_phosphorus_min = phosphorus_exported * soil_phosphorus_content_2stl_tmin + 10 * density * soil_depth * (phosphorus_content_threshold_min - soil_phosphorus_content_corrected) * soil_phosphorus_content_stl_tmin
crop_phosphorus_years_min = IF (crop_phosphorus_min > P_CROP_MAX; CEIL (crop_phosphorus_min/P_CROP_MAX); 1)
crop_phosphorus_max = phosphorus_exported * soil_phosphorus_content_2stl_tmax + 10 * density * soil_depth * (phosphorus_content_threshold_max - soil_phosphorus_content_corrected) * soil_phosphorus_content_stl_tmax
crop_phosphorus_years_max = IF (crop_phosphorus_max > P_CROP_MAX; CEIL (crop_phosphorus_max/P_CROP_MAX); 1)
soil_phosphorus_content_stl_tmax = IF (soil_phosphorus_content_corrected < phosphorus_content_threshold_max; 1; 0)
soil_phosphorus_content_2stl_tmax = IF (soil_phosphorus_content_corrected > 2*phosphorus_content_threshold_max; 0.5; 1)

phosphorus_sufficiency = 10 * density * soil_depth * (phosphorus_content_threshold_min - soil_phosphorus_content_corrected) * soil_phosphorus_content_stl_tmin / crop_phosphorus_years_min
phosphorus_minimum_fertilizer = (phosphorus_exported * soil_phosphorus_content_2stl_tmin + 10 * density * soil_depth*(phosphorus_content_threshold_min - soil_phosphorus_content_corrected) * soil_phosphorus_content_stl_tmin) / crop_phosphorus_years_min
phosphorus_maximum_yield = (phosphorus_exported * soil_phosphorus_content_2stl_tmax + 10 * density * soil_depth*(phosphorus_content_threshold_max - soil_phosphorus_content_corrected) * soil_phosphorus_content_stl_tmax) / crop_phosphorus_years_max
phosphorus_maintenance = phosphorus_exported

p2o5_sufficiency = 0
p2o5_minimum_fertilizer = phosphorus_minimum_fertilizer * 2.293
p2o5_maximum_yield = phosphorus_maximum_yield * 2.293
p2o5_maintenance = phosphorus_maintenance * 2.293

----- POTASSIUM -----

potassium_exported = harvest_dry_matter_med_50*harvest_potassium_content + residues_dry_matter_med_50 * (1 - FMC_R) * residues_potassium_content * residues_exported

soil_potassium_content_stl_tmin = IF (soil_potassium_content < potassium_content_threshold_min; 1; 0)
soil_potassium_content_2stl_tmin = IF (soil_potassium_content > 2*potassium_content_threshold_min; 0.5; 1)
crop_potassium_min = potassium_exported*soil_potassium_content_2stl_tmin + 10 * density * soil_depth*(potassium_content_threshold_min - soil_potassium_content)*fk*soil_potassium_content_stl_tmin
crop_potassium_years_min = IF (crop_potassium_min > K_CROP_MAX; CEIL (crop_potassium_min/K_CROP_MAX); 1)
soil_potassium_content_stl_tmax = IF (soil_potassium_content < potassium_content_threshold_max; 1; 0)
soil_potassium_content_2stl_tmax = IF (soil_potassium_content > 2*potassium_content_threshold_max; 0.5; 1)
crop_potassium_max = potassium_minimum_fertilizer*soil_potassium_content_2stl_tmax + 10 * density * soil_depth*(potassium_content_threshold_max - soil_potassium_content)*fk*soil_potassium_content_stl_tmax
crop_potassium_years_max = IF (crop_potassium_max > K_CROP_MAX; CEIL (crop_potassium_max/K_CROP_MAX); 1)

potassium_sufficiency = 10 * density * soil_depth*(potassium_content_threshold_min - soil_potassium_content)*fk*soil_potassium_content_stl_tmin/crop_potassium_years_min
potassium_minimum_fertilizer = (potassium_exported*soil_potassium_content_2stl_tmin + 10 * density * soil_depth*(potassium_content_threshold_min - soil_potassium_content)*fk*soil_potassium_content_stl_tmin)/crop_potassium_years_min
potassium_maximum_yield = (potassium_exported*soil_potassium_content_2stl_tmax + 10 * density * soil_depth*(potassium_content_threshold_max - soil_potassium_content)*fk*soil_potassium_content_stl_tmax)/crop_potassium_years_max
potassium_maintenance = potassium_exported

k2o_sufficiency = 0
k2o_minimum_fertilizer = potassium_minimum_fertilizer * 1.205
k2o_maximum_yield = potassium_maximum_yield * 1.205
k2o_maintenance = potassium_maintenance * 1.205

----- MINERALIZATION -----

humidity_factor = IF_ERROR (VLOOKUP (climatic_zone; Clima; 2); 1.0)
idx = MATCH (soil_organic_matter_content; [0, 0.5, 1, 1.5, 2, 2.5]; 1)  # 1 to 6
soil_texture_agroasesor = VLOOKUP (soil_texture; SoilData; 2)  # 1, 2, 3
nitrogen_mineralization = GET (GET (Nmineralization_SOM, idx), MATCH (soil_texture_agroasesor; [1, 2, 3]))*humidity_factor

----- FIXATION -----

n_fix_per = IF (not crop_profile.is_legume; 0; VLOOKUP (CONCAT (n_fix_code; CONCAT (IF (soil_organic_matter_content <= 3; '<=3'; '>3'); VLOOKUP (crop_type; CropData; 8))); n_fix_per; 2))
yield_dry_matter = yield * harvest_dry_matter
residues_dry_matter = yield_dry_matter * (1 - harvest_index) / harvest_index
yield_nitrogen = yield_dry_matter*harvest_nitrogen_content
residues_nitrogen = residues_dry_matter * residues_nitrogen_content_typical
nitrogen_fixation = IF (not crop_profile.is_legume; 10; (1 + FNR)*(yield_nitrogen + residues_nitrogen)*n_fix_per)

----- IRRIGATION -----

if irrigation_method == trickle:
	irrigation_factor = 0.9
IF irrigation_method == sprinkler;
	irrigation_factor = 0.85
IF irrigation_method == surface:
 	irrigation_factor = 0.7
ELSE:
	irrigation_factor = 0

if water_supply_method == rainfed:
	nitrogen_irrigation = 0
ELSE:
	nitrogen_irrigation = irrigation_nitrogen_no3_content * irrigation_dose * irrigation_factor * 22.6/100000)

----- LEACHING -----
PI = (rainfall_annual - 10160/carbon_nitrogen_ratio + 101.6)**2/(rainfall_annual + 15240/carbon_nitrogen_ratio - 152.4)
SI = ((2*rainfall_autumn_winter)/rainfall_annual)**(1/3)
LI = PI*SI
nitrogen_leaching = soil_nitrogen_content_initial*(1 - EXP ((0-LI)/(soil_depth*1000*qfc_qwp_avg)))

----- UPTAKE -----

nitrogen_uptake = (harvest_dry_matter_med_50 * harvest_nitrogen_content + residues_dry_matter_med_50 * residues_nitrogen_content_typical) * (1 + FNR)
nitrogen_uptake_min = (harvest_dry_matter_med_20*harvest_nitrogen_content + (harvest_dry_matter_med_20*(1 - harvest_index)/harvest_index) * residues_nitrogen_content_typical)*(1 + FNR)
nitrogen_uptake_max = (harvest_dry_matter_med_80*harvest_nitrogen_content + (harvest_dry_matter_med_80*(1 - harvest_index)/harvest_index) * residues_nitrogen_content_typical)*(1 + FNR)

----- DENITRIFICATION -----

output_avg = SUM (nitrogen_leaching; nitrogen_uptake; soil_nitrogen_content_final)
input_avg = input_min = input_max = SUM (nitrogen_mineralization; nitrogen_fixation; nitrogen_irrigation; soil_nitrogen_content_initial)
Ncrop_avg = MAX (output_avg - input_avg; 0)
nitrogen_denitrification = 0.34*E**(0.012*Ncrop_avg)

----- VOLATILIZATION -----

nitrogen_volatilization = nitrogen_total_losses_volatilization
