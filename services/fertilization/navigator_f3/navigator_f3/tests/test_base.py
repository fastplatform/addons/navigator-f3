"""Test suite to compare the FaST Python implementation to the
base algorithm in Javascript/sheetscript
"""
import subprocess
from pathlib import Path
import json
import random
import os
from unittest.signals import installHandler

from django.test import TestCase
from constance import config

from navigator_f3.lib.algorithm import (
    NavigatorF3Algorithm,
    NavigatorF3ResultInputs,
    NavigatorF3ResultOutputs,
    Fertilization,
)

from navigator_f3.models import (
    CropProfile,
    Fertilizer,
    PhosphorusPotassiumStrategy,
    Soil,
    PhosphorusAnalysisMethod,
    ClimateZone,
    WaterSupplyMethod,
    IrrigationMethod,
    FertilizerApplicationFrequency,
)


TESTS_DIR = Path(__file__).parent


class BaseTestCase(TestCase):
    """This test case compares our implementation of Navigator F3 versus the original
    implementation in Node.JS

    Note: to be able to run, Node.JS >= 11 must be installed and on the $PATH
    """

    ITERATIONS = 100

    def setUp(self) -> None:
        super().setUp()

        # Empty the database and fill it with our inits
        env = os.environ.copy()
        env.update({"POSTGRES_DATABASE": "test_" + env["POSTGRES_DATABASE"]})
        subprocess.run(["sh", "./scripts/clear_data.sh"], env=env)
        subprocess.run(["sh", "./scripts/init_data.sh"], env=env)

    def _generate_random_input(self):
        """Generate random inputs to test against the base algorithm and
        ours

        Returns:
            dict: input with random parameters
        """

        # Select a random crop
        crop_profile = random.choice(CropProfile.objects.filter(is_active=True).all())
        crop_yield = round(random.uniform(0, 10000))

        # Randomly override some of the crop parameters
        # (these are optional inputs from the mutation)
        if random.choice([True, False]):
            crop_profile.residues_left_in_field = round(random.uniform(0, 100))

        if random.choice([True, False]):
            crop_profile.harvest_index = round(random.uniform(10, 99))

        if random.choice([True, False]):
            crop_profile.cv = round(random.uniform(0, 100))

        if random.choice([True, False]):
            crop_profile.harvest_nitrogen_content = round(random.uniform(0, 10), 1)

        if random.choice([True, False]):
            crop_profile.harvest_phosphorus_content = round(random.uniform(0, 1), 2)

        if random.choice([True, False]):
            crop_profile.harvest_potassium_content = round(random.uniform(0, 1), 2)

        # Select a random soil and random soil properties
        soil = random.choice(Soil.objects.filter(is_active=True).all())
        soil_ph = round(random.uniform(4, 10), 1)
        soil_depth = round(random.uniform(0.1, 1), 2)
        soil_organic_matter_content = round(random.uniform(0, 10), 1)
        soil_nitrogen_content_initial = round(random.uniform(0, 30))
        soil_nitrogen_content_final = round(random.uniform(0, 30))
        soil_phosphorus_analysis_method = random.choice(
            PhosphorusAnalysisMethod.objects.filter(is_active=True).all()
        )
        soil_phosphorus_content = round(random.uniform(0, 100))
        soil_potassium_content = round(random.uniform(0, 100))

        if random.choice([True, False]):
            soil.cec = round(random.uniform(0, 500), 1)

        # Select a random climate and random climate properties
        climate_zone = random.choice(ClimateZone.objects.filter(is_active=True).all())
        if random.choice([True, False]):
            climate_zone.rainfall_annual = round(random.uniform(200, 2000), -1)

        if random.choice([True, False]):
            climate_zone.rainfall_autumn_winter = round(
                random.uniform(0, climate_zone.rainfall_annual), -1
            )

        water_supply_method = random.choice([m.value for m in WaterSupplyMethod])
        if water_supply_method == WaterSupplyMethod.irrigated:
            irrigation_method = random.choice([m.value for m in IrrigationMethod])
            irrigation_dose = round(random.uniform(0, 10000), -2)
            irrigation_nitrogen_no3_content = round(random.uniform(0, 100))
        else:
            irrigation_method = None
            irrigation_dose = None
            irrigation_nitrogen_no3_content = None

        input = {
            "crop_profile": crop_profile,
            "phosphorus_potassium_strategy": random.choice(
                [s.value for s in PhosphorusPotassiumStrategy]
            ),
            "crop_yield": crop_yield,
            "soil": soil,
            "soil_ph": soil_ph,
            "soil_organic_matter_content": soil_organic_matter_content,
            "soil_depth": soil_depth,
            "soil_nitrogen_content_initial": soil_nitrogen_content_initial,
            "soil_nitrogen_content_final": soil_nitrogen_content_final,
            "soil_phosphorus_analysis_method": soil_phosphorus_analysis_method,
            "soil_phosphorus_content": soil_phosphorus_content,
            "soil_potassium_content": soil_potassium_content,
            "climate_zone": climate_zone,
            "water_supply_method": water_supply_method,
            "irrigation_method": irrigation_method,
            "irrigation_dose": irrigation_dose,
            "irrigation_nitrogen_no3_content": irrigation_nitrogen_no3_content,
            "fertilizations": [],
            "additional_fertilizations": [],
        }

        for _ in range(random.choice([0, 1, 2, 3])):
            input["fertilizations"].append(
                Fertilization(
                    fertilizer=random.choice(
                        Fertilizer.objects.filter(is_active=True).all()
                    ),
                    quantity=round(random.uniform(0, 500), -1),
                    application_frequency=random.choice(
                        [f.value for f in FertilizerApplicationFrequency]
                    ),
                )
            )

        return input

    def _run_base(self, input):
        """Run our inputs against the base version (in nodejs) of the algorithm
        and return the output (reformatted to FaST types)
        """

        # Map the FaST inputs to the nodejs version input names
        js_input = {
            "crop_type": input["crop_profile"].id,
            "soil_texture": input["soil"].id,
            "Pc_method": input["soil_phosphorus_analysis_method"].id,
            "climatic_zone": input["climate_zone"].id,
            "water_supply": "0"
            if input["water_supply_method"] == WaterSupplyMethod.rainfed
            else "1",
            "type_irrigated": input["irrigation_method"],
            "PK_strategy": input["phosphorus_potassium_strategy"].replace("_", "-"),
            "tilled": "whatever",  # this is not used by the algorithm anyway
            "export_r": 100 - input["crop_profile"].residues_left_in_field,
            "depth_s": input["soil_depth"],
            "HI_est": input["crop_profile"].harvest_index,
            "Nc_h": input["crop_profile"].harvest_nitrogen_content,
            "Pc_h": input["crop_profile"].harvest_phosphorus_content,
            "Kc_h": input["crop_profile"].harvest_potassium_content,
            "Pc_s": input["soil_phosphorus_content"],
            "Kc_s": input["soil_potassium_content"],
            "yield": input["crop_yield"],
            "pH": input["soil_ph"],
            "CEC": input["soil"].cec,
            "CV": input["crop_profile"].cv,
            "SOM": input["soil_organic_matter_content"],
            "Nc_s_initial": input["soil_nitrogen_content_initial"],
            "Nc_end": input["soil_nitrogen_content_final"],
            "dose_irrigation": input["irrigation_dose"],
            "Nc_NO3_water": input["irrigation_nitrogen_no3_content"],
            "rain_a": input["climate_zone"].rainfall_annual,
            "rain_w": input["climate_zone"].rainfall_autumn_winter,
            "applied": [],
            "fertilizers": [],
        }

        for fertilization in input["fertilizations"]:
            js_input["fertilizers"].append(
                {
                    "fertilizerID": fertilization.fertilizer.id,
                    "amount": fertilization.quantity,
                    "frequency": fertilization.application_frequency,
                    "method": fertilization.fertilizer.incorporation_method,
                    "clasification_fm": "Organic"
                    if fertilization.fertilizer.is_organic
                    else "Non-organic",
                    "N": fertilization.fertilizer.nitrogen_total_content,
                }
            )

        # Run the nodejs algorithm on the inputs, by running a separate process
        # and reading the stdout
        result = subprocess.check_output(
            [
                "node",
                "-e",
                """
                async function run() {{
                    process.chdir('./navigator_f3/tests/base');
                    let requeriments = require('./navigator_f3/tests/base/src/controllers/L3/NavigatorF3Ctrl.js')().requeriments;
                    let fertilizers = require('./navigator_f3/tests/base/src/controllers/NavigatorBestFertilizerCtrl.js')();
                    let input = {input};
                    input.fertilizers = input.fertilizers.map(function(f) {{
                        let fert = fertilizers.getFertilizerID(f.fertilizerID);
                        f.N = f.N || fert.nitrogen.Ncf;
                        f.P = fert.phosphorus.Pcf;
                        f.K = fert.potassium.Kcf;
                        return f
                    }})
                    let output = await requeriments(input);
                    output["fertilizers"] = {{}};
                    output["fertilizers"]["N"] = input.fertilizers.reduce((acc, fert) => acc + fert.amount*(fert.N || 0)/100, 0);
                    output["fertilizers"]["P"] = input.fertilizers.reduce((acc, fert) => acc + fert.amount*(fert.P || 0)/100, 0);
                    output["fertilizers"]["K"] = input.fertilizers.reduce((acc, fert) => acc + fert.amount*(fert.K || 0)/100, 0);
                    console.log(JSON.stringify({{input: input, output: output}}));
                }}
                run();
                """.format(
                    input=json.dumps(js_input)
                ),
            ]
        )
        result = json.loads(result)

        # Map the output of the nodejs algorithm to FaST semantics
        js_input = result["input"]
        js_output = result["output"]

        if result["output"]["Ndenitrification"] > 100000:
            print(result['input'])

        if (
            input["phosphorus_potassium_strategy"]
            == PhosphorusPotassiumStrategy.maximum_yield
        ):
            P = js_output["P_maxBM"]
            K = js_output["K_maxBM"]
        elif (
            input["phosphorus_potassium_strategy"]
            == PhosphorusPotassiumStrategy.minimum_fertilizer
        ):
            P = js_output["P_minBM"]
            K = js_output["K_minBM"]
        elif (
            input["phosphorus_potassium_strategy"]
            == PhosphorusPotassiumStrategy.sufficiency
        ):
            P = js_output["P_sufficiency"]
            K = js_output["K_sufficiency"]
        elif (
            input["phosphorus_potassium_strategy"]
            == PhosphorusPotassiumStrategy.maintenance
        ):
            P = js_output["P_maintenance"]
            K = js_output["K_maintenance"]

        return {
            "inputs": NavigatorF3ResultInputs(
                nitrogen_mineralization=js_output["Nmineralization"],
                nitrogen_fixation=js_output["Nfixation"],
                nitrogen_irrigation=js_output["Nirrigation"],
                nitrogen_in_soil_initial=js_output["Nc_s_initial"],
                nitrogen_fertilizers=js_output["fertilizers"]["N"],
                phosphorus_fertilizers=js_output["fertilizers"]["P"],
                potassium_fertilizers=js_output["fertilizers"]["K"],
            ),
            "outputs": NavigatorF3ResultOutputs(
                nitrogen_leaching=js_output["Nleaching"],
                nitrogen_denitrification=js_output["Ndenitrification"],
                nitrogen_in_soil_final=js_output["Nc_s_end"],
                nitrogen_volatilization=js_output["Nvolatilization"],
                nitrogen_uptake=js_output["Nuptake"],
                phosphorus_uptake=P,
                potassium_uptake=K,
            ),
        }

    def _run_ours(self, input):
        """Run our inputs against our version of the algorithm"""
        algorithm = NavigatorF3Algorithm(config=config, **input)
        algorithm.compute()
        return {"inputs": algorithm.inputs, "outputs": algorithm.outputs}

    def test_base(self):
        """The actual test: run the same process X times:
        - generate random input set
        - compute the expected output (using the base algorithm in nodejs)
        - compute our own output (using our lib)
        - compare each items of both outputs for equality
        """

        for iteration in range(self.ITERATIONS):
            print(f"Iteration: {iteration}/{self.ITERATIONS}")

            input = self._generate_random_input()
            our_output = self._run_ours(input)
            expected_output = self._run_base(input)

            fields_inputs = [
                "nitrogen_mineralization",
                "nitrogen_fixation",
                "nitrogen_irrigation",
                "nitrogen_in_soil_initial",
                "nitrogen_fertilizers",
                "phosphorus_fertilizers",
                "potassium_fertilizers",
            ]

            for field in fields_inputs:
                with self.subTest(iteration=iteration + 1, field=field, **input):
                    self.assertAlmostEquals(
                        getattr(our_output["inputs"], field),
                        getattr(expected_output["inputs"], field),
                        places=2,
                        msg=f"{field}: failed with {input}",
                    )

            fields_outputs = [
                "nitrogen_leaching",
                "nitrogen_denitrification",
                "nitrogen_in_soil_final",
                "nitrogen_uptake",
                "phosphorus_uptake",
                "potassium_uptake",
                "nitrogen_volatilization",
            ]

            for field in fields_outputs:
                with self.subTest(iteration=iteration + 1, field=field, **input):
                    # print(
                    #     field,
                    #     getattr(our_output["outputs"], field),
                    #     getattr(expected_output["outputs"], field),
                    # )
                    self.assertAlmostEquals(
                        getattr(our_output["outputs"], field),
                        getattr(expected_output["outputs"], field),
                        places=2,
                        msg=f"{field}: failed with {input}",
                    )

    def tearDown(self) -> None:
        super().tearDown()
