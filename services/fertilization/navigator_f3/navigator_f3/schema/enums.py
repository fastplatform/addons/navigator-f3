import logging

import graphene
from django.utils import translation
from django.conf import settings

from navigator_f3.models import (
    FertilizerApplicationFrequency,
    FertilizerIncorporationMethod,
    IrrigationMethod,
    PhosphorusPotassiumStrategy,
    IrrigationMethod,
    WaterSupplyMethod,
)

logger = logging.getLogger(__name__)


class FertilizerApplicationFrequencyEnum(graphene.Enum):
    annual = "annual"
    biennial = "biennial"

    @property
    def description(self):
        with translation.override(settings.LANGUAGE_CODE):
            choices = {v: str(l) for v, l in FertilizerApplicationFrequency.choices}
            return choices[self.value]

    class Meta:
        name = "navigator_f3__fertilization_application_frequency_enum"
        description = "Fertilizer application frequency"


class FertilizerIncorporationMethodEnum(graphene.Enum):
    incorporated = "incorporated"
    topdressing = "topdressing"

    @property
    def description(self):
        with translation.override(settings.LANGUAGE_CODE):
            choices = {v: str(l) for v, l in FertilizerIncorporationMethod.choices}
            return choices[self.value]

    class Meta:
        name = "navigator_f3__fertilization_incorporation_method_enum"
        description = "Fertilizer incorporation method"


class WaterSupplyMethodEnum(graphene.Enum):
    rainfed = "rainfed"
    irrigated = "irrigated"

    @property
    def description(self):
        with translation.override(settings.LANGUAGE_CODE):
            choices = {v: str(l) for v, l in WaterSupplyMethod.choices}
            return choices[self.value]

    class Meta:
        name = "navigator_f3__water_supply_method_enum"
        description = "Water supply method"


class IrrigationMethodEnum(graphene.Enum):
    trickle = "trickle"
    sprinkler = "sprinkler"
    surface = "surface"

    @property
    def description(self):
        with translation.override(settings.LANGUAGE_CODE):
            choices = {v: str(l) for v, l in IrrigationMethod.choices}
            return choices[self.value]

    class Meta:
        name = "navigator_f3__irrigation_method_enum"
        description = "Irrigation method"


class PhosphorusPotassiumStrategyEnum(graphene.Enum):
    sufficiency = "sufficiency"
    maintenance = "maintenance"
    maximum_yield = "maximum_yield"
    minimum_fertilizer = "minimum_fertilizer"

    @property
    def description(self):
        with translation.override(settings.LANGUAGE_CODE):
            choices = {v: str(l) for v, l in PhosphorusPotassiumStrategy.choices}
            return choices[self.value]

    class Meta:
        name = "navigator_f3__phosphorus_potassium_strategy_enum"
        description = "Phosphorus-potassium strategy"
