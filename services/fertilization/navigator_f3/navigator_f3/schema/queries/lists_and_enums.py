import logging
import json
import hashlib

import graphene
from django.conf import settings

from constance import config

from navigator_f3.models import (
    ClimateZone,
    CropProfileGroup,
    CropProfile,
    Fertilizer,
    PhosphorusAnalysisMethod,
    SoilMineralizationProfile,
    Soil,
)

from navigator_f3.schema.types import (
    ConfigType,
    ClimateZoneType,
    CropProfileGroupType,
    CropProfileType,
    PhosphorusAnalysisMethodType,
    SoilMineralizationProfileType,
    SoilType,
    FertilizerType,
)

from navigator_f3.schema.enums import (
    FertilizerApplicationFrequencyEnum,
    FertilizerIncorporationMethodEnum,
    WaterSupplyMethodEnum,
    IrrigationMethodEnum,
    PhosphorusPotassiumStrategyEnum,
)

logger = logging.getLogger(__name__)


class Query:
    navigator_f3__config = graphene.Field(ConfigType, description="Configuration")
    navigator_f3__climate_zone = graphene.List(
        ClimateZoneType, description="Climate zones"
    )
    navigator_f3__crop_profile_group = graphene.List(
        CropProfileGroupType, description="Crop profile groups"
    )
    navigator_f3__crop_profile = graphene.List(
        CropProfileType, description="Crop profiles"
    )
    navigator_f3__phosphorus_analysis_method = graphene.List(
        PhosphorusAnalysisMethodType, description="Phosphorus analysis methods"
    )
    navigator_f3__soil_mineralization_profile = graphene.List(
        SoilMineralizationProfileType, description="Soil mineralization profiles"
    )
    navigator_f3__soil = graphene.List(SoilType, description="Soil textures")
    navigator_f3__fertilizer = graphene.List(FertilizerType, description="Fertilizers")

    def resolve_navigator_f3__config(root, info):
        conf = ConfigType(
            id="",
            # Feature flags
            enable_young_trees=settings.ENABLE_YOUNG_TREES,
            # Display surface unit
            display_surface_unit_name=config.DISPLAY_SURFACE_UNIT_NAME,
            display_surface_unit_short_name=config.DISPLAY_SURFACE_UNIT_SHORT_NAME,
            display_surface_unit_ratio_to_hectare=config.DISPLAY_SURFACE_UNIT_RATIO_TO_HECTARE,
            # Default interface values for crop
            default_phosphorus_potassium_strategy=config.DEFAULT_PHOSPHORUS_POTASSIUM_STRATEGY,
            default_yield=config.DEFAULT_YIELD,
            # Default interface values for soil
            default_soil_id=config.DEFAULT_SOIL_ID,
            default_soil_ph=config.DEFAULT_SOIL_PH,
            default_soil_organic_matter=config.DEFAULT_SOIL_ORGANIC_MATTER,
            default_soil_depth=config.DEFAULT_SOIL_DEPTH,
            default_soil_nitrogen_content_initial=config.DEFAULT_SOIL_NITROGEN_CONTENT_INITIAL,
            default_soil_nitrogen_content_final=config.DEFAULT_SOIL_NITROGEN_CONTENT_FINAL,
            default_phosphorus_analysis_method_id=config.DEFAULT_PHOSPHORUS_ANALYSIS_METHOD_ID,
            default_soil_phosphorus=config.DEFAULT_SOIL_PHOSPHORUS,
            default_soil_potassium=config.DEFAULT_SOIL_POTASSIUM,
            # Default interface values for plot
            default_climate_zone_id=config.DEFAULT_CLIMATE_ZONE_ID,
            default_water_supply_method=config.DEFAULT_WATER_SUPPLY_METHOD,
            default_irrigation_method=config.DEFAULT_IRRIGATION_METHOD,
            default_irrigation_dose=config.DEFAULT_IRRIGATION_DOSE,
            default_nitrogen_no3_content_in_water=config.DEFAULT_NITROGEN_NO3_CONTENT_IN_WATER,
        )

        conf.id = hashlib.md5(
            json.dumps(conf.__dict__, sort_keys=True).encode()
        ).hexdigest()

        return conf

    def resolve_navigator_f3__climate_zone(root, info):
        return ClimateZone.objects.filter(is_active=True).all()

    def resolve_navigator_f3__crop_profile_group(root, info):
        return CropProfileGroup.objects.all()

    def resolve_navigator_f3__crop_profile(root, info):
        queryset = CropProfile.objects.filter(is_active=True).select_related("group")
        if settings.ENABLE_YOUNG_TREES:
            queryset = queryset.prefetch_related(
                "young_trees", "young_trees__fertilization_years"
            )
        return queryset.all()

    def resolve_navigator_f3__phosphorus_analysis_method(root, info):
        return PhosphorusAnalysisMethod.objects.filter(is_active=True).all()

    def resolve_navigator_f3__soil_mineralization_profile(root, info):
        return SoilMineralizationProfile.objects.all()

    def resolve_navigator_f3__soil(root, info):
        return Soil.objects.filter(is_active=True).all()

    def resolve_navigator_f3__fertilizer(root, info):
        return Fertilizer.objects.filter(is_active=True).all()

    navigator_f3__fertilization_application_frequency = graphene.Field(
        FertilizerApplicationFrequencyEnum, description="Fertilization application frequencies"
    )
    navigator_f3__fertilization_incorporation_method = graphene.Field(
        FertilizerIncorporationMethodEnum, description="Fertilization incorporation methods"
    )
    navigator_f3__water_supply_method = graphene.Field(WaterSupplyMethodEnum, description="Water supply methods")
    navigator_f3__irrigation_method = graphene.Field(IrrigationMethodEnum, description="Irrigation methods")
    navigator_f3__phosphorus_potassium_strategy = graphene.Field(
        PhosphorusPotassiumStrategyEnum, description="Phosphorus-potassium strategies"
    )
