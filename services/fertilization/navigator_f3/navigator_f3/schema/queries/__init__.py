from navigator_f3.schema.queries.lists_and_enums import Query as ListsAndEnumsQuery
from navigator_f3.schema.queries.compute import Query as ComputeQuery
from navigator_f3.schema.queries.pdf import Query as PdfQuery


class Query(ListsAndEnumsQuery, ComputeQuery, PdfQuery):
    pass
