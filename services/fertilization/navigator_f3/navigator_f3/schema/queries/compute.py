import logging
import hashlib

import graphene
from graphql import GraphQLError

from constance import config

from navigator_f3.models import (
    ClimateZone,
    CropProfile,
    Fertilizer,
    FertilizerApplicationFrequency,
    FertilizerIncorporationMethod,
    IrrigationMethod,
    PhosphorusAnalysisMethod,
    PhosphorusPotassiumStrategy,
    IrrigationMethod,
    WaterSupplyMethod,
    Soil,
)

from navigator_f3.lib.algorithm import NavigatorF3Algorithm
from navigator_f3.lib.types import Fertilization, AdditionalFertilization

from navigator_f3.schema.enums import (
    PhosphorusPotassiumStrategyEnum,
    WaterSupplyMethodEnum,
    IrrigationMethodEnum,
)
from navigator_f3.schema.types import (
    ComputeBalanceResultType,
    FertilizationType,
    AdditionalFertilizationType,
)
from navigator_f3.schema.input_types import FertilizationInputType

logger = logging.getLogger(__name__)


class Query:

    navigator_f3__compute_balance = graphene.Field(
        ComputeBalanceResultType,
        description="Compute the nutrient balance using the Navigator F3 algorithm",
        # Crop
        crop_profile_id=graphene.Argument(
            graphene.ID, required=True, description="Crop profile identifier"
        ),
        phosphorus_potassium_strategy=graphene.Argument(
            PhosphorusPotassiumStrategyEnum,
            required=True,
            description="Phosphorus-potassium strategy",
        ),
        crop_yield=graphene.Argument(
            graphene.Float, required=True, description="Crop yield (in kg/ha)"
        ),
        residues_left_in_field=graphene.Argument(
            graphene.Float,
            required=False,
            description="Residues left in field (in %). Leave blank for default value.",
        ),
        harvest_index=graphene.Argument(
            graphene.Float,
            required=False,
            description="Harvest index (no unit). Leave blank for default value.",
        ),
        cv=graphene.Argument(
            graphene.Float,
            required=False,
            description="Crop coefficient of variation (no unit). Leave blank for default value.",
        ),
        harvest_nitrogen_content=graphene.Argument(
            graphene.Float,
            required=False,
            description="Harvest nitrogen content (in %). Leave blank for default value.",
        ),
        harvest_phosphorus_content=graphene.Argument(
            graphene.Float,
            required=False,
            description="Harvest phosphorus content (in %). Leave blank for default value.",
        ),
        harvest_potassium_content=graphene.Argument(
            graphene.Float,
            required=False,
            description="Harvest potassium content (in %). Leave blank for default value.",
        ),
        # Soil
        soil_id=graphene.ID(
            required=True,
            description="Soil texture identifier",
        ),
        soil_ph=graphene.Float(
            required=True,
            description="Soil pH (0 to 14).",
        ),
        soil_cec=graphene.Float(
            required=False,
            description="Soil CEC. Required only if phosphorus_potassium_strategy != 'maintenance'.",
        ),
        soil_organic_matter_content=graphene.Float(
            required=True,
            description="Soil organic matter content (in %).",
        ),
        soil_depth=graphene.Float(
            required=True,
            description="Soil depth (in m).",
        ),
        soil_nitrogen_content_initial=graphene.Float(
            required=True,
            description="Initial (pre-sowing) nitrogen soil content (in ppm).",
        ),
        soil_nitrogen_content_final=graphene.Float(
            required=True,
            description="Final (post-harvest) nitrogen soil content (in ppm).",
        ),
        soil_phosphorus_analysis_method_id=graphene.ID(
            required=False,
            description="Analysis method used to determine the soil phosphorus content. Required only if phosphorus_potassium_strategy != 'maintenance'.",
        ),
        soil_phosphorus_content=graphene.Float(
            required=False,
            description="Initial soil phosphorus content (in ppm). Required only if phosphorus_potassium_strategy != 'maintenance'.",
        ),
        soil_potassium_content=graphene.Float(
            required=False,
            description="Initial soil potassium content (in ppm). Required only if phosphorus_potassium_strategy != 'maintenance'.",
        ),
        # Plot
        climate_zone_id=graphene.Argument(
            graphene.ID, required=True, description="Climate zone identifier"
        ),
        rainfall_annual=graphene.Argument(
            graphene.Float,
            required=False,
            description="Annual rainfall (in mm). Leave blank for default value.",
        ),
        rainfall_autumn_winter=graphene.Argument(
            graphene.Float,
            required=False,
            description="Autumn and winter rainfall (in mm). Leave blank for default value.",
        ),
        water_supply_method=graphene.Argument(
            WaterSupplyMethodEnum, required=True, description="Water supply method"
        ),
        irrigation_method=graphene.Argument(
            IrrigationMethodEnum,
            required=False,
            description="Irrigation method. Required only if water_supply_method == 'irrigation'.",
        ),
        irrigation_dose=graphene.Argument(
            graphene.Float,
            required=False,
            description="Irrigation dose (in m3/ha). Required only if water_supply_method == 'irrigation'.",
        ),
        irrigation_nitrogen_no3_content=graphene.Argument(
            graphene.Float,
            required=False,
            description="Irrigation nitrogen NO3 content (in ppm). Required only if water_supply_method == 'irrigation'.",
        ),
        # Fertilizers
        fertilizations=graphene.Argument(
            graphene.List(FertilizationInputType),
            required=False,
            description="List of planned fertilizations. Can be empty.",
        ),
        additional_fertilizers=graphene.Argument(
            graphene.List(graphene.ID),
            required=False,
            description="List of additional fertilizers to be considered by the optimizer, in addition to the ones already listed in the 'fertilizations' argument. Can be empty or ignored.",
        ),
    )

    def resolve_navigator_f3__compute_balance(
        root,
        info,
        crop_profile_id,
        phosphorus_potassium_strategy,
        crop_yield,
        residues_left_in_field,
        harvest_index,
        cv,
        harvest_nitrogen_content,
        harvest_phosphorus_content,
        harvest_potassium_content,
        soil_id,
        soil_ph,
        soil_cec,
        soil_organic_matter_content,
        soil_depth,
        soil_nitrogen_content_initial,
        soil_nitrogen_content_final,
        soil_phosphorus_analysis_method_id,
        soil_phosphorus_content,
        soil_potassium_content,
        climate_zone_id,
        water_supply_method,
        irrigation_method,
        irrigation_dose,
        irrigation_nitrogen_no3_content,
        rainfall_annual,
        rainfall_autumn_winter,
        fertilizations=None,
        additional_fertilizers=None,
    ):
        try:
            crop_profile = CropProfile.objects.get(pk=crop_profile_id, is_active=True)
        except CropProfile.DoesNotExist:
            raise ValueError(f"Crop profile with id={crop_profile_id} does not exist")

        try:
            phosphorus_potassium_strategy = getattr(
                PhosphorusPotassiumStrategy, phosphorus_potassium_strategy.name
            )
        except AttributeError:
            raise ValueError(
                f'PhosphorusPotassiumStrategy "{phosphorus_potassium_strategy}" does not exist'
            )

        if residues_left_in_field is not None:
            crop_profile.residues_left_in_field = residues_left_in_field
        else:
            if crop_profile.residues_left_in_field is None:
                raise ValueError(
                    "No default value present in database for crop_profile.residues_left_in_field: residues_exported must be non null"
                )

        if harvest_index is not None:
            crop_profile.harvest_index = harvest_index
        if crop_profile.harvest_index is None:
            raise ValueError(
                "No default value present in database for crop_profile.harvest_index: harvest_index must be non null"
            )

        if cv is not None:
            crop_profile.cv = cv
        if crop_profile.cv is None:
            raise ValueError(
                "No default value present in database for crop_profile.cv: cv must be non null"
            )

        if harvest_nitrogen_content is not None:
            crop_profile.harvest_nitrogen_content = harvest_nitrogen_content
        if crop_profile.harvest_nitrogen_content is None:
            raise ValueError(
                "No default value present in database for crop_profile.harvest_nitrogen_content: harvest_nitrogen_content must be non null"
            )

        if harvest_phosphorus_content is not None:
            crop_profile.harvest_phosphorus_content = harvest_phosphorus_content
        if crop_profile.harvest_phosphorus_content is None:
            raise ValueError(
                "No default value present in database for crop_profile.harvest_phosphorus_content: harvest_phosphorus_content must be non null"
            )

        if harvest_potassium_content is not None:
            crop_profile.harvest_potassium_content = harvest_potassium_content
        if crop_profile.harvest_potassium_content is None:
            raise ValueError(
                "No default value present in database for crop_profile.harvest_potassium_content: harvest_potassium_content must be non null"
            )

        try:
            soil = Soil.objects.get(pk=soil_id, is_active=True)
        except Soil.DoesNotExist:
            raise ValueError("Soil with id=%s does not exist", soil_id)

        if not soil_nitrogen_content_initial >= 0:
            raise ValueError("Soil initial nitrogen content must be positive")

        if not soil_nitrogen_content_final >= 0:
            raise ValueError("Soil final nitrogen content must be positive")

        if not soil_organic_matter_content >= 0:
            raise ValueError("Soil organic matter content must be positive")

        if not soil_depth > 0:
            raise ValueError("Soil depth must be strictly positive")

        if not (0 <= soil_ph <= 14):
            raise ValueError("Soil pH must be between 0 and 14")

        if phosphorus_potassium_strategy != PhosphorusPotassiumStrategy.maintenance:
            if any(
                [
                    soil_phosphorus_analysis_method_id is None,
                    soil_phosphorus_content is None,
                    soil_potassium_content is None,
                ]
            ):
                raise ValueError(
                    "Soil values are required, when phosphorus_potassium_strategy != 'maintenance'"
                )

            # Override default soil CEC value if custom value is provided
            if soil_cec is not None:
                if not soil_cec >= 0:
                    raise ValueError("Soil CEC must be positive")
                soil.cec = soil_cec

            try:
                soil_phosphorus_analysis_method = PhosphorusAnalysisMethod.objects.get(
                    pk=soil_phosphorus_analysis_method_id, is_active=True
                )
            except PhosphorusAnalysisMethod.DoesNotExist:
                raise ValueError(
                    "Phosphorus analysis method with id=%s does not exist",
                    soil_phosphorus_analysis_method_id,
                )

            if not soil_phosphorus_content >= 0:
                raise ValueError("Soil phosphorus content must be positive")

            if not soil_potassium_content >= 0:
                raise ValueError("Soil potassium content must be positive")

        else:
            soil_phosphorus_analysis_method = None
            soil_phosphorus_content = None
            soil_potassium_content = None

        try:
            climate_zone = ClimateZone.objects.get(pk=climate_zone_id, is_active=True)
        except ClimateZone.DoesNotExist:
            raise ValueError("Climate zone with id=%s does not exist", climate_zone_id)

        # Check irrigation parameters
        water_supply_method = getattr(WaterSupplyMethod, water_supply_method.name)

        if water_supply_method == WaterSupplyMethod.irrigated:
            if irrigation_method is None:
                raise ValueError(
                    "irrigation_method cannot be null when water_supply_method=irrigated"
                )
            irrigation_method = getattr(IrrigationMethod, irrigation_method.name)
            if irrigation_dose is None:
                raise ValueError(
                    "irrigation_dose cannot be null when water_supply_method=irrigated"
                )
            if irrigation_nitrogen_no3_content is None:
                raise ValueError(
                    "irrigation_nitrogen_no3_content cannot be null when water_supply_method=irrigated"
                )

        # Check if custom values for rainfall have been set
        if rainfall_annual is not None:
            if not rainfall_annual > 0:
                raise ValueError("rainfall_annual must be strictly positive")
            climate_zone.rainfall_annual = rainfall_annual
        if rainfall_autumn_winter is not None:
            climate_zone.rainfall_autumn_winter = rainfall_autumn_winter

        # Convert fertilizations input types to actual Fertilization objects
        # to be consumed by the algorithm
        _fertilizations = []
        if fertilizations:
            for fertilization in fertilizations:
                if fertilization.fertilizer_id is not None:
                    try:
                        fertilizer = Fertilizer.objects.get(
                            pk=fertilization.fertilizer_id, is_active=True
                        )
                    except Fertilizer.DoesNotExist:
                        raise ValueError(
                            "Fertilizer with id=%s does not exist",
                            fertilization.fertilizer_id,
                        )
                else:
                    # That is a custom fertilizer, check the parameters
                    if fertilization.name is None or fertilization.name == "":
                        raise ValueError(
                            "Fertilizer name cannot be null if fertilizer_id is null"
                        )
                    if fertilization.is_organic is None:
                        raise ValueError(
                            "Fertilization is_organic cannot be null if fertilizer_id is null"
                        )
                    if fertilization.is_organic:
                        if fertilization.nitrogen_organic_dry_matter_content is None:
                            raise ValueError(
                                "Fertilization nitrogen_organic_dry_matter_content cannot be null if fertilizer_id is null and is_organic is true"
                            )
                        if fertilization.nitrogen_organic_dry_matter_content < 0:
                            raise ValueError(
                                "Fertilization nitrogen_organic_dry_matter_content must be positive"
                            )
                    else:
                        if fertilization.nitrogen_total_content is None:
                            raise ValueError(
                                "Fertilization nitrogen_total_content cannot be null if fertilizer_id is null and is_organic is false"
                            )
                        if fertilization.nitrogen_total_content < 0:
                            raise ValueError(
                                "Fertilization nitrogen_total_content must be positive"
                            )
                    if (
                        fertilization.phosphorus_total_content is None
                        or fertilization.phosphorus_total_content < 0
                    ):
                        raise ValueError(
                            "Fertilization phosphorus_total_content cannot be null or negative if fertilizer_id is null"
                        )
                    if (
                        fertilization.potassium_total_content is None
                        or fertilization.potassium_total_content < 0
                    ):
                        raise ValueError(
                            "Fertilization potassium_total_content cannot be null or negative if fertilizer_id is null"
                        )
                    if (
                        fertilization.sodium_total_content is not None
                        and fertilization.sodium_total_content < 0
                    ):
                        raise ValueError(
                            "Fertilization sodium_total_content must be positive or null"
                        )
                    if (
                        fertilization.calcium_total_content is not None
                        and fertilization.calcium_total_content < 0
                    ):
                        raise ValueError(
                            "Fertilization calcium_total_content must be positive or null"
                        )
                    if (
                        fertilization.magnesium_total_content is not None
                        and fertilization.magnesium_total_content < 0
                    ):
                        raise ValueError(
                            "Fertilization magnesium_total_content must be positive or null"
                        )
                    if (
                        fertilization.sulphur_total_content is not None
                        and fertilization.sulphur_total_content < 0
                    ):
                        raise ValueError(
                            "Fertilization sulphur_total_content must be positive or null"
                        )
                    if (
                        fertilization.chlorine_total_content is not None
                        and fertilization.chlorine_total_content < 0
                    ):
                        raise ValueError(
                            "Fertilization chlorine_total_content must be positive or null"
                        )

                    fertilizer = Fertilizer(
                        id="custom",
                        name=fertilization.name,
                        is_active=True,
                        is_organic=fertilization.is_organic,
                        volatilization_coefficient=0,
                        nitrogen_total_content=fertilization.nitrogen_total_content,
                        nitrogen_organic_dry_matter_content=fertilization.nitrogen_organic_dry_matter_content,
                        phosphorus_total_content=fertilization.phosphorus_total_content,
                        potassium_total_content=fertilization.potassium_total_content,
                        sodium_total_content=fertilization.sodium_total_content,
                        calcium_total_content=fertilization.calcium_total_content,
                        magnesium_total_content=fertilization.magnesium_total_content,
                        sulphur_total_content=fertilization.sulphur_total_content,
                        chlorine_total_content=fertilization.chlorine_total_content,
                    )

                application_frequency = getattr(
                    FertilizerApplicationFrequency,
                    fertilization.application_frequency.name,
                )

                if fertilization.quantity is not None and fertilization.quantity < 0:
                    raise ValueError("Fertilization quantity must be positive or zero")

                # If incorporation_method is provided, then it overrides the default value
                # of the fertilizer
                if fertilization.incorporation_method is not None:
                    fertilizer.incorporation_method = getattr(
                        FertilizerIncorporationMethod,
                        fertilization.incorporation_method.name,
                    )

                _fertilizations.append(
                    Fertilization(
                        fertilizer=fertilizer,
                        application_frequency=application_frequency,
                        quantity=fertilization.quantity,
                    )
                )

        _additional_fertilizations = []
        if additional_fertilizers:
            for fertilizer_id in additional_fertilizers:
                try:
                    fertilizer = Fertilizer.objects.get(
                        pk=fertilizer_id, is_active=True
                    )
                    _additional_fertilizations.append(
                        AdditionalFertilization(fertilizer=fertilizer, quantity=None)
                    )
                except Fertilizer.DoesNotExist:
                    raise ValueError(
                        "Fertilizer with id=%s does not exist",
                        fertilizer_id,
                    )

        algorithm = NavigatorF3Algorithm(
            config=config,
            crop_profile=crop_profile,
            phosphorus_potassium_strategy=phosphorus_potassium_strategy,
            crop_yield=crop_yield,
            soil=soil,
            soil_ph=soil_ph,
            soil_organic_matter_content=soil_organic_matter_content,
            soil_depth=soil_depth,
            soil_nitrogen_content_initial=soil_nitrogen_content_initial,
            soil_nitrogen_content_final=soil_nitrogen_content_final,
            soil_phosphorus_analysis_method=soil_phosphorus_analysis_method,
            soil_phosphorus_content=soil_phosphorus_content,
            soil_potassium_content=soil_potassium_content,
            climate_zone=climate_zone,
            water_supply_method=water_supply_method,
            irrigation_method=irrigation_method,
            irrigation_dose=irrigation_dose,
            irrigation_nitrogen_no3_content=irrigation_nitrogen_no3_content,
            fertilizations=_fertilizations,
            additional_fertilizations=_additional_fertilizations,
        )

        try:
            algorithm.compute()
        except Exception as e:
            logger.exception("Exception in algorithm")
            return GraphQLError("ALGORITHM_EXCEPTION")

        algorithm.inputs.round(num_digits=2)
        algorithm.outputs.round(num_digits=2)

        fertilizations = [
            FertilizationType(
                id=f.id,
                quantity=round(f.quantity, 2),
                fertilizer=f.fertilizer,
            )
            for f in algorithm.fertilizations
        ]

        additional_fertilizations = [
            AdditionalFertilizationType(
                id=f.id,
                quantity=round(f.quantity, 2),
                fertilizer=f.fertilizer,
            )
            for f in algorithm.additional_fertilizations
        ]

        return ComputeBalanceResultType(
            id=hashlib.md5(
                "".join(
                    [f.id for f in fertilizations]
                    + [algorithm.inputs.id, algorithm.outputs.id]
                ).encode()
            ).hexdigest(),
            inputs=algorithm.inputs,
            outputs=algorithm.outputs,
            fertilizations=fertilizations,
            additional_fertilizations=additional_fertilizations,
        )
