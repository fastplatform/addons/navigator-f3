import base64
import logging

import graphene
import requests
from opentelemetry import trace
from django.conf import settings

from navigator_f3.schema.types import (
    PDFGeneratorInputType,
    PDFGeneratorOutputType,
)

from navigator_f3.errors import GraphQLServiceError

from .pdf_translations import FIELDS, FERTILIZATION_LABELS, PK_STRATEGY_OPTIONS

logger = logging.getLogger(__name__)

# OpenTelemetry
def tracer():
    return trace.get_tracer(__name__)


NO_QUANTITY_SET = " - "

MAIN_RESULTS_FIELDS = [
    "recommendation",
    "recommendation_per_tree",
    "inputs",
    "outputs",
]

INPUT_RESULTS_FIELDS = [
    "in_soil_initial",
    "biological_fixation",
    "soil_mineralization",
    "irrigation",
    "planned",
]

OUTPUT_RESULTS_FIELDS = [
    "in_soil_final",
    "leaching",
    "uptake",
    "denitrification",
    "volatilization",
]


class Query:
    navigator_f3__generate_results_pdf = graphene.NonNull(
        PDFGeneratorOutputType,
        input=graphene.Argument(PDFGeneratorInputType, required=True),
    )

    def resolve_navigator_f3__generate_results_pdf(self, info, input):
        inputs = input["inputs"]
        logger.info("-------------")
        logger.info(
            "resolve_navigator_f3__generate_results_pdf with inputs initiated : %s",
            inputs,
        )

        language = str(settings.PDF_GENERATOR_LANGUAGE_CODE)
        logger.info("Resolving with language: %s", language)

        try:
            aggregate = {}

            for plan in inputs["fertilization_plans"]:
                surface_unit = plan["parameters"]["displaySurfaceUnit"]["short_name"]
                surface_ratio_to_ha = plan["parameters"]["displaySurfaceUnit"][
                    "ratio_to_hectare"
                ]

                plan["surface_unit"] = surface_unit
                plan["surface_ratio_to_ha"] = surface_ratio_to_ha

                for plot_id in plan["recommendations_by_plot"]:
                    logger.info("plot_id: %s", plot_id)
                    for plot_recommendation in plan["recommendations_by_plot"][plot_id][
                        "recommendations"
                    ]:
                        plan["crop_yield"] = (
                            f"{round(plot_recommendation['crop_yield'] * surface_ratio_to_ha, 2)} kg/{surface_unit}"
                            if "crop_yield" in plot_recommendation
                            else None
                        )

                        for element_recommendation in plot_recommendation[
                            "element_recommendations"
                        ]:
                            recommendation_type = str(
                                element_recommendation["recommendation_type"]
                            ).lower()

                            if not recommendation_type in aggregate:
                                aggregate[recommendation_type] = {
                                    "label": FIELDS[language][recommendation_type]
                                }

                            quantity = (
                                f"{round(element_recommendation['quantity'] * surface_ratio_to_ha, 2)}"
                                if element_recommendation["quantity"]
                                else NO_QUANTITY_SET
                            )

                            if (
                                recommendation_type == "recommendation"
                                and not quantity == NO_QUANTITY_SET
                            ):
                                quantity = f"{quantity} kg/{surface_unit}"

                            if recommendation_type == "recommendation_per_tree":
                                quantity_min = (
                                    f"{element_recommendation['quantity_min']}"
                                    if element_recommendation["quantity_min"]
                                    else ""
                                )

                                quantity_max = (
                                    f"{element_recommendation['quantity_max']}"
                                    if element_recommendation["quantity_max"]
                                    else ""
                                )
                                quantity = (
                                    f"{quantity_min} - {quantity_max} kg"
                                    if quantity_min and quantity_max
                                    else NO_QUANTITY_SET
                                )

                            element = str(
                                element_recommendation["chemical_element_id"]
                            ).lower()

                            aggregate[recommendation_type][element] = quantity

                if (
                    "youngTreesFertilizationYear" in plan["parameters"]
                    and plan["parameters"]["youngTreesFertilizationYear"]
                ):
                    logger.info("Computing young trees...")
                    aggregate["recommendation"][
                        "young_trees_fertilization_year"
                    ] = plan["parameters"]["youngTreesFertilizationYear"]["label"]
                    aggregate["recommendation"]["young_trees_per_surface"] = round(
                        plan["parameters"]["youngTreesPerHa"] * surface_ratio_to_ha, 2
                    )

                plan["main_results"] = {
                    recommendation_type: aggregate[recommendation_type]
                    for recommendation_type in MAIN_RESULTS_FIELDS
                    if recommendation_type in aggregate
                }

                plan["input_results"] = [
                    aggregate[recommendation_type]
                    for recommendation_type in INPUT_RESULTS_FIELDS
                    if recommendation_type in aggregate
                ]

                plan["output_results"] = [
                    aggregate[recommendation_type]
                    for recommendation_type in OUTPUT_RESULTS_FIELDS
                    if recommendation_type in aggregate
                ]

                fertilization_strategy = (
                    PK_STRATEGY_OPTIONS[language][
                        plan["parameters"]["pkStrategy"]["name"]
                    ]
                    if "pkStrategy" in plan["parameters"]
                    else None
                )

                plan["fertilization_strategy"] = (
                    {
                        "label": fertilization_strategy["label"],
                        "description": fertilization_strategy["description"],
                    }
                    if fertilization_strategy
                    else None
                )
                if "fertilizations" in plan["parameters"]:
                    for fertilization in plan["parameters"]["fertilizations"]:
                        fertilization["fertilizerApplicationFrequency"][
                            "label"
                        ] = FERTILIZATION_LABELS[language][
                            fertilization["fertilizerApplicationFrequency"]["name"]
                        ]

                        fertilization["fertilizerIncorporationMethod"][
                            "label"
                        ] = FERTILIZATION_LABELS[language][
                            fertilization["fertilizerIncorporationMethod"]["name"]
                        ]

                        fertilization["quantity_formatted"] = (
                            f"{round(fertilization['fertilizerQuantity'] * surface_ratio_to_ha, 2)} kg/{surface_unit}"
                            if fertilization["fertilizerQuantity"]
                            else NO_QUANTITY_SET
                        )

        except Exception as exc:
            logger.exception("Failed to aggregate data")
            raise GraphQLServiceError() from exc

        try:
            # Get algorithm logo as static image
            with open(settings.BASE_DIR / "static/navigator_f3/logo.jpg", "rb") as f:
                logo = f.read()
                inputs["algorithm_logo_base64"] = (
                    "data:image/jpg;base64," + base64.b64encode(logo).decode()
                )
        except Exception as exc:
            logger.exception("Failed to read algorithm logo")
            raise GraphQLServiceError() from exc

        with tracer().start_as_current_span("generate_pdf"):
            try:
                base_html = "base_bg.html" if language == "bg" else "base_gr.html"
                with open(settings.BASE_DIR / f"templates/pdf/{base_html}", "r") as f:
                    template = f.read()

                response = requests.post(
                    settings.PDF_GENERATOR_ENDPOINT,
                    headers={},
                    json={"template": template, "data": inputs},
                )
                response.raise_for_status()

                return PDFGeneratorOutputType(pdf=response.content)

            except Exception as exc:
                logger.exception("Failed to build pdf")
                raise GraphQLServiceError() from exc
