import logging

import graphene
from graphene_django import DjangoObjectType
from graphene_pydantic import PydanticObjectType

from navigator_f3.models import (
    ClimateZone,
    CropProfileGroup,
    CropProfile,
    Fertilizer,
    PhosphorusAnalysisMethod,
    SoilMineralizationProfile,
    Soil,
)

from navigator_f3.lib.algorithm import (
    NavigatorF3ResultInputs,
    NavigatorF3ResultOutputs,
)

logger = logging.getLogger(__name__)


# Types needed to expose lists to the app user interface
# ======================================================


class ConfigType(graphene.ObjectType):
    id = graphene.ID(required=True)
    # Feature flags
    enable_young_trees = graphene.NonNull(
        graphene.Boolean,
        description="Whether or not the 'young trees' feature is enabled.",
    )
    # Display surface unit
    display_surface_unit_name = graphene.NonNull(
        graphene.String, description="Name of the unit used to display surface values."
    )
    display_surface_unit_short_name = graphene.NonNull(
        graphene.String,
        description="Short name of the unit used to display surface values.",
    )
    display_surface_unit_ratio_to_hectare = graphene.NonNull(
        graphene.Float,
        description="Ratio of the unit used to display surface values versus hectare.",
    )
    # Default interface values for crop
    default_phosphorus_potassium_strategy = graphene.NonNull(
        graphene.String,
        description="Default phosphorus-potassium strategy to be proposed to the user.",
    )
    default_yield = graphene.NonNull(
        graphene.Float,
        description="Default harvest yield (in kg/ha) to be proposed to the user.",
    )
    # Default interface values for soil
    default_soil_id = graphene.NonNull(
        graphene.String,
        description="Default soil texture identifier to be proposed to the user.",
    )
    default_soil_ph = graphene.NonNull(
        graphene.Float, description="Default soil pH to be proposed to the user."
    )
    default_soil_organic_matter = graphene.NonNull(
        graphene.Float,
        description="Default soil organic matter (in %) to be proposed to the user.",
    )
    default_soil_depth = graphene.NonNull(
        graphene.Float,
        description="Default soil depth  (in meters) to be proposed to the user.",
    )
    default_soil_nitrogen_content_initial = graphene.NonNull(
        graphene.Float,
        description="Default initial soil nitrogen content (in kg.N/ha) to be proposed to the user.",
    )
    default_soil_nitrogen_content_final = graphene.NonNull(
        graphene.Float,
        description="Default final soil nitrogen content (in kg.N/ha) to be proposed to the user.",
    )
    default_phosphorus_analysis_method_id = graphene.NonNull(
        graphene.String,
        description="Default phosphorus analysis method identifier to be proposed to the user.",
    )
    default_soil_phosphorus = graphene.NonNull(
        graphene.Float,
        description="Default soil phosphorus (in ppm) to be proposed to the user.",
    )
    default_soil_potassium = graphene.NonNull(
        graphene.Float,
        description="Default soil potassium (in ppm) to be proposed to the user.",
    )
    # Default interface values for plot
    default_climate_zone_id = graphene.NonNull(
        graphene.String,
        description="Default climate zone identifier to be proposed to the user.",
    )
    default_water_supply_method = graphene.NonNull(
        graphene.String,
        description="Default water supply method to be proposed to the user.",
    )
    default_irrigation_method = graphene.NonNull(
        graphene.String,
        description="Default irrigation method to be proposed to the user.",
    )
    default_irrigation_dose = graphene.NonNull(
        graphene.Float,
        description="Default irrigation dose (in m3/ha) to be proposed to the user.",
    )
    default_nitrogen_no3_content_in_water = graphene.NonNull(
        graphene.Float,
        description="Default nitrogen NO3 content in water (in ppm) to be proposed to the user.",
    )

    class Meta:
        name = "navigator_f3__config"
        description = "Configuration"


class ClimateZoneType(DjangoObjectType):
    class Meta:
        model = ClimateZone
        name = "navigator_f3__climate_zone"
        convert_choices_to_enum = False
        description = ClimateZone._meta.verbose_name.capitalize()


class CropProfileGroupType(DjangoObjectType):
    class Meta:
        model = CropProfileGroup
        name = "navigator_f3__crop_profile_group_type"
        convert_choices_to_enum = False
        description = CropProfileGroup._meta.verbose_name.capitalize()


class CropProfileType(DjangoObjectType):
    class Meta:
        model = CropProfile
        name = "navigator_f3__crop_profile_type"
        convert_choices_to_enum = False
        description = CropProfile._meta.verbose_name.capitalize()


class PhosphorusAnalysisMethodType(DjangoObjectType):
    class Meta:
        model = PhosphorusAnalysisMethod
        name = "navigator_f3__phosphorus_analysis_method"
        convert_choices_to_enum = False
        description = PhosphorusAnalysisMethod._meta.verbose_name.capitalize()


class SoilMineralizationProfileType(DjangoObjectType):
    class Meta:
        model = SoilMineralizationProfile
        name = "navigator_f3__soil_mineralization_profile"
        convert_choices_to_enum = False
        description = SoilMineralizationProfile._meta.verbose_name.capitalize()


class SoilType(DjangoObjectType):
    class Meta:
        model = Soil
        name = "navigator_f3__soil"
        convert_choices_to_enum = False
        description = Soil._meta.verbose_name.capitalize()


class FertilizerType(DjangoObjectType):
    class Meta:
        model = Fertilizer
        name = "navigator_f3__fertilizer"
        convert_choices_to_enum = False
        description = Fertilizer._meta.verbose_name.capitalize()


# Algorithm outputs
# =================


class FertilizationType(graphene.ObjectType):
    id = graphene.ID()
    quantity = graphene.Float(required=True)
    fertilizer = graphene.Field(FertilizerType, required=True)

    class Meta:
        name = "navigator_f3__fertilization_type"
        description = "Fertilization"


class AdditionalFertilizationType(graphene.ObjectType):
    id = graphene.ID()
    quantity = graphene.Float(required=True)
    fertilizer = graphene.Field(FertilizerType, required=True)

    class Meta:
        name = "navigator_f3__additional_fertilization_type"
        description = "Additional fertilization"


class NavigatorF3ResultInputsType(PydanticObjectType):
    id = graphene.ID()

    class Meta:
        model = NavigatorF3ResultInputs
        name = "navigator_f3__result_inputs_type"
        description = "Algorithm results (inputs)"


class NavigatorF3ResultOutputsType(PydanticObjectType):
    id = graphene.ID()

    class Meta:
        model = NavigatorF3ResultOutputs
        name = "navigator_f3__result_outputs_type"
        description = "Algorithm results (outputs)"


class ComputeBalanceResultType(graphene.ObjectType):
    id = graphene.ID(required=True)
    inputs = graphene.Field(NavigatorF3ResultInputsType)
    outputs = graphene.Field(NavigatorF3ResultOutputsType)
    fertilizations = graphene.List(FertilizationType)
    additional_fertilizations = graphene.List(AdditionalFertilizationType)

    class Meta:
        name = "navigator_f3__compute_balance_result_type"
        description = "Algorithm results"


# pdf generation
# ==============
class PDFGeneratorInputType(graphene.InputObjectType):
    inputs = graphene.JSONString(required=True)

    class Meta:
        name = "navigator_f3__pdf_generator_input"


class PDFGeneratorOutputType(graphene.ObjectType):
    pdf = graphene.Base64()

    class Meta:
        name = "navigator_f3__pdf_generator_output"
