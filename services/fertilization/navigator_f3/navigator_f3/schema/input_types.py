import logging

import graphene

from navigator_f3.schema.enums import (
    FertilizerApplicationFrequencyEnum,
    FertilizerIncorporationMethodEnum,
)

logger = logging.getLogger(__name__)


class FertilizationInputType(graphene.InputObjectType):
    quantity = graphene.Argument(
        graphene.Float,
        required=False,
        description="Quantity of fertilizer. If the value is null, the returned quantity will be calculated by the algorithm.",
    )
    fertilizer_id = graphene.Argument(
        graphene.ID,
        required=False,
        description="Fertilizer identifier. If null, the custom fertilizer parameters (name, is_organic, nitrogen_total_content, phosphorus_total_content, potassium_total_content) must be provided.",
    )
    application_frequency = graphene.Argument(
        FertilizerApplicationFrequencyEnum,
        required=True,
        description="Fertilizer application frequency",
    )
    incorporation_method = graphene.Argument(
        FertilizerIncorporationMethodEnum,
        required=False,
        description="Fertilizer incorporation method",
    )

    # When the fertilizer_id is not provided, the fertilizer is assumed to be custom and the following parameters must be provided:
    # - name
    # - is_organic
    # - nitrogen_total_content
    # - phosphorus_total_content
    # - potassium_total_content
    name = graphene.Argument(
        graphene.String, required=False, description="Custom fertilizer name"
    )
    is_organic = graphene.Argument(
        graphene.Boolean, required=False, description="Is custom fertilizer organic?"
    )
    nitrogen_total_content = graphene.Argument(
        graphene.Float,
        required=False,
        description="Custom fertilizer nitrogen total content",
    )
    nitrogen_organic_dry_matter_content = graphene.Argument(
        graphene.Float,
        required=False,
        description="Custom fertilizer nitrogen total content (in dry matter, for organic fertilizers)",
    )
    phosphorus_total_content = graphene.Argument(
        graphene.Float,
        required=False,
        description="Custom fertilizer phosphorus total content",
    )
    potassium_total_content = graphene.Argument(
        graphene.Float,
        required=False,
        description="Custom fertilizer potassium total content",
    )
    sodium_total_content = graphene.Argument(
        graphene.Float,
        required=False,
        description="Custom fertilizer sodium total content",
    )
    calcium_total_content = graphene.Argument(
        graphene.Float,
        required=False,
        description="Custom fertilizer calcium total content",
    )
    magnesium_total_content = graphene.Argument(
        graphene.Float,
        required=False,
        description="Custom fertilizer magnesium total content",
    )
    sulphur_total_content = graphene.Argument(
        graphene.Float,
        required=False,
        description="Custom fertilizer sulphur total content",
    )
    chlorine_total_content = graphene.Argument(
        graphene.Float,
        required=False,
        description="Custom fertilizer chlorine total content",
    )

    class Meta:
        name = "navigator_f3__fertilization_input_type"
        descritpion = "Fertilization"
