from typing import Optional, List
from math import exp, ceil
import logging

import scipy.optimize
import numpy as np


from django.db.models import Q
from pydantic import confloat

from navigator_f3.models import (
    ClimateZone,
    CropProfile,
    FertilizerApplicationFrequency,
    FertilizerIncorporationMethod,
    IrrigationMethod,
    VolatilizationCoefficientForPH,
    VolatilizationCoefficientForCEC,
    PhosphorusAnalysisMethod,
    PhosphorusPotassiumStrategy,
    IrrigationMethod,
    SoilMineralizationProfile,
    WaterSupplyMethod,
    Soil,
    CropNitrogenFixationProfile,
)

from navigator_f3.lib.types import (
    Fertilization,
    AdditionalFertilization,
    NavigatorF3ResultInputs,
    NavigatorF3ResultOutputs,
)


logger = logging.getLogger(__name__)


class NavigatorF3Algorithm:

    T_20 = -0.84
    T_50 = 0
    T_80 = 0.84

    def __init__(
        self,
        config,
        # Crop section
        crop_profile: CropProfile,
        phosphorus_potassium_strategy: PhosphorusPotassiumStrategy,
        crop_yield: confloat(ge=0),
        # Soil section
        soil: Soil,
        soil_ph: confloat(ge=0, le=14),
        soil_organic_matter_content: confloat(ge=0, le=100),
        soil_depth: confloat(gt=0),
        soil_nitrogen_content_initial: confloat(ge=0),
        soil_nitrogen_content_final: confloat(ge=0),
        soil_phosphorus_analysis_method: PhosphorusAnalysisMethod,
        soil_phosphorus_content: confloat(ge=0),
        soil_potassium_content: confloat(ge=0),
        # Plot section
        climate_zone: ClimateZone,
        water_supply_method: WaterSupplyMethod,
        irrigation_method: Optional[IrrigationMethod],
        irrigation_dose: Optional[confloat(ge=0)],
        irrigation_nitrogen_no3_content: Optional[confloat(ge=0)],
        # Fertilizations that the user has already chosen
        fertilizations: List[Fertilization],
        # Additional fertilizers that the user might consider and for which we should
        # compute the best combination
        additional_fertilizations: List[AdditionalFertilization],
    ):
        # Config
        # ------
        self.NITROGEN_RATIO_ROOTS_VS_SHOOTS = config.NITROGEN_RATIO_ROOTS_VS_SHOOTS
        self.FRACTION_RESIDUES_LEFT_IN_FIELD_AFTER_HARVEST_WITH_ROOTS = (
            config.FRACTION_RESIDUES_LEFT_IN_FIELD_AFTER_HARVEST_WITH_ROOTS
        )
        self.NITROGEN_FIXATION_NON_LEGUME = config.NITROGEN_FIXATION_NON_LEGUME
        self.IRRIGATION_FACTOR_TRICKLE = config.IRRIGATION_FACTOR_TRICKLE
        self.IRRIGATION_FACTOR_SPRINKLER = config.IRRIGATION_FACTOR_SPRINKLER
        self.IRRIGATION_FACTOR_SURFACE = config.IRRIGATION_FACTOR_SURFACE
        self.CROP_PHOSPHORUS_EXTRACTION_MAX = config.CROP_PHOSPHORUS_EXTRACTION_MAX
        self.CROP_POTASSIUM_EXTRACTION_MAX = config.CROP_POTASSIUM_EXTRACTION_MAX

        self.MAXIMUM_COMPUTED_QUANTITY_FOR_ORGANIC_FERTILIZERS = (
            config.MAXIMUM_COMPUTED_QUANTITY_FOR_ORGANIC_FERTILIZERS
        )
        self.MAXIMUM_COMPUTED_QUANTITY_FOR_MINERAL_FERTILIZERS = (
            config.MAXIMUM_COMPUTED_QUANTITY_FOR_MINERAL_FERTILIZERS
        )
        self.MAXIMUM_COMPUTED_QUANTITY_TOTAL_NITROGEN = (
            config.MAXIMUM_COMPUTED_QUANTITY_TOTAL_NITROGEN
        )
        self.MAXIMUM_COMPUTED_QUANTITY_TOTAL_PHOSPHORUS = (
            config.MAXIMUM_COMPUTED_QUANTITY_TOTAL_PHOSPHORUS
        )
        self.MAXIMUM_COMPUTED_QUANTITY_TOTAL_POTASSIUM = (
            config.MAXIMUM_COMPUTED_QUANTITY_TOTAL_POTASSIUM
        )

        # Inputs
        # ------
        self.crop_profile = crop_profile
        self.phosphorus_potassium_strategy = phosphorus_potassium_strategy
        self.crop_yield = crop_yield
        self.soil = soil
        self.soil_ph = soil_ph
        self.soil_organic_matter_content = soil_organic_matter_content
        self.soil_depth = soil_depth
        self.soil_nitrogen_content_initial = soil_nitrogen_content_initial
        self.soil_nitrogen_content_final = soil_nitrogen_content_final
        self.soil_phosphorus_analysis_method = soil_phosphorus_analysis_method
        self.soil_phosphorus_content = soil_phosphorus_content
        self.soil_potassium_content = soil_potassium_content
        self.climate_zone = climate_zone
        self.water_supply_method = water_supply_method
        self.irrigation_method = irrigation_method
        self.irrigation_dose = irrigation_dose
        self.irrigation_nitrogen_no3_content = irrigation_nitrogen_no3_content
        self.fertilizations = fertilizations
        self.additional_fertilizations = additional_fertilizations

        # Results
        # -------
        self.inputs = NavigatorF3ResultInputs()
        self.outputs = NavigatorF3ResultOutputs()

        # Coefficients
        # ------------
        self._harvest_dry_matter_med_20 = (
            self.crop_yield
            * (self.crop_profile.harvest_dry_matter / 100)
            * (1 + (self.crop_profile.cv / 100) * self.T_20)
        )
        self._harvest_dry_matter_med_50 = (
            self.crop_yield
            * (self.crop_profile.harvest_dry_matter / 100)
            * (1 + (self.crop_profile.cv / 100) * self.T_50)
        )
        self._harvest_dry_matter_med_80 = (
            self.crop_yield
            * (self.crop_profile.harvest_dry_matter / 100)
            * (1 + (self.crop_profile.cv / 100) * self.T_80)
        )
        self._residues_dry_matter_med_50 = (
            self._harvest_dry_matter_med_50
            * (1 - self.crop_profile.harvest_index / 100)
            / (self.crop_profile.harvest_index / 100)
        )

    def compute(self):

        try:
            self.compute_inputs()
            self.compute_outputs()

            # Compute missing fertilizer quantities based on the remaining outputs to
            # be covered
            if any([f.quantity is None for f in self.fertilizations]):
                self.compute_fertilizer_quantities()

                # As we have edited the fertilizer quantities, recompute all the other
                # values that depend on them
                self.compute_inputs()
                self.compute_outputs()

            if self.additional_fertilizations:
                self.recommend_additional_fertilizations()

        except Exception as e:
            logger.exception("Failed to compute")
            raise

    def compute_inputs(self):
        """Compute crop inputs"""

        self.compute_input_nitrogen_mineralization()
        self.compute_input_nitrogen_fixation()
        self.compute_input_nitrogen_irrigation()
        self.compute_input_nitrogen_in_soil_initial()
        self.compute_input_fertilizers()
        self.compute_input_totals()

    def compute_outputs(self):
        """Compute crop outputs"""

        self.compute_output_nitrogen_leaching()
        self.compute_output_nitrogen_uptake()
        self.compute_output_nitrogen_in_soil_final()
        self.compute_output_phosphorus_uptake()
        self.compute_output_potassium_uptake()
        self.compute_output_nitrogen_denitrification()
        self.compute_output_nitrogen_volatilization()
        self.compute_output_totals()

    def compute_input_nitrogen_mineralization(self):
        """Compute nitrogen mineralization from the soil organic matter"""

        nitrogen_mineralization = SoilMineralizationProfile.objects.get(
            Q(soil_mineralization_texture=self.soil.mineralization_texture)
            & (
                Q(soil_organic_matter_content_from__isnull=True)
                | Q(
                    soil_organic_matter_content_from__lte=self.soil_organic_matter_content
                )
            )
            & (
                Q(soil_organic_matter_content_to__isnull=True)
                | Q(soil_organic_matter_content_to__gt=self.soil_organic_matter_content)
            )
        ).nitrogen_mineralization

        self.inputs.nitrogen_mineralization = (
            nitrogen_mineralization * self.climate_zone.humidity_factor
        )

    def compute_input_nitrogen_fixation(self):
        """Compute crop atmospheric nitrogen fixation from the air"""

        if not self.crop_profile.is_legume:
            self.inputs.nitrogen_fixation = self.NITROGEN_FIXATION_NON_LEGUME
            return

        nitrogen_fixation_coefficient = CropNitrogenFixationProfile.objects.get(
            Q(crop_profile_cycle=self.crop_profile.cycle)
            & (
                Q(soil_organic_matter_content_from__isnull=True)
                | Q(
                    soil_organic_matter_content_from__lt=self.soil_organic_matter_content
                )
            )
            & (
                Q(soil_organic_matter_content_to__isnull=True)
                | Q(
                    soil_organic_matter_content_to__gte=self.soil_organic_matter_content
                )
            )
        ).nitrogen_fixation

        crop_yield_harvest_dry_matter = self.crop_yield * (
            self.crop_profile.harvest_dry_matter / 100
        )
        crop_yield_residues_dry_matter = (
            crop_yield_harvest_dry_matter
            * (1 - (self.crop_profile.harvest_index / 100))
            / (self.crop_profile.harvest_index / 100)
        )
        crop_yield_harvest_nitrogen = crop_yield_harvest_dry_matter * (
            self.crop_profile.harvest_nitrogen_content / 100
        )
        crop_yield_residues_nitrogen = crop_yield_residues_dry_matter * (
            self.crop_profile.residues_nitrogen_content / 100
        )

        self.inputs.nitrogen_fixation = (
            (1 + self.NITROGEN_RATIO_ROOTS_VS_SHOOTS)
            * (crop_yield_harvest_nitrogen + crop_yield_residues_nitrogen)
            * nitrogen_fixation_coefficient
        )

    def compute_input_nitrogen_irrigation(self):
        """Compute nitrogen input from irrigation water"""

        if self.water_supply_method != WaterSupplyMethod.irrigated:
            self.inputs.nitrogen_irrigation = 0
            return

        if self.irrigation_method == IrrigationMethod.trickle:
            irrigation_factor = self.IRRIGATION_FACTOR_TRICKLE
        elif self.irrigation_method == IrrigationMethod.sprinkler:
            irrigation_factor = self.IRRIGATION_FACTOR_SPRINKLER
        elif self.irrigation_method == IrrigationMethod.surface:
            irrigation_factor = self.IRRIGATION_FACTOR_SURFACE
        else:
            raise ValueError(
                "Unknown irrigation method: %s",
                self.irrigation_method,
            )

        self.inputs.nitrogen_irrigation = (
            self.irrigation_nitrogen_no3_content
            * self.irrigation_dose
            * irrigation_factor
            * 22.6
            / 100000
        )

    def compute_input_nitrogen_in_soil_initial(self):
        """Compute nitrogen input from the soil (initial)"""
        self.inputs.nitrogen_in_soil_initial = self.soil_nitrogen_content_initial

    def compute_input_fertilizers(self):
        """Compute fertilizer inputs"""

        self.inputs.nitrogen_fertilizers = 0
        self.inputs.phosphorus_fertilizers = 0
        self.inputs.potassium_fertilizers = 0

        for fertilization in self.fertilizations:
            if fertilization.quantity is None:
                continue
            self.inputs.nitrogen_fertilizers += (
                (fertilization.fertilizer.nitrogen_organic_dry_matter_content or 0)
                / 100
                if fertilization.fertilizer.is_organic
                else (fertilization.fertilizer.nitrogen_total_content or 0) / 100
            ) * fertilization.quantity

            self.inputs.phosphorus_fertilizers += (
                (fertilization.fertilizer.phosphorus_total_content or 0)
                / 100
                * fertilization.quantity
            )

            self.inputs.potassium_fertilizers += (
                (fertilization.fertilizer.potassium_total_content or 0)
                / 100
                * fertilization.quantity
            )

    def compute_input_totals(self):
        """Compute total inputs"""

        self.inputs.nitrogen_total = (
            self.inputs.nitrogen_mineralization
            + self.inputs.nitrogen_fixation
            + self.inputs.nitrogen_irrigation
            + self.inputs.nitrogen_in_soil_initial
            + self.inputs.nitrogen_fertilizers
        )
        self.inputs.phosphorus_total = self.inputs.phosphorus_fertilizers
        self.inputs.potassium_total = self.inputs.potassium_fertilizers

    def compute_output_nitrogen_leaching(self):
        """Compute nitrogen leaching from the soil due to rainfall"""

        percolation_index = (
            (
                self.climate_zone.rainfall_annual
                - 10160 / self.soil.carbon_nitrogen_ratio
                + 101.6
            )
            ** 2
        ) / (
            self.climate_zone.rainfall_annual
            + 15240 / self.soil.carbon_nitrogen_ratio
            - 152.4
        )
        seasonal_index = (
            (2 * self.climate_zone.rainfall_autumn_winter)
            / self.climate_zone.rainfall_annual
        ) ** (1 / 3)

        leaching_index = percolation_index * seasonal_index

        self.outputs.nitrogen_leaching = self.soil_nitrogen_content_initial * (
            1 - exp((-leaching_index) / (self.soil_depth * 1000 * self.soil.qfc_qwp))
        )

    def compute_output_nitrogen_uptake(self):
        """Compute nitrogen uptake from the crop"""

        self.outputs.nitrogen_uptake = (
            self._harvest_dry_matter_med_50
            * (self.crop_profile.harvest_nitrogen_content / 100)
            + self._residues_dry_matter_med_50
            * (self.crop_profile.residues_nitrogen_content / 100)
        ) * (1 + self.NITROGEN_RATIO_ROOTS_VS_SHOOTS)

    def compute_output_phosphorus_uptake(self):
        """Compute phosphorus uptake from the crop"""

        phosphorus_exported = (
            self._harvest_dry_matter_med_50
            * (self.crop_profile.harvest_phosphorus_content / 100)
            + self._residues_dry_matter_med_50
            * (1 - self.FRACTION_RESIDUES_LEFT_IN_FIELD_AFTER_HARVEST_WITH_ROOTS)
            * (self.crop_profile.residues_phosphorus_content / 100)
            * (100 - self.crop_profile.residues_left_in_field)
            / 100
        )

        if (
            self.phosphorus_potassium_strategy
            == PhosphorusPotassiumStrategy.maintenance
        ):
            self.outputs.phosphorus_uptake = phosphorus_exported
            return

        soil_phosphorus_content_corrected = (
            self.soil_phosphorus_content * self.soil_phosphorus_analysis_method.value
        )

        soil_phosphorus_content_stl_tmin = (
            1
            if soil_phosphorus_content_corrected
            < self.soil.phosphorus_content_threshold_min
            else 0
        )
        soil_phosphorus_content_2stl_tmin = (
            0.5
            if soil_phosphorus_content_corrected
            > 2 * self.soil.phosphorus_content_threshold_min
            else 1
        )
        soil_phosphorus_content_stl_tmax = (
            1
            if soil_phosphorus_content_corrected
            < self.soil.phosphorus_content_threshold_max
            else 0
        )
        soil_phosphorus_content_2stl_tmax = (
            0.5
            if soil_phosphorus_content_corrected
            > 2 * self.soil.phosphorus_content_threshold_max
            else 1
        )

        crop_phosphorus_min = (
            phosphorus_exported * soil_phosphorus_content_2stl_tmin
            + (10 * self.soil.density * self.soil_depth)
            * (
                self.soil.phosphorus_content_threshold_min
                - soil_phosphorus_content_corrected
            )
            * soil_phosphorus_content_stl_tmin
        )
        crop_phosphorus_years_min = max(
            ceil(crop_phosphorus_min / self.CROP_PHOSPHORUS_EXTRACTION_MAX), 1
        )

        crop_phosphorus_max = (
            phosphorus_exported * soil_phosphorus_content_2stl_tmax
            + (10 * self.soil.density * self.soil_depth)
            * (
                self.soil.phosphorus_content_threshold_max
                - soil_phosphorus_content_corrected
            )
            * soil_phosphorus_content_stl_tmax
        )
        crop_phosphorus_years_max = max(
            ceil(crop_phosphorus_max / self.CROP_PHOSPHORUS_EXTRACTION_MAX), 1
        )

        if (
            self.phosphorus_potassium_strategy
            == PhosphorusPotassiumStrategy.sufficiency
        ):
            self.outputs.phosphorus_uptake = (
                (10 * self.soil.density * self.soil_depth)
                * (
                    self.soil.phosphorus_content_threshold_min
                    - soil_phosphorus_content_corrected
                )
                * soil_phosphorus_content_stl_tmin
                / crop_phosphorus_years_min
            )
        elif (
            self.phosphorus_potassium_strategy
            == PhosphorusPotassiumStrategy.minimum_fertilizer
        ):
            self.outputs.phosphorus_uptake = (
                phosphorus_exported * soil_phosphorus_content_2stl_tmin
                + (10 * self.soil.density * self.soil_depth)
                * (
                    self.soil.phosphorus_content_threshold_min
                    - soil_phosphorus_content_corrected
                )
                * soil_phosphorus_content_stl_tmin
            ) / crop_phosphorus_years_min

        elif (
            self.phosphorus_potassium_strategy
            == PhosphorusPotassiumStrategy.maximum_yield
        ):
            self.outputs.phosphorus_uptake = (
                phosphorus_exported * soil_phosphorus_content_2stl_tmax
                + (10 * self.soil.density * self.soil_depth)
                * (
                    self.soil.phosphorus_content_threshold_max
                    - soil_phosphorus_content_corrected
                )
                * soil_phosphorus_content_stl_tmax
            ) / crop_phosphorus_years_max

        else:
            raise ValueError(
                "Unknown PK strategy: %s", self.phosphorus_potassium_strategy
            )

    def compute_output_potassium_uptake(self):
        """Compute potassium uptake from the crop"""

        potassium_exported = (
            self._harvest_dry_matter_med_50
            * (self.crop_profile.harvest_potassium_content / 100)
            + self._residues_dry_matter_med_50
            * (1 - self.FRACTION_RESIDUES_LEFT_IN_FIELD_AFTER_HARVEST_WITH_ROOTS)
            * (self.crop_profile.residues_potassium_content / 100)
            * (100 - self.crop_profile.residues_left_in_field)
            / 100
        )

        if (
            self.phosphorus_potassium_strategy
            == PhosphorusPotassiumStrategy.maintenance
        ):
            self.outputs.potassium_uptake = potassium_exported
            return

        soil_potassium_content_stl_tmin = (
            1
            if self.soil_potassium_content < self.soil.potassium_content_threshold_min
            else 0
        )
        soil_potassium_content_2stl_tmin = (
            0.5
            if self.soil_potassium_content / 100
            > 2 * self.soil.potassium_content_threshold_min
            else 1
        )
        soil_potassium_content_stl_tmax = (
            1
            if self.soil_potassium_content < self.soil.potassium_content_threshold_max
            else 0
        )
        soil_potassium_content_2stl_tmax = (
            0.5
            if self.soil_potassium_content
            > 2 * self.soil.potassium_content_threshold_max
            else 1
        )

        crop_potassium_min = (
            potassium_exported * soil_potassium_content_2stl_tmin
            + (10 * self.soil.density * self.soil_depth)
            * (self.soil.potassium_content_threshold_min - self.soil_potassium_content)
            * self.soil.fk
            * soil_potassium_content_stl_tmin
        )
        crop_potassium_years_min = max(
            ceil(crop_potassium_min / self.CROP_POTASSIUM_EXTRACTION_MAX), 1
        )

        if (
            self.phosphorus_potassium_strategy
            == PhosphorusPotassiumStrategy.minimum_fertilizer
        ):
            self.outputs.potassium_uptake = (
                potassium_exported * soil_potassium_content_2stl_tmin
                + (10 * self.soil.density * self.soil_depth)
                * (
                    self.soil.potassium_content_threshold_min
                    - self.soil_potassium_content
                )
                * self.soil.fk
                * soil_potassium_content_stl_tmin
            ) / crop_potassium_years_min
            return

        elif (
            self.phosphorus_potassium_strategy
            == PhosphorusPotassiumStrategy.sufficiency
        ):
            self.outputs.potassium_uptake = (
                (10 * self.soil.density * self.soil_depth)
                * (
                    self.soil.potassium_content_threshold_min
                    - self.soil_potassium_content
                )
                * self.soil.fk
                * soil_potassium_content_stl_tmin
                / crop_potassium_years_min
            )
            return

        if (
            self.phosphorus_potassium_strategy
            == PhosphorusPotassiumStrategy.maximum_yield
        ):
            crop_potassium_max = (
                soil_potassium_content_2stl_tmax
                * (
                    (
                        potassium_exported * soil_potassium_content_2stl_tmin
                        + (10 * self.soil.density * self.soil_depth)
                        * (
                            self.soil.potassium_content_threshold_min
                            - self.soil_potassium_content
                        )
                        * self.soil.fk
                        * soil_potassium_content_stl_tmin
                    )
                    / crop_potassium_years_min
                )
                + (10 * self.soil.density * self.soil_depth)
                * (
                    self.soil.potassium_content_threshold_max
                    - self.soil_potassium_content
                )
                * self.soil.fk
                * soil_potassium_content_stl_tmax
            )

            crop_potassium_years_max = max(
                ceil(crop_potassium_max / self.CROP_POTASSIUM_EXTRACTION_MAX), 1
            )

            self.outputs.potassium_uptake = (
                potassium_exported * soil_potassium_content_2stl_tmax
                + (10 * self.soil.density * self.soil_depth)
                * (
                    self.soil.potassium_content_threshold_max
                    - self.soil_potassium_content
                )
                * self.soil.fk
                * soil_potassium_content_stl_tmax
            ) / crop_potassium_years_max

        else:
            raise ValueError(
                "Unknown PK strategy: %s", self.phosphorus_potassium_strategy
            )

    def compute_output_nitrogen_denitrification(self):
        """Compute nitrogen denitrification from the crop"""

        nitrogen_crop = max(
            self.outputs.nitrogen_leaching
            + self.outputs.nitrogen_uptake
            + self.outputs.nitrogen_in_soil_final
            - self.inputs.nitrogen_mineralization
            - self.inputs.nitrogen_fixation
            - self.inputs.nitrogen_irrigation
            - self.inputs.nitrogen_in_soil_initial,
            0,
        )
        self.outputs.nitrogen_denitrification = 0.34 * exp(0.012 * nitrogen_crop)

    def compute_output_nitrogen_in_soil_final(self):
        """Compute nitrogen in soil at the end of the crop"""

        self.outputs.nitrogen_in_soil_final = self.soil_nitrogen_content_final

    def compute_output_nitrogen_volatilization(self):
        """Compute nitrogen volatilization, based on irrigation method, soil pH and CEC"""

        nitrogen_mineralization_from_fertilization = 0
        nitrogen_total_losses_volatilization = 0
        nitrogen_total_losses_denitrification = 0

        # Compute nitrogen volatilization coefficient based on irrigation and soil
        # conditions
        nitrogen_volatilization_coefficient = -0.402

        if self.water_supply_method == WaterSupplyMethod.rainfed:
            nitrogen_volatilization_coefficient -= 0.045

        nitrogen_volatilization_coefficient += (
            VolatilizationCoefficientForPH.objects.get(
                (Q(ph_from__lte=self.soil_ph) | Q(ph_from__isnull=True))
                & (Q(ph_to__gt=self.soil_ph) | Q(ph_to__isnull=True))
            ).volatilization_coefficient
        )

        nitrogen_volatilization_coefficient += (
            VolatilizationCoefficientForCEC.objects.get(
                (Q(cec_from__lte=self.soil.cec) | Q(cec_from__isnull=True))
                & (Q(cec_to__gt=self.soil.cec) | Q(cec_to__isnull=True))
            ).volatilization_coefficient
        )

        nitrogen_volatilization_coefficient = exp(nitrogen_volatilization_coefficient)

        # For each fertilization/fertilizer, compute the volatilization and
        # denitrification
        for fertilization in self.fertilizations:
            fertilizer_nitrogen_content = (
                fertilization.fertilizer.nitrogen_organic_dry_matter_content
                if fertilization.fertilizer.is_organic
                else fertilization.fertilizer.nitrogen_total_content
            ) / 100

            # fertilizer_nitrogen_content = (
            #     fertilization.fertilizer.nitrogen_total_content / 100
            # )

            nitrogen_volatilization_losses = (
                exp(
                    -(
                        1.895
                        if fertilization.fertilizer.incorporation_method
                        == FertilizerIncorporationMethod.incorporated
                        else (
                            1.305
                            if fertilization.fertilizer.incorporation_method
                            == FertilizerIncorporationMethod.topdressing
                            else 0
                        )
                    )
                    + fertilization.fertilizer.volatilization_coefficient
                )
                * nitrogen_volatilization_coefficient
            )

            nitrogen_bf_volatilization = fertilizer_nitrogen_content * (
                1 - nitrogen_volatilization_losses
            )

            nitrogen_denitrification_losses = 0
            nitrogen_bf_denitrification = fertilizer_nitrogen_content * (
                1 - nitrogen_denitrification_losses
            )

            if fertilizer_nitrogen_content > 0:
                nitrogen_bf = (
                    nitrogen_bf_volatilization
                    * nitrogen_bf_denitrification
                    / fertilizer_nitrogen_content
                )
            else:
                nitrogen_bf = 0

            fertilizer_application_frequency = (
                1.0
                if (
                    not fertilization.fertilizer.is_organic
                    or fertilization.application_frequency
                    == FertilizerApplicationFrequency.annual
                )
                else 0.5
            )

            if fertilization.quantity is not None:
                nitrogen_mineralization_from_fertilization += (
                    nitrogen_bf
                    * fertilization.quantity
                    * fertilizer_application_frequency
                )
                nitrogen_raw = (
                    nitrogen_bf
                    * fertilization.quantity
                    / (
                        1
                        - nitrogen_volatilization_losses
                        - nitrogen_denitrification_losses
                    )
                )
                nitrogen_total_losses_volatilization += (
                    nitrogen_raw * nitrogen_volatilization_losses
                )

                nitrogen_total_losses_denitrification += (
                    nitrogen_raw - nitrogen_raw * nitrogen_volatilization_losses
                ) * nitrogen_denitrification_losses

        self.outputs.nitrogen_volatilization = nitrogen_total_losses_volatilization

    def compute_output_totals(self):
        """Compute total outputs"""

        self.outputs.nitrogen_total = (
            self.outputs.nitrogen_leaching
            + self.outputs.nitrogen_denitrification
            + self.outputs.nitrogen_in_soil_final
            + self.outputs.nitrogen_volatilization
            + self.outputs.nitrogen_uptake
        )
        self.outputs.phosphorus_total = self.outputs.phosphorus_uptake
        self.outputs.potassium_total = self.outputs.potassium_uptake

    def compute_fertilizer_quantities(self):
        """Compute the quantity of the fertilizations/fertilizers for which the
        provided quantity is None

        For each of the fertilizers, max out the quantity until the total of either
        nitrogen, phosphorus or potassium is reached: this means that whatever the
        computed quantity, the total input nitrogen is never more than the total output
        (same for phosphorus and potassium).

        Note: the order of fertilizers in the fertilizations list is important, as the
        first fertilizers are maxed first.
        """

        # Missing nitrogen (to be covered) is the difference between the total nitrogen
        # outputs and the total nitrogen inputs, but it cannot be more than the allowed
        # nitrogen quantity per year
        missing_nitrogen = max(
            min(
                self.outputs.nitrogen_total - self.inputs.nitrogen_total,
                self.MAXIMUM_COMPUTED_QUANTITY_TOTAL_NITROGEN
                - self.inputs.nitrogen_fertilizers,
            ),
            0,
        )

        # Missing phosphorus (to be covered) is the difference between the total
        # phosphorus outputs and the total phosphorus inputs, but it cannot be more
        # than the allowed phosphorus quantity per year
        missing_phosphorus = max(
            min(
                self.outputs.phosphorus_total - self.inputs.phosphorus_total,
                self.MAXIMUM_COMPUTED_QUANTITY_TOTAL_PHOSPHORUS
                - self.inputs.phosphorus_fertilizers,
            ),
            0,
        )

        # Missing potassium (to be covered) is the difference between the total
        # potassium outputs and the total potassium inputs, but it cannot be more
        # than the allowed potassium quantity per year
        missing_potassium = max(
            min(
                self.outputs.potassium_total - self.inputs.potassium_total,
                self.MAXIMUM_COMPUTED_QUANTITY_TOTAL_POTASSIUM
                - self.inputs.potassium_fertilizers,
            ),
            0,
        )

        # Iterate over the fertilizations, and compute the quantity for each of
        # them, maxing out the quantity until the total of either nitrogen,
        # phosphorus or potassium is reached
        for fertilization in self.fertilizations:
            if fertilization.quantity is None:
                quantities = []

                if (
                    fertilization.fertilizer.is_organic
                    and fertilization.fertilizer.nitrogen_organic_dry_matter_content
                ):
                    quantities.append(
                        missing_nitrogen
                        / fertilization.fertilizer.nitrogen_organic_dry_matter_content
                        * 100
                    )

                if (
                    not fertilization.fertilizer.is_organic
                    and fertilization.fertilizer.nitrogen_total_content > 0
                ):
                    quantities.append(
                        missing_nitrogen
                        / fertilization.fertilizer.nitrogen_total_content
                        * 100
                    )

                if fertilization.fertilizer.phosphorus_total_content > 0:
                    quantities.append(
                        missing_phosphorus
                        / fertilization.fertilizer.phosphorus_total_content
                        * 100
                    )

                if fertilization.fertilizer.potassium_total_content > 0:
                    quantities.append(
                        missing_potassium
                        / fertilization.fertilizer.potassium_total_content
                        * 100
                    )

                if quantities:

                    fertilization.quantity = min(quantities)
                    if fertilization.fertilizer.is_organic:
                        fertilization.quantity = min(
                            fertilization.quantity,
                            self.MAXIMUM_COMPUTED_QUANTITY_FOR_ORGANIC_FERTILIZERS,
                        )
                    else:
                        fertilization.quantity = min(
                            fertilization.quantity,
                            self.MAXIMUM_COMPUTED_QUANTITY_FOR_MINERAL_FERTILIZERS,
                        )
                else:
                    fertilization.quantity = 0

                missing_nitrogen -= fertilization.quantity * (
                    fertilization.fertilizer.nitrogen_organic_dry_matter_content
                    if fertilization.fertilizer.is_organic
                    else fertilization.fertilizer.nitrogen_total_content
                )
                missing_phosphorus -= (
                    fertilization.quantity
                    * fertilization.fertilizer.phosphorus_total_content
                )
                missing_potassium -= (
                    fertilization.quantity
                    * fertilization.fertilizer.potassium_total_content
                )

    def recommend_additional_fertilizations(self):
        """Recommend additional fertilizations

        This method will compute optimized quantities for the remaining nutrients to
        be covered (N, P and K) using the list of fertilizer products passed in the
        `additional_fertilizers` parameter.
        """

        missing_nitrogen = max(
            min(
                self.outputs.nitrogen_total - self.inputs.nitrogen_total,
                self.MAXIMUM_COMPUTED_QUANTITY_TOTAL_NITROGEN
                - self.inputs.nitrogen_fertilizers,
            ),
            0,
        )
        missing_phosphorus = max(
            min(
                self.outputs.phosphorus_total - self.inputs.phosphorus_total,
                self.MAXIMUM_COMPUTED_QUANTITY_TOTAL_PHOSPHORUS
                - self.inputs.phosphorus_fertilizers,
            ),
            0,
        )
        missing_potassium = max(
            min(
                self.outputs.potassium_total - self.inputs.potassium_total,
                self.MAXIMUM_COMPUTED_QUANTITY_TOTAL_POTASSIUM
                - self.inputs.potassium_fertilizers,
            ),
            0,
        )

        fertilizers_nitrogen_content = np.array(
            [
                (f.fertilizer.nitrogen_organic_dry_matter_content or 0) / 100
                if f.fertilizer.is_organic
                else (f.fertilizer.nitrogen_total_content or 0) / 100
                for f in self.additional_fertilizations
            ]
        )
        fertilizers_phosphorus_content = np.array(
            [
                (f.fertilizer.phosphorus_total_content or 0) / 100
                for f in self.additional_fertilizations
            ]
        )
        fertilizers_potassium_content = np.array(
            [
                (f.fertilizer.potassium_total_content or 0) / 100
                for f in self.additional_fertilizations
            ]
        )

        # These nutrient factors are used in the minimized function to give more
        # "weight" to some of the nutrients.
        # They are a derived from the rough typical daily uptake of crops.
        nitrogen_factor = 1.5
        phosphorus_factor = 0.3
        potassium_factor = 1.5

        # We will optimize the additional fertilizers quantities based on the following
        # objectives:
        # - Minimize the missing nutrients after fertilization
        # - Constrain the total quantity of nutrients to never exceed the total outputs
        # - Keep the fertilizer quantities non-negative
        def missing_nutrients(quantities):
            fertilizer_nitrogen = np.multiply(
                quantities, fertilizers_nitrogen_content
            ).sum()
            fertilizer_phosphorus = np.multiply(
                quantities, fertilizers_phosphorus_content
            ).sum()
            fertilizer_potassium = np.multiply(
                quantities, fertilizers_potassium_content
            ).sum()
            loss = (
                (missing_nitrogen - fertilizer_nitrogen) ** 2 / nitrogen_factor ** 2
                + (missing_phosphorus - fertilizer_phosphorus) ** 2
                / phosphorus_factor ** 2
                + (missing_potassium - fertilizer_potassium) ** 2
                / potassium_factor ** 2
            )
            return loss

        # We will start with all fertilizer quantities set to 0
        x0 = np.zeros(len(self.additional_fertilizations))

        # Set a minimum bound of zero for our fertilizer quantities and no maximum
        # bound (the max will be dealt with by the constraint)
        bounds = scipy.optimize.Bounds(
            lb=np.zeros(len(self.additional_fertilizations)),
            ub=np.array([np.inf] * len(self.additional_fertilizations)),
        )

        # Set a constraint to ensure that the total inputs is never more than the total
        # outputs (ie that the input from the additional fertilizers is never more than
        # the missing nutrients)
        def quantity_nutrient_content_constraint_fun(quantities):
            return np.array(
                [
                    np.multiply(quantities, fertilizers_nitrogen_content).sum(),
                    np.multiply(quantities, fertilizers_phosphorus_content).sum(),
                    np.multiply(quantities, fertilizers_potassium_content).sum(),
                ]
            )

        quantity_nutrient_content_constraint = scipy.optimize.NonlinearConstraint(
            fun=quantity_nutrient_content_constraint_fun,
            lb=np.zeros(3),
            ub=np.array([missing_nitrogen, missing_phosphorus, missing_potassium]),
            keep_feasible=False,
            hess=lambda x, v: np.zeros(len(self.additional_fertilizations)),
        )

        # Set a constraint to ensure that the proposed quantities never exceed the max
        # values for organic and mineral fertilizers
        quantity_constraint_ub = np.array(
            [
                self.MAXIMUM_COMPUTED_QUANTITY_FOR_ORGANIC_FERTILIZERS
                if f.fertilizer.is_organic
                else self.MAXIMUM_COMPUTED_QUANTITY_FOR_MINERAL_FERTILIZERS
                for f in self.additional_fertilizations
            ]
        )
        quantity_constraint = scipy.optimize.NonlinearConstraint(
            fun=lambda quantities: quantities,
            lb=np.zeros(len(self.additional_fertilizations)),
            ub=quantity_constraint_ub,
            keep_feasible=False,
            hess=lambda x, v: np.zeros(len(self.additional_fertilizations)),
        )

        # Run the optimizer
        result = scipy.optimize.minimize(
            fun=missing_nutrients,
            x0=x0,
            method="trust-constr",
            bounds=bounds,
            constraints=[quantity_nutrient_content_constraint, quantity_constraint],
            hess=lambda x: np.zeros(len(self.additional_fertilizations)),
        )

        # Assign result quantities to the additional fertilizers
        for quantity, fertilization in zip(result.x, self.additional_fertilizations):
            fertilization.quantity = quantity
