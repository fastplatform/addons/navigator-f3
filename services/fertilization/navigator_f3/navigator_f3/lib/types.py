from typing import Optional
import hashlib
import logging
import json

from pydantic import confloat, BaseModel

from navigator_f3.models import Fertilizer, FertilizerApplicationFrequency


logger = logging.getLogger(__name__)


class Fertilization(BaseModel):
    fertilizer: Fertilizer
    application_frequency: FertilizerApplicationFrequency
    quantity: Optional[confloat(ge=0)]  # if None, will be calculated

    def round(self, num_digits: int = 2):
        self.quantity = round(self.quantity, num_digits)

    @property
    def id(self):
        return hashlib.md5(
            json.dumps(
                {
                    "fertilizer_id": self.fertilizer.id,
                    "application_frequency_id": self.fertilizer.id,
                    "quantity": self.quantity,
                }
            ).encode()
        ).hexdigest()

    class Config:
        arbitrary_types_allowed = True


class AdditionalFertilization(BaseModel):
    fertilizer: Fertilizer
    quantity: Optional[confloat(ge=0)]

    def round(self, num_digits: int = 2):
        self.quantity = round(self.quantity, num_digits)

    @property
    def id(self):
        return hashlib.md5(
            json.dumps(
                {
                    "fertilizer_id": self.fertilizer.id,
                    "quantity": self.quantity,
                }
            ).encode()
        ).hexdigest()

    class Config:
        arbitrary_types_allowed = True


class NavigatorF3ResultInputs(BaseModel):
    nitrogen_mineralization: float = None
    nitrogen_fixation: float = None
    nitrogen_irrigation: float = None
    nitrogen_in_soil_initial: float = None
    nitrogen_fertilizers: float = None
    phosphorus_fertilizers: float = None
    potassium_fertilizers: float = None
    nitrogen_total: float = None
    phosphorus_total: float = None
    potassium_total: float = None

    def round(self, num_digits: int = 2):
        for attr in self.dict().keys():
            if attr != "id":
                setattr(self, attr, round(getattr(self, attr), num_digits))

    @property
    def id(self):
        return hashlib.md5(self.json(sort_keys=True).encode()).hexdigest()


class NavigatorF3ResultOutputs(BaseModel):
    nitrogen_leaching: float = None
    nitrogen_denitrification: float = None
    nitrogen_in_soil_final: float = None
    nitrogen_volatilization: float = None
    nitrogen_uptake: float = None
    phosphorus_uptake: float = None
    potassium_uptake: float = None
    nitrogen_total: float = None
    phosphorus_total: float = None
    potassium_total: float = None

    def round(self, num_digits: int = 2):
        for attr in self.dict().keys():
            if attr != "id":
                setattr(self, attr, round(getattr(self, attr), num_digits))

    @property
    def id(self):
        return hashlib.md5(self.json(sort_keys=True).encode()).hexdigest()
