files=`ls ../../../init/bg/*.csv`

for filename in $files;
do
    base_name=`basename "$filename" .csv`;
    table_name=`echo "$base_name" | awk -F. '{print $2}'`
    echo "table_name: $table_name"
    table_headers=`head -n 1 $filename`
    copy_sql="\\COPY public.$table_name($table_headers) FROM '$filename' DELIMITER ',' CSV HEADER;"
    PGPASSWORD=${POSTGRES_PASSWORD} psql -U ${POSTGRES_USER} -h ${POSTGRES_HOST} -p ${POSTGRES_PORT} -d ${POSTGRES_DATABASE} -c "$copy_sql"
    echo "-----------------------------------------"
done
