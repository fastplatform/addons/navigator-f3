from django.utils.translation import ugettext_lazy as _

from constance.apps import ConstanceConfig


class ParametersConfig(ConstanceConfig):
    verbose_name = _("Global parameters")
