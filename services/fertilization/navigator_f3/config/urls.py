from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.views.generic import RedirectView
from django.urls.conf import include, include
from django.views.decorators.csrf import csrf_exempt
from django.utils.translation import ugettext_lazy as _
from django.conf.urls.i18n import i18n_patterns

from graphene_django.views import GraphQLView

admin.site.site_title = _("Navigator F3")
admin.site.site_header = _("Navigator F3")
admin.site.index_title = _("Management console")
admin.site.site_url = "/admin"

urlpatterns = [
    path("", RedirectView.as_view(pattern_name="admin:index")),
    path("graphql", csrf_exempt(GraphQLView.as_view(graphiql=False))),
    path("i18n/", include("django.conf.urls.i18n")),
] + i18n_patterns(path("admin/", admin.site.urls))

if settings.DEBUG:
    import debug_toolbar

    urlpatterns += [
        path('__debug__/', include(debug_toolbar.urls)),
    ]
