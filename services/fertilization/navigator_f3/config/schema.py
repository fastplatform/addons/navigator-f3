import graphene
from django.conf import settings

from navigator_f3.schema.queries import Query as NavigatorF3Query
from fastplatform.schema import Query as FastPlatformQuery

query_classes = [NavigatorF3Query, FastPlatformQuery, graphene.ObjectType]

if settings.ENABLE_YOUNG_TREES:
    from young_trees.schema import Query as YoungTreesQuery

    query_classes = [YoungTreesQuery, *query_classes]


class Query(*query_classes):
    navigator_f3__healthz = graphene.String(default_value="OK", description="Liveliness probe")


schema = graphene.Schema(query=Query, auto_camelcase=False)
