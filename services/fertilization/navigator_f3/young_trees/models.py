from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.validators import MinValueValidator, MaxValueValidator


class YoungTree(models.Model):

    crop_profile = models.OneToOneField(
        to="navigator_f3.CropProfile",
        null=False,
        blank=False,
        on_delete=models.PROTECT,
        verbose_name=_("crop profile"),
        related_name="young_trees",
    )

    ph_ideal_min = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("minimum ideal pH"),
        validators=[MinValueValidator(0), MaxValueValidator(14)],
    )

    ph_ideal = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("ideal pH"),
        validators=[MinValueValidator(0), MaxValueValidator(14)],
    )

    ph_ideal_max = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("maximum ideal pH"),
        validators=[MinValueValidator(0), MaxValueValidator(14)],
    )

    trees_per_ha = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("trees per hectare"),
        validators=[MinValueValidator(0), MaxValueValidator(14)],
    )

    def __str__(self):
        return self.crop_profile.name

    class Meta:
        db_table = "young_tree"
        verbose_name = _("young tree")
        verbose_name_plural = _("young trees")


class YoungTreeFertilizationYear(models.Model):

    young_tree = models.ForeignKey(
        to="young_trees.YoungTree",
        null=False,
        blank=False,
        on_delete=models.CASCADE,
        verbose_name=_("young tree"),
        related_name="fertilization_years",
    )

    sort_order = models.IntegerField(null=False, blank=False, verbose_name=_("order"))

    label = models.CharField(
        max_length=60, null=False, blank=False, verbose_name=_("label")
    )

    nitrogen_per_ha_min = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("minimum nitrogen per hectare"),
        validators=[MinValueValidator(0)],
    )
    nitrogen_per_ha = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("nitrogen per hectare"),
        validators=[MinValueValidator(0)],
    )
    nitrogen_per_ha_max = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("maximum nitrogen per hectare"),
        validators=[MinValueValidator(0)],
    )
    phosphorus_per_ha_min = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("minimum phosphorus per hectare"),
        validators=[MinValueValidator(0)],
    )
    phosphorus_per_ha = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("phosphorus per hectare"),
        validators=[MinValueValidator(0)],
    )
    phosphorus_per_ha_max = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("maximum phosphorus per hectare"),
        validators=[MinValueValidator(0)],
    )
    potassium_per_ha_min = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("minimum potassium per hectare"),
        validators=[MinValueValidator(0)],
    )
    potassium_per_ha = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("potassium per hectare"),
        validators=[MinValueValidator(0)],
    )
    potassium_per_ha_max = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("maximum potassium per hectare"),
        validators=[MinValueValidator(0)],
    )
    nitrogen_per_tree_min = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("minimum nitrogen per tree"),
        validators=[MinValueValidator(0)],
    )
    nitrogen_per_tree = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("nitrogen per tree"),
        validators=[MinValueValidator(0)],
    )
    nitrogen_per_tree_max = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("maximum nitrogen per tree"),
        validators=[MinValueValidator(0)],
    )
    phosphorus_per_tree_min = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("minimum phosphorus per tree"),
        validators=[MinValueValidator(0)],
    )
    phosphorus_per_tree = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("phosphorus per tree"),
        validators=[MinValueValidator(0)],
    )
    phosphorus_per_tree_max = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("maximum phosphorus per tree"),
        validators=[MinValueValidator(0)],
    )
    potassium_per_tree_min = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("minimum potassium per tree"),
        validators=[MinValueValidator(0)],
    )
    potassium_per_tree = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("potassium per tree"),
        validators=[MinValueValidator(0)],
    )
    potassium_per_tree_max = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("maximum potassium per tree"),
        validators=[MinValueValidator(0)],
    )

    def __str__(self):
        return f"{self.young_tree} / {self.label}"

    class Meta:
        db_table = "young_tree_fertilization_year"
        verbose_name = _("fertilization year")
        verbose_name_plural = _("fertilization years")
        ordering = ("sort_order",)
