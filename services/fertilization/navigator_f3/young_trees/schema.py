import json
import hashlib

import graphene
from graphene_django import DjangoObjectType
from graphql import GraphQLError


from young_trees.models import YoungTree, YoungTreeFertilizationYear


class YoungTreeType(DjangoObjectType):
    class Meta:
        model = YoungTree
        name = "young_trees__young_tree"
        convert_choices_to_enum = False
        description = YoungTree._meta.verbose_name.capitalize()


class YoungTreeFertilizationYearType(DjangoObjectType):
    class Meta:
        model = YoungTreeFertilizationYear
        name = "young_trees__young_tree_fertilization_year"
        fields = ("id", "label")
        convert_choices_to_enum = False
        description = YoungTreeFertilizationYear._meta.verbose_name.capitalize()


class YoungTreeGetFertilizationResultType(graphene.ObjectType):
    id = graphene.ID(required=True)
    nitrogen_per_ha_min = graphene.Float(required=False)
    nitrogen_per_ha = graphene.Float(required=False)
    nitrogen_per_ha_max = graphene.Float(required=False)
    phosphorus_per_ha_min = graphene.Float(required=False)
    phosphorus_per_ha = graphene.Float(required=False)
    phosphorus_per_ha_max = graphene.Float(required=False)
    potassium_per_ha_min = graphene.Float(required=False)
    potassium_per_ha = graphene.Float(required=False)
    potassium_per_ha_max = graphene.Float(required=False)
    nitrogen_per_tree_min = graphene.Float(required=False)
    nitrogen_per_tree = graphene.Float(required=False)
    nitrogen_per_tree_max = graphene.Float(required=False)
    phosphorus_per_tree_min = graphene.Float(required=False)
    phosphorus_per_tree = graphene.Float(required=False)
    phosphorus_per_tree_max = graphene.Float(required=False)
    potassium_per_tree_min = graphene.Float(required=False)
    potassium_per_tree = graphene.Float(required=False)
    potassium_per_tree_max = graphene.Float(required=False)

    class Meta:
        name = "young_trees__get_fertilization_result"
        description = "Algorithm results (for young trees)"


class Query:
    young_trees__young_trees = graphene.List(YoungTreeType, description="Young trees")
    young_trees__get_fertilization = graphene.Field(
        YoungTreeGetFertilizationResultType,
        description="Compute fertilization advice for young trees",
        fertilization_year_id=graphene.Argument(
            graphene.ID,
            required=True,
            description="Fertilization year identifier (also determines the young tree identifier)",
        ),
        trees_per_ha=graphene.Argument(
            graphene.Int, required=False, description="Number of trees per hectare"
        ),
    )

    def resolve_young_trees__young_trees(root, info):
        return YoungTree.objects.prefetch_related("fertilization_years").all()

    def resolve_young_trees__get_fertilization(
        root, info, fertilization_year_id, trees_per_ha=None
    ):

        try:
            fertilization_year = YoungTreeFertilizationYear.objects.select_related(
                "young_tree"
            ).get(pk=fertilization_year_id)
        except YoungTreeFertilizationYear.DoesNotExist:
            raise GraphQLError(
                "VALUE_ERROR",
                extensions={
                    f"Fertilization year with id={fertilization_year_id} does not exist"
                },
            )

        if trees_per_ha is not None:
            fertilization_year.young_tree.trees_per_ha = trees_per_ha

        result = {}

        for element in ["nitrogen", "phosphorus", "potassium"]:
            for suffix in ["_min", "", "_max"]:
                per_tree_arg = f"{element}_per_tree{suffix}"
                per_ha_arg = f"{element}_per_ha{suffix}"

                result[per_tree_arg] = getattr(fertilization_year, per_tree_arg)
                result[per_ha_arg] = getattr(fertilization_year, per_ha_arg)

        result["id"] = hashlib.md5(
            json.dumps(result, sort_keys=True).encode()
        ).hexdigest()

        return YoungTreeGetFertilizationResultType(**result)
