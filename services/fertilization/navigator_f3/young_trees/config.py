from typing import OrderedDict
from django.utils.translation import ugettext_lazy as _

YOUNG_TREES_ADDITIONAL_FIELDS = {}
YOUNG_TREES_CONFIG = OrderedDict([])
YOUNG_TREES_CONFIG_FIELDSETS = OrderedDict([])
