from django.contrib import admin
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from young_trees.models import YoungTree, YoungTreeFertilizationYear


class YoungTreeFertilizationYearInline(admin.StackedInline):
    model = YoungTreeFertilizationYear
    extra = 0

    fieldsets = (
        (None, {
            "fields": ('id', 'sort_order', 'label')
        }),
        (_('Per hectare'), {
            "fields": (
                ('nitrogen_per_ha_min', 'nitrogen_per_ha', 'nitrogen_per_ha_max'),
                ('phosphorus_per_ha_min', 'phosphorus_per_ha', 'phosphorus_per_ha_max'),
                ('potassium_per_ha_min', 'potassium_per_ha', 'potassium_per_ha_max'),
            )
        }),
        (_('Per tree'), {
            "fields": (
                ('nitrogen_per_tree_min', 'nitrogen_per_tree', 'nitrogen_per_tree_max'),
                ('phosphorus_per_tree_min', 'phosphorus_per_tree', 'phosphorus_per_tree_max'),
                ('potassium_per_tree_min', 'potassium_per_tree', 'potassium_per_tree_max'),
            )
        })
    )


class YoungTreeAdmin(admin.ModelAdmin):
    inlines = [YoungTreeFertilizationYearInline]

    fieldsets = (
        (None, {
            "fields": ('crop_profile', 'trees_per_ha')
        }),
        (_('pH'), {
            "fields": (
                (('ph_ideal_min', 'ph_ideal', 'ph_ideal_max'),)
            )
        })
    )


if settings.ENABLE_YOUNG_TREES:
    admin.site.register(YoungTree, YoungTreeAdmin)
