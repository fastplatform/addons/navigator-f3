from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class YoungTreesConfig(AppConfig):
    name = "young_trees"
    verbose_name = _("Young trees data")
