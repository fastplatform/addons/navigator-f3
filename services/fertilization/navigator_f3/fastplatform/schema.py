import logging

import graphene
from graphene_django import DjangoObjectType


from fastplatform.models import CropProfilePlantSpecies

logger = logging.getLogger(__name__)


class CropProfilePlantSpeciesType(DjangoObjectType):
    class Meta:
        model = CropProfilePlantSpecies
        name = "navigator_f3__crop_profile_plant_species_type"
        convert_choices_to_enum = False


class Query:
    navigator_f3__crop_profile_plant_species = graphene.List(
        graphene.NonNull(CropProfilePlantSpeciesType),
        description="Crop mapping between the IACS nomenclature and the Navigator F3 nomenclature",
        plant_species_id=graphene.Argument(graphene.ID, required=False),
    )

    def resolve_navigator_f3__crop_profile_plant_species(
        root, info, plant_species_id=None
    ):
        queryset = CropProfilePlantSpecies.objects.filter(crop_profile__is_active=True)
        if plant_species_id is not None:
            queryset = queryset.filter(
                plant_species_id=plant_species_id
            )
        return queryset.all()
