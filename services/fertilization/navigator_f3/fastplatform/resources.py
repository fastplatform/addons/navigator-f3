from utils.resources import ModelResourceWithColumnIds
from fastplatform.models import CropProfilePlantSpecies


class CropProfilePlantSpeciesResource(ModelResourceWithColumnIds):
    class Meta:
        model = CropProfilePlantSpecies