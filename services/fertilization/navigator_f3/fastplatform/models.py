from django.db import models
from django.utils.translation import ugettext_lazy as _


class CropProfilePlantSpecies(models.Model):
    """Maps the relationship between a crop profile (in Navigator F3) and a plant
    species (in the IACS nomenclature of the target country)"""

    id = models.AutoField(primary_key=True, verbose_name=_("identifier"))

    crop_profile = models.ForeignKey(
        to="navigator_f3.CropProfile",
        on_delete=models.CASCADE,
        verbose_name=_("crop profile"),
    )

    plant_species_id = models.CharField(
        null=True,
        max_length=8,
        verbose_name=_("plant species identifier"),
        db_index=True,
        help_text=_(
            "Plant species identifier in the FaST database, usually corresponds to the IACS plant species identifier"
        ),
    )

    class Meta:
        db_table = "crop_profile_plant_species"
        verbose_name = _("crop profile / plant species mapping")
        verbose_name_plural = _("crop profile / plant species mappings")
        unique_together = ("crop_profile", "plant_species_id")
