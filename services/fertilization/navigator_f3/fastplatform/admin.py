from django.contrib import admin

from import_export.admin import ImportExportMixin

from fastplatform.models import CropProfilePlantSpecies
from fastplatform.resources import CropProfilePlantSpeciesResource


@admin.register(CropProfilePlantSpecies)
class CropProfilePlantSpeciesAdmin(ImportExportMixin, admin.ModelAdmin):
    list_display = ('id', 'crop_profile', 'plant_species_id')
    resource_class = CropProfilePlantSpeciesResource
